import re
from collections import defaultdict

from .toposort import CycleError, topological_sort
import json

from abc import ABCMeta, abstractmethod

aesthetic_keys = {
    'background',
    'bgcolor',
    'bgcolor2',
    'bgfillcolor_angle',
    'bgfillcolor_autogradient',
    'bgfillcolor_color',
    'bgfillcolor_color1',
    'bgfillcolor_color2',
    'bgfillcolor_proportion',
    'bgfillcolor_type',
    'bgmode',
    'bgoncolor',
    'blinkcolor',
    'border',
    'bordercolor',
    'checkedcolor',
    'clickthrough',
    'color',
    'curvecolor',
    'enablehscroll',
    'enablevscroll',
    'fontface',
    'fontname',
    'fontsize',
    'gradient',
    'gridcolor',
    'hidden',
    'htabcolor',
    'htricolor',
    'ignoreclick',
    'markercolor',
    'needlecolor',
    'offcolor',
    'offset',
    'outlinecolor',
    'patching_rect',
    'presentation',
    'presentation_rect',
    'rounded',
    'selectioncolor',
    'slidercolor',
    'style',
    'tabcolor',
    'textcolor',
    'textoncolor',
    'textovercolor',
    'tricolor',
    'uncheckedcolor',
    'usebgoncolor',
}

nonprinted_keys = {
    'data',
    'id',
    'maxclass',
    'numinlets',
    'numoutlets',
    'outlettype',
    'saved_attribute_attributes',
    'saved_object_attributes',
}


TAB = '    '


def filter_keys(d, to_remove):
    return {k: v for (k, v) in d.items() if k not in to_remove}


def remove_aesthetics(box):
    return {k: v for (k, v) in box.items() if not (k in aesthetic_keys or k.endswith('color'))}


########################################################################
class BasePatchDecompiler(metaclass=ABCMeta):
    """"""

    #----------------------------------------------------------------------
    def __init__(self, content, file, id_prefix='', tabn=1):
        """"""
        self.id_prefix = id_prefix
        self.inlets = []
        self.outlets = []
        self.tabn = tabn
        self.tabs = TAB * tabn

        if len(content) == 1 and 'patcher' in content: #not subpatch
            content = content['patcher']

        content = content.copy()

        boxes = {b['box']['id']: b['box'] for b in content.pop('boxes', [])}
        lines = [(l['patchline']['source'], l['patchline']['destination'])
                 for l in content.pop('lines', [])]

        divined_id_map = {id: self._get_id(box, self.id_prefix)
                          for (id, box) in boxes.items()}

        lines_by_source_id = defaultdict(list)
        for line in lines:
            source, dest = line
            lines_by_source_id[source[0]].append(line)

        topo_sort_graph = {
            source_id: set(line[1][0] for line in lines)
            for (source_id, lines) in lines_by_source_id.items()
        }

        try:
            sort_order = list(topological_sort(topo_sort_graph))
        except CycleError:
            sort_order = []

        self.boxes = boxes
        self.lines = lines
        self.divined_id_map = divined_id_map
        self.lines_by_source_id = lines_by_source_id
        self.sort_order = sort_order

        self.subpatchers = []
        for box in boxes.values():
            if 'patcher' in box:
                self.subpatchers.append(box.pop('patcher'))
                box['subpatcher_id'] = (len(self.subpatchers) - 1)
                box['maxclass'] = '_subpatcher'

        self.file = file

    #----------------------------------------------------------------------
    def _get_id(self, box, id_prefix=''):
        """"""
        num_id = box['id'].replace('obj-', '')
        t_id = self._get_box_printable_class(box)
        if t_id == 'newobj' and 'text' in box:
            t_id = (self._get_name(box['text'].lower()) or t_id)

        id_ = f"{id_prefix}{t_id}_{num_id}"

        if t_id == 'inlet':
            self.inlets.append(id_)

        if t_id == 'outlet':
            self.outlets.append(id_)

        return id_

    #----------------------------------------------------------------------

    def _get_box_printable_class(self, box):
        t_id = box['maxclass']
        if t_id == 'newobj':
            if box['text'].startswith('r '):
                t_id = 'receive'
            elif box['text'].startswith('s '):
                t_id = 'send'
            elif box['text'].isalnum():
                t_id = box['text']

        t_id = t_id.replace('~', '_sigl')
        return t_id

    #----------------------------------------------------------------------
    def _get_name(self, value):
        """"""
        value = re.sub('[^a-z0-9_]+', '', value.replace(' ', '_'), flags=re.I)
        if not value:
            return None
        if not value[0].isalpha():
            value = '_' + value
        return value

    #----------------------------------------------------------------------
    def _sort_key(self, pair):
        """"""
        id, box = pair
        if id in self.sort_order:
            return (False, -self.sort_order.index(id))
        else:
            return (True, self.divined_id_map.get(id, id))

    #----------------------------------------------------------------------
    def process(self):
        """"""
        for id, box in sorted(self.boxes.items(), key=self._sort_key):
            self.process_box(box)

        if self.outlets:
            self.file.write(self.tabs + f'return {", ".join(self.outlets)}\n')

        for id, subpatcher in enumerate(self.subpatchers):
            self.process_subpatcher(id, subpatcher)

        return self.file

    #----------------------------------------------------------------------
    @abstractmethod
    def process_box(self, box):
        pass

    #----------------------------------------------------------------------
    @abstractmethod
    def process_subpatcher(self, id, subpatcher):
        pass


########################################################################
class PatchDecompiler(BasePatchDecompiler):
    """"""

    #----------------------------------------------------------------------
    def process_box(self, box):
        """"""
        id = box['id']
        box = box.copy()
        if box['maxclass'] == 'comment':
            self.file.write(f'# (comment) {box["text"]}\n')
            return

        #type_comment = '{n_in} in, {n_out} out ({types})'.format(
            #n_in=box.get('numinlets', 0),
            #n_out=box.get('numoutlets', 0),
            #types=', '.join(s or '?' for s in box.get('outlettype', [])),
        #)
        #self.file.write(f'#{type_comment}\n')

        printable_class = self._get_box_printable_class(box)
        filtered_box = filter_keys(remove_aesthetics(box), nonprinted_keys)
        formatted_keys = ['{}={}'.format(key, repr(value)) for (key, value) in sorted(filtered_box.items())]

        if len(formatted_keys) <= 2 or len(''.join(formatted_keys)) <= 79:
            self.file.write(self.tabs + '{id} = {cls}({keys})\n'.format(
                id=self.divined_id_map[id],
                cls=printable_class,
                keys=', '.join(formatted_keys),
            ))

        else:
            self.file.write(self.tabs + '{id} = {cls}('.format(
                id=self.divined_id_map[id],
                cls=printable_class,
            ))
            self.file.write(self.tabs + f'{self.divined_id_map[id]} = {printable_class}(')

            for line in formatted_keys:
                self.file.write(f' {line},')
            self.file.write(')\n')

        outlet_types = dict(enumerate(box.get('outlettype', [])))
        for line in sorted(self.lines_by_source_id[id]):
            (source_id, source_pin), (dest_id, dest_pin) = line

            self.file.write(self.tabs + '{dest_id}[{dest_pin}] = {source_id}[{source_pin}]  # type = {type}\n'.format(
                source_id=self.divined_id_map[source_id],
                source_pin=source_pin,
                dest_id=self.divined_id_map[dest_id],
                dest_pin=dest_pin,
                type=(outlet_types.get(source_pin) or '?'),
            ))

    def process_subpatcher(self, id, subpatcher):
        sub_id_prefix = self.id_prefix + f'sp{id}_'
        subdecompiler = self.__class__(subpatcher, self.file, id_prefix=sub_id_prefix, tabn=2)
        self.file.write('\n' + subdecompiler.tabs.replace(TAB, '', 1) + f'#----------------------------------------------------------------------\n')
        self.file.write(subdecompiler.tabs.replace(TAB, '', 1) + f'def subpatcher_{id}({", ".join(subdecompiler.inlets)}):\n')
        #outlets = subdecompiler.outlets

        subdecompiler.process()

        #self.file.write(TAB + f'return {", ".join(outlets)}\n')


def decompile_patch(patcher, file, id_prefix=''):
    decomp = PatchDecompiler(patcher, file, id_prefix)
    #decomp.initialize(patcher)
    decomp.process()


def decompile_patchverse(patchverse, file):
    for patch_name, patch in patchverse.items():
        prefix = re.sub('[^a-z0-9_]+', '', patch_name.split('.')[0], flags=re.I)
        if not prefix[0].isalpha():
            prefix = 'p' + prefix
        print('# ===')
        print('# === ' + patch_name)
        print('# ===')
        decompile_patch(patch, file, id_prefix=prefix + '_')


#----------------------------------------------------------------------
def decompile(filename, file):
    """"""
    with open(filename, 'rb') as infp:
        patchverse = json.load(infp)

        if 'patcher' in patchverse:
            decompile_patch(patchverse, file)
        else:
            decompile_patchverse(patchverse, file)


#----------------------------------------------------------------------
def decompiles(patchverse, file):
    """"""
    if 'patcher' in patchverse:
        decompile_patch(patchverse, file)
    else:
        decompile_patchverse(patchverse, file)
