p

    send_195 = send(text='s PrimeSet')
    send_119 = send(text='s Rhy')
    flonum_61 = flonum(format=6, parameter_enable=0)
    send_119[0] = flonum_61[0]  # type = ?
    send_132 = send(text='s VarI_T')
    number_238 = number(parameter_enable=0)
    send_132[0] = number_238[0]  # type = ?
    send_183 = send(text='s PcRange')
    number_194 = number(parameter_enable=0)
    send_183[0] = number_194[0]  # type = ?
    send_213 = send(text='s Tr')
    number_153 = number(parameter_enable=0)
    send_213[0] = number_153[0]  # type = ?
    send_218 = send(text='s I-Tr')
    number_211 = number(parameter_enable=0)
    send_218[0] = number_211[0]  # type = ?
    send_154 = send(text='s VarRy')
    number_193 = number(parameter_enable=0)
    send_154[0] = number_193[0]  # type = ?
    number_18 = number(parameter_enable=0)
    send_141 = send(text='s cardinal')
    message_179 = message(text='91 93 98 99 100')
    send_59 = send(text='s Reinicio-cont')
    button_58 = button()
    send_59[0] = button_58[0]  # type = bang
    send_167 = send(text='s Transpose')
    gswitch2_266 = gswitch2(int=1, parameter_enable=0)
    send_167[0] = gswitch2_266[0]  # type = ?
    button_58[0] = gswitch2_266[0]  # type = ?
    fromsymbol_170 = fromsymbol(text='fromsymbol')
    message_179[1] = fromsymbol_170[0]  # type = ?
    gswitch2_266[1] = fromsymbol_170[0]  # type = ?
    route_symbol_168 = newobj(text='route symbol')
    fromsymbol_170[0] = route_symbol_168[0]  # type = ?
    coll_vectors_t_33 = newobj(text='coll Vectors_T')
    route_symbol_168[0] = coll_vectors_t_33[0]  # type = ?
    p_tmatrixdone_52 = _subpatcher(subpatcher_id=8, text='p TMatrixDone')
    button_25 = button()
    p_tmatrixdone_52[0] = button_25[0]  # type = bang
    p_tmatrixtovector_37 = _subpatcher(subpatcher_id=15, text='p T-MatrixtoVector')
    coll_vectors_t_33[0] = p_tmatrixtovector_37[0]  # type = ?
    button_25[0] = p_tmatrixtovector_37[1]  # type = int
    bpatcher_9 = _subpatcher(embed=1, lockeddragscroll=0, subpatcher_id=19, viewvisibility=1)
    p_tmatrixtovector_37[0] = bpatcher_9[0]  # type = ?
    p_tmatrixtovector_37[1] = bpatcher_9[1]  # type = jit_matrix
    send_188 = send(text='s Rythm')
    message_191 = message(presentation_linecount=2, text='642.762329 160.690598 482.071686 642.762329 803.452881 964.143494 1285.524658 1446.21521 321.381195 1446.')
    fromsymbol_190 = fromsymbol(text='fromsymbol')
    send_188[0] = fromsymbol_190[0]  # type = ?
    message_191[1] = fromsymbol_190[0]  # type = ?
    route_symbol_189 = newobj(text='route symbol')
    fromsymbol_190[0] = route_symbol_189[0]  # type = ?
    coll_vectors_rhy_56 = newobj(text='coll Vectors_Rhy')
    route_symbol_189[0] = coll_vectors_rhy_56[0]  # type = ?
    p_rmatrixdone_204 = _subpatcher(subpatcher_id=9, text='p RMatrixDone')
    button_53 = button()
    p_rmatrixdone_204[0] = button_53[0]  # type = bang
    p_rhymatrixtovector_60 = _subpatcher(subpatcher_id=12, text='p Rhy-MatrixtoVector')
    coll_vectors_rhy_56[0] = p_rhymatrixtovector_60[0]  # type = ?
    button_53[0] = p_rhymatrixtovector_60[1]  # type = int
    bpatcher_85 = _subpatcher(embed=1, lockeddragscroll=0, subpatcher_id=13, viewvisibility=1)
    p_rhymatrixtovector_60[0] = bpatcher_85[0]  # type = int
    p_rhymatrixtovector_60[1] = bpatcher_85[1]  # type = jit_matrix
    p_rhythmcalculation_111 = _subpatcher(subpatcher_id=11, text='p RhythmCalculation')
    bpatcher_85[0] = p_rhythmcalculation_111[0]  # type = ?
    bpatcher_85[1] = p_rhythmcalculation_111[1]  # type = ?
    bpatcher_85[2] = p_rhythmcalculation_111[2]  # type = ?
    bpatcher_85[3] = p_rhythmcalculation_111[3]  # type = ?
    bpatcher_85[4] = p_rhythmcalculation_111[4]  # type = ?
    bpatcher_85[5] = p_rhythmcalculation_111[5]  # type = ?
    bpatcher_85[6] = p_rhythmcalculation_111[6]  # type = ?
    bpatcher_85[7] = p_rhythmcalculation_111[7]  # type = ?
    bpatcher_85[8] = p_rhythmcalculation_111[8]  # type = ?
    bpatcher_85[9] = p_rhythmcalculation_111[9]  # type = ?
    bpatcher_85[10] = p_rhythmcalculation_111[10]  # type = ?
    bpatcher_85[11] = p_rhythmcalculation_111[11]  # type = ?
    bpatcher_85[12] = p_rhythmcalculation_111[12]  # type = ?
    bpatcher_85[13] = p_rhythmcalculation_111[13]  # type = ?
    p_rhythm_62 = _subpatcher(subpatcher_id=16, text='p Rhythm')
    p_rhythmcalculation_111[0] = p_rhythm_62[0]  # type = ?
    send_69 = send(text='s Reinicio-cont')
    button_64 = button()
    send_69[0] = button_64[0]  # type = bang
    send_180 = send(text='s Inverse')
    gswitch2_267 = gswitch2(int=1, parameter_enable=0)
    send_180[0] = gswitch2_267[0]  # type = ?
    button_64[0] = gswitch2_267[0]  # type = ?
    message_187 = message(text='54 56 57 58 60 51 53')
    fromsymbol_185 = fromsymbol(text='fromsymbol')
    message_187[1] = fromsymbol_185[0]  # type = ?
    gswitch2_267[1] = fromsymbol_185[0]  # type = ?
    route_symbol_182 = newobj(text='route symbol')
    fromsymbol_185[0] = route_symbol_182[0]  # type = ?
    coll_vectors_i_49 = newobj(text='coll Vectors_I')
    route_symbol_182[0] = coll_vectors_i_49[0]  # type = ?
    p_imatrixdone_274 = _subpatcher(subpatcher_id=2, text='p IMatrixDone')
    button_46 = button()
    p_imatrixdone_274[0] = button_46[0]  # type = bang
    p_imatrixtovector_51 = _subpatcher(subpatcher_id=14, text='p I-MatrixtoVector')
    coll_vectors_i_49[0] = p_imatrixtovector_51[0]  # type = ?
    button_46[0] = p_imatrixtovector_51[1]  # type = int
    bpatcher_28 = _subpatcher(embed=1, lockeddragscroll=0, subpatcher_id=18, viewvisibility=1)
    p_imatrixtovector_51[0] = bpatcher_28[0]  # type = ?
    p_imatrixtovector_51[1] = bpatcher_28[1]  # type = jit_matrix
    button_47 = button()
    send_166 = send(text='s PrimeForm')
    send_8 = send(text='s Reinicio-cont')
    button_24 = button()
    send_8[0] = button_24[0]  # type = bang
    gswitch2_192 = gswitch2(parameter_enable=0)
    send_166[0] = gswitch2_192[0]  # type = ?
    button_24[0] = gswitch2_192[0]  # type = ?
    message_160 = message(text='72 73 74 76 77 78 80')
    fromsymbol_164 = fromsymbol(text='fromsymbol')
    message_160[1] = fromsymbol_164[0]  # type = ?
    gswitch2_192[1] = fromsymbol_164[0]  # type = ?
    route_symbol_165 = newobj(text='route symbol')
    fromsymbol_164[0] = route_symbol_165[0]  # type = ?
    coll_vectors_45 = newobj(text='coll Vectors')
    route_symbol_165[0] = coll_vectors_45[0]  # type = ?
    p_matrixtovector_44 = _subpatcher(subpatcher_id=17, text='p MatrixtoVector')
    coll_vectors_45[0] = p_matrixtovector_44[0]  # type = ?
    button_47[0] = p_matrixtovector_44[1]  # type = int
    bpatcher_7 = _subpatcher(embed=1, lockeddragscroll=0, subpatcher_id=20, viewvisibility=1)
    p_matrixtovector_44[0] = bpatcher_7[0]  # type = ?
    p_matrixtovector_44[1] = bpatcher_7[1]  # type = jit_matrix
    number_176 = number(parameter_enable=0)
    bpatcher_28[5] = number_176[0]  # type = ?
    p_rhythm_62[5] = number_176[0]  # type = ?
    bpatcher_7[5] = number_176[0]  # type = ?
    bpatcher_9[5] = number_176[0]  # type = ?
    number_175 = number(parameter_enable=0)
    bpatcher_28[6] = number_175[0]  # type = ?
    p_rhythm_62[6] = number_175[0]  # type = ?
    bpatcher_7[6] = number_175[0]  # type = ?
    bpatcher_9[6] = number_175[0]  # type = ?
    number_173 = number(parameter_enable=0)
    bpatcher_28[2] = number_173[0]  # type = ?
    p_rhythm_62[2] = number_173[0]  # type = ?
    bpatcher_7[2] = number_173[0]  # type = ?
    bpatcher_9[2] = number_173[0]  # type = ?
    number_172 = number(parameter_enable=0)
    bpatcher_28[3] = number_172[0]  # type = ?
    p_rhythm_62[3] = number_172[0]  # type = ?
    bpatcher_7[3] = number_172[0]  # type = ?
    bpatcher_9[3] = number_172[0]  # type = ?
    number_171 = number(parameter_enable=0)
    bpatcher_28[1] = number_171[0]  # type = ?
    p_rhythm_62[1] = number_171[0]  # type = ?
    bpatcher_7[1] = number_171[0]  # type = ?
    bpatcher_9[1] = number_171[0]  # type = ?
    number_169 = number(parameter_enable=0)
    bpatcher_28[0] = number_169[0]  # type = ?
    p_rhythm_62[0] = number_169[0]  # type = ?
    bpatcher_7[0] = number_169[0]  # type = ?
    bpatcher_9[0] = number_169[0]  # type = ?
    number_178 = number(parameter_enable=0)
    bpatcher_28[8] = number_178[0]  # type = ?
    p_rhythm_62[8] = number_178[0]  # type = ?
    bpatcher_7[8] = number_178[0]  # type = ?
    bpatcher_9[8] = number_178[0]  # type = ?
    number_174 = number(parameter_enable=0)
    bpatcher_28[7] = number_174[0]  # type = ?
    p_rhythm_62[7] = number_174[0]  # type = ?
    bpatcher_7[7] = number_174[0]  # type = ?
    bpatcher_9[7] = number_174[0]  # type = ?
    number_177 = number(parameter_enable=0)
    bpatcher_28[4] = number_177[0]  # type = ?
    p_rhythm_62[4] = number_177[0]  # type = ?
    bpatcher_7[4] = number_177[0]  # type = ?
    bpatcher_9[4] = number_177[0]  # type = ?
    p_pcset9_114 = _subpatcher(subpatcher_id=21, text='p PCSet9')
    number_169[0] = p_pcset9_114[0]  # type = ?
    number_171[0] = p_pcset9_114[1]  # type = ?
    number_173[0] = p_pcset9_114[2]  # type = ?
    number_172[0] = p_pcset9_114[3]  # type = ?
    number_177[0] = p_pcset9_114[4]  # type = ?
    number_176[0] = p_pcset9_114[5]  # type = ?
    number_175[0] = p_pcset9_114[6]  # type = ?
    number_174[0] = p_pcset9_114[7]  # type = ?
    number_178[0] = p_pcset9_114[8]  # type = ?
    number_17 = number(parameter_enable=0)
    p_pcset9_114[0] = number_17[0]  # type = ?
    send_141[0] = number_17[0]  # type = ?
    unpack_i_l_i_6 = newobj(text='unpack i l i')
    number_17[0] = unpack_i_l_i_6[0]  # type = int
    number_18[0] = unpack_i_l_i_6[2]  # type = int
    fromsymbol_1 = fromsymbol(text='fromsymbol')
    unpack_i_l_i_6[0] = fromsymbol_1[0]  # type = ?
    message_34 = message(text='"7 - 13"')
    fromsymbol_1[0] = message_34[0]  # type = ?
    t_b_l_3 = newobj(text='t b l')
    message_34[0] = t_b_l_3[0]  # type = bang
    message_34[1] = t_b_l_3[1]  # type = ?
    number_41 = number(parameter_enable=0)
    number_43 = number(parameter_enable=0)
    number_40 = number(parameter_enable=0)
    number_39 = number(parameter_enable=0)
    number_38 = number(parameter_enable=0)
    number_42 = number(parameter_enable=0)
    unpack_i_i_i_i_i_i_22 = newobj(text='unpack i i i i i i')
    number_43[0] = unpack_i_i_i_i_i_i_22[0]  # type = int
    number_42[0] = unpack_i_i_i_i_i_i_22[1]  # type = int
    number_41[0] = unpack_i_i_i_i_i_i_22[2]  # type = int
    number_40[0] = unpack_i_i_i_i_i_i_22[3]  # type = int
    number_39[0] = unpack_i_i_i_i_i_i_22[4]  # type = int
    number_38[0] = unpack_i_i_i_i_i_i_22[5]  # type = int
    fromsymbol_21 = fromsymbol(text='fromsymbol')
    unpack_i_i_i_i_i_i_22[0] = fromsymbol_21[0]  # type = ?
    message_36 = message(text='"4 4 3 5 3 2"')
    fromsymbol_21[0] = message_36[0]  # type = ?
    t_b_l_4 = newobj(text='t b l')
    message_36[0] = t_b_l_4[0]  # type = bang
    message_36[1] = t_b_l_4[1]  # type = ?
    fromsymbol_20 = fromsymbol(text='fromsymbol')
    p_pcset9_114[1] = fromsymbol_20[0]  # type = ?
    message_35 = message(text='"0 1 2 4 5 6 8"')
    fromsymbol_20[0] = message_35[0]  # type = ?
    t_b_l_5 = newobj(text='t b l')
    message_35[0] = t_b_l_5[0]  # type = bang
    message_35[1] = t_b_l_5[1]  # type = ?
    unpack_l_l_l_32 = newobj(text='unpack l l l')
    t_b_l_3[0] = unpack_l_l_l_32[0]  # type = ?
    t_b_l_5[0] = unpack_l_l_l_32[1]  # type = ?
    t_b_l_4[0] = unpack_l_l_l_32[2]  # type = ?
    coll_pcset_19 = newobj(text='coll PcSet')
    unpack_l_l_l_32[0] = coll_pcset_19[0]  # type = ?
    number_110 = number(parameter_enable=0)
    coll_pcset_19[0] = number_110[0]  # type = ?
    send_124 = send(text='s VarFP')
    number_163 = number(parameter_enable=0)
    send_124[0] = number_163[0]  # type = ?
    p_no_silence_162 = _subpatcher(subpatcher_id=0, text='p no silence')
    number_163[0] = p_no_silence_162[0]  # type = int
    zlchange_155 = newobj(text='zl.change')
    p_no_silence_162[0] = zlchange_155[0]  # type = ?
    number_26 = number(parameter_enable=0)
    zlchange_155[0] = number_26[0]  # type = ?
    coll_pcset_19[0] = number_26[0]  # type = ?
    send_86 = send(text='s PcComplement')
    number_242 = number(parameter_enable=0)
    send_86[0] = number_242[0]  # type = ?
    send_125 = send(text='s VarT')
    number_232 = number(parameter_enable=0)
    send_125[0] = number_232[0]  # type = ?
    p_incontrol_263 = _subpatcher(subpatcher_id=6, text='p InControl')
    send_195[0] = p_incontrol_263[0]  # type = ?
    number_26[0] = p_incontrol_263[0]  # type = ?
    number_110[0] = p_incontrol_263[1]  # type = ?
    number_163[0] = p_incontrol_263[2]  # type = ?
    flonum_61[0] = p_incontrol_263[3]  # type = ?
    number_193[0] = p_incontrol_263[4]  # type = ?
    number_194[0] = p_incontrol_263[5]  # type = ?
    number_153[0] = p_incontrol_263[6]  # type = ?
    number_232[0] = p_incontrol_263[7]  # type = ?
    number_211[0] = p_incontrol_263[8]  # type = ?
    number_238[0] = p_incontrol_263[9]  # type = ?
    number_242[0] = p_incontrol_263[10]  # type = ?
    gate_4_133 = newobj(text='gate 4')
    p_incontrol_263[2] = gate_4_133[0]  # type = ?
    receive_139 = receive(text='r e2')
    gate_4_133[1] = receive_139[0]  # type = ?
    number_216 = number(parameter_enable=0)
    bpatcher_28[9] = number_216[0]  # type = ?
    gate_4_131 = newobj(text='gate 4')
    p_incontrol_263[0] = gate_4_131[0]  # type = ?
    p_incontrol_263[4] = gate_4_131[1]  # type = ?
    p_incontrol_263[6] = gate_4_131[2]  # type = ?
    p_incontrol_263[8] = gate_4_131[3]  # type = ?
    gate_4_135 = newobj(text='gate 4')
    p_incontrol_263[3] = gate_4_135[0]  # type = ?
    p_incontrol_263[3] = gate_4_135[1]  # type = ?
    p_incontrol_263[3] = gate_4_135[2]  # type = ?
    gate_4_137 = newobj(text='gate 4')
    gate_4_134 = newobj(text='gate 4')
    p_incontrol_263[1] = gate_4_134[0]  # type = ?
    p_incontrol_263[5] = gate_4_134[1]  # type = ?
    p_incontrol_263[7] = gate_4_134[2]  # type = ?
    gate_4_136 = newobj(text='gate 4')
    message_127 = message(text='2')
    gate_4_131[0] = message_127[0]  # type = ?
    gate_4_133[0] = message_127[0]  # type = ?
    gate_4_134[0] = message_127[0]  # type = ?
    gate_4_135[0] = message_127[0]  # type = ?
    gate_4_136[0] = message_127[0]  # type = ?
    gate_4_137[0] = message_127[0]  # type = ?
    send_63 = send(text='s bang2')
    button_90 = button()
    message_127[0] = button_90[0]  # type = bang
    send_63[0] = button_90[0]  # type = bang
    send_65 = send(text='s bang3')
    message_128 = message(text='3')
    gate_4_131[0] = message_128[0]  # type = ?
    gate_4_133[0] = message_128[0]  # type = ?
    gate_4_134[0] = message_128[0]  # type = ?
    gate_4_135[0] = message_128[0]  # type = ?
    gate_4_136[0] = message_128[0]  # type = ?
    gate_4_137[0] = message_128[0]  # type = ?
    button_92 = button()
    message_128[0] = button_92[0]  # type = bang
    send_65[0] = button_92[0]  # type = bang
    send_31 = send(text='s bang1')
    message_126 = message(text='1')
    gate_4_131[0] = message_126[0]  # type = ?
    gate_4_133[0] = message_126[0]  # type = ?
    gate_4_134[0] = message_126[0]  # type = ?
    gate_4_135[0] = message_126[0]  # type = ?
    gate_4_136[0] = message_126[0]  # type = ?
    gate_4_137[0] = message_126[0]  # type = ?
    button_84 = button()
    message_126[0] = button_84[0]  # type = bang
    send_31[0] = button_84[0]  # type = bang
    message_129 = message(text='4')
    gate_4_131[0] = message_129[0]  # type = ?
    gate_4_133[0] = message_129[0]  # type = ?
    gate_4_134[0] = message_129[0]  # type = ?
    gate_4_135[0] = message_129[0]  # type = ?
    gate_4_136[0] = message_129[0]  # type = ?
    gate_4_137[0] = message_129[0]  # type = ?
    send_67 = send(text='s bang4')
    button_94 = button()
    message_129[0] = button_94[0]  # type = bang
    send_67[0] = button_94[0]  # type = bang
    sel_1_2_3_4_68 = newobj(text='sel 1 2 3 4')
    button_84[0] = sel_1_2_3_4_68[0]  # type = bang
    button_90[0] = sel_1_2_3_4_68[1]  # type = bang
    button_92[0] = sel_1_2_3_4_68[2]  # type = bang
    button_94[0] = sel_1_2_3_4_68[3]  # type = bang
    number_66 = number(parameter_enable=0)
    sel_1_2_3_4_68[0] = number_66[0]  # type = ?
    message_148 = message(text='1')
    number_66[0] = message_148[0]  # type = ?
    receive_87 = receive(text='r f1')
    message_148[0] = receive_87[0]  # type = ?
    receive_100 = receive(text='r ent2')
    gate_4_135[1] = receive_100[0]  # type = ?
    receive_149 = receive(text='r VarFP')
    coll_vectors_45[0] = receive_149[0]  # type = ?
    flonum_122 = flonum(format=6, parameter_enable=0)
    p_rhythm_62[9] = flonum_122[0]  # type = ?
    message_48 = message(text='open')
    coll_vectors_i_49[0] = message_48[0]  # type = ?
    message_23 = message(text='open')
    coll_pcset_19[0] = message_23[0]  # type = ?
    message_50 = message(text='open')
    coll_vectors_45[0] = message_50[0]  # type = ?
    receive_99 = receive(text='r ent3')
    gate_4_137[1] = receive_99[0]  # type = ?
    receive_199 = receive(text='r VarInit')
    coll_vectors_rhy_56[0] = receive_199[0]  # type = ?
    receive_146 = receive(text='r VarI_T')
    coll_vectors_i_49[0] = receive_146[0]  # type = ?
    receive_215 = receive(text='r I-Tr')
    number_216[0] = receive_215[0]  # type = ?
    p_inout1_77 = _subpatcher(subpatcher_id=5, text='p InOut1')
    gswitch2_192[0] = p_inout1_77[0]  # type = ?
    receive_140 = receive(text='r e3')
    gate_4_134[1] = receive_140[0]  # type = ?
    thispatcher_186 = thispatcher(save=['#N', 'thispatcher', ';', '#Q', 'end', ';'], text='thispatcher')
    message_184 = message(text='presentation 1')
    thispatcher_186[0] = message_184[0]  # type = ?
    toggle_159 = toggle(parameter_enable=0)
    message_184[0] = toggle_159[0]  # type = int
    loadmess_1_156 = newobj(text='loadmess 1')
    toggle_159[0] = loadmess_1_156[0]  # type = ?
    message_151 = message(text='3')
    number_66[0] = message_151[0]  # type = ?
    receive_30 = receive(text='r VarInit')
    coll_vectors_45[0] = receive_30[0]  # type = ?
    message_201 = message(text='max quit')
    closebang_197 = closebang(text='closebang')
    message_201[0] = closebang_197[0]  # type = bang
    message_29 = message(text='open')
    coll_vectors_t_33[0] = message_29[0]  # type = ?
    message_54 = message(text='open')
    coll_vectors_rhy_56[0] = message_54[0]  # type = ?
    receive_158 = receive(text='r VarRy')
    coll_vectors_rhy_56[0] = receive_158[0]  # type = ?
    number_27 = number(parameter_enable=0)
    bpatcher_9[9] = number_27[0]  # type = ?
    receive_120 = receive(text='r Rhy')
    flonum_122[0] = receive_120[0]  # type = ?
    message_152 = message(text='4')
    number_66[0] = message_152[0]  # type = ?
    p_inout2_203 = _subpatcher(subpatcher_id=4, text='p InOut2')
    gswitch2_266[0] = p_inout2_203[0]  # type = ?
    receive_147 = receive(text='r VarT')
    coll_vectors_t_33[0] = receive_147[0]  # type = ?
    send_113 = send(text='s ComplSet')
    number_104 = number(parameter_enable=0)
    send_113[0] = number_104[0]  # type = ?
    p_pcsetcomp_81 = _subpatcher(subpatcher_id=10, text='p PCSetComp')
    number_104[0] = p_pcsetcomp_81[0]  # type = int
    receive_102 = receive(text='r PcComplement')
    p_pcsetcomp_81[1] = receive_102[0]  # type = ?
    message_150 = message(text='2')
    number_66[0] = message_150[0]  # type = ?
    coll_complement_95 = newobj(text='coll Complement')
    p_pcsetcomp_81[0] = coll_complement_95[0]  # type = ?
    message_97 = message(text='open')
    coll_complement_95[0] = message_97[0]  # type = ?
    receive_98 = receive(text='r ent1')
    gate_4_136[1] = receive_98[0]  # type = ?
    receive_214 = receive(text='r Tr')
    number_27[0] = receive_214[0]  # type = ?
    thispatcher_207 = thispatcher(save=['#N', 'thispatcher', ';', '#Q', 'end', ';'], text='thispatcher')
    message_206 = message(text='window flags close, window exec')
    thispatcher_207[0] = message_206[0]  # type = ?
    receive_181 = receive(text='r e1')
    gate_4_131[1] = receive_181[0]  # type = ?
    receive_196 = receive(text='r PrimeSet')
    coll_complement_95[0] = receive_196[0]  # type = ?
    receive_93 = receive(text='r f4')
    message_152[0] = receive_93[0]  # type = ?
    p_inout3_222 = _subpatcher(subpatcher_id=3, text='p InOut3')
    gswitch2_267[0] = p_inout3_222[0]  # type = ?
    receive_91 = receive(text='r f3')
    message_151[0] = receive_91[0]  # type = ?
    receive_88 = receive(text='r f2')
    message_150[0] = receive_88[0]  # type = ?
    loadbang_202 = loadbang(text='loadbang')
    message_206[0] = loadbang_202[0]  # type = bang
    receive_275 = receive(text='r IInit')
    coll_vectors_i_49[0] = receive_275[0]  # type = ?
    receive_198 = receive(text='r TInit')
    coll_vectors_t_33[0] = receive_198[0]  # type = ?
    bpatcher_10 = _subpatcher(embed=1, lockeddragscroll=0, subpatcher_id=1, viewvisibility=1)
# (comment) Interval Vector
# (comment) Complement Chosen
# (comment) 3.1)
# (comment) 3.1.2)
# (comment) 3.1.3)
# (comment) 3.2)
# (comment) 1-a)
# (comment) 3.2.1.)
# (comment) 3.2.2.)
# (comment) 3.2.3.)
# (comment) 3.4.)
# (comment) 3.4.2.)
# (comment) 1-b)
# (comment) 3.2.3.1.)
# (comment) 3.3.1.)
# (comment) 1-c)
# (comment) 3.3)
# (comment) 3.3.2.)
# (comment) 2)
# (comment) 3.3.3.)
# (comment) 3.4.3.)
# (comment) 3.4.4.)
# (comment) 3.4.5.)
# (comment) 3.1.1)
# (comment) 3.4.1.)
# (comment) 3)
# (comment) Prime Form
# (comment) 3.4.5.1.)
# (comment) Prime Form Variation
# (comment) Rhythm Base
# (comment) Rhythm Variation
# (comment) Range
# (comment) Transpos Base
# (comment) Transpos Variation
# (comment) Inversion Base
# (comment) Inversion Variation
# (comment) Complement
# (comment) Complement
# (comment) 3.3.3.1.)
# (comment) Cardinal
# (comment) PcSet
    p_inoutcontrol_55 = _subpatcher(subpatcher_id=7, text='p InOutControl')

    #----------------------------------------------------------------------
    def subpatcher_0(sp0_inlet_161):
        sp0_outlet_2 = outlet(comment='', index=1)
        sp0___1_1 = newobj(text='+ 1')
        sp0_outlet_2[0] = sp0___1_1[0]  # type = int
        sp0_random_8_156 = newobj(text='random 8')
        sp0___1_1[0] = sp0_random_8_156[0]  # type = int
        sp0___1_3 = newobj(text='- 1')
        sp0_random_8_156[1] = sp0___1_3[0]  # type = int
        sp0_receive_142 = receive(text='r cardinal')
        sp0___1_3[0] = sp0_receive_142[0]  # type = ?
        sp0_button_159 = button()
        sp0_random_8_156[0] = sp0_button_159[0]  # type = bang
        sp0_inlet_161 = inlet(comment='', index=1)
        sp0_button_159[0] = sp0_inlet_161[0]  # type = ?
        return sp0_outlet_2

    #----------------------------------------------------------------------
    def subpatcher_1(sp1_inlet_144, sp1_inlet_112, sp1_inlet_109, sp1_inlet_137):
        sp1_send_21 = send(text='s e1')
        sp1_outlet_9 = outlet(comment='', index=5)
        sp1_flonum_110 = flonum(format=6, parameter_enable=0)
        sp1_send_21[0] = sp1_flonum_110[0]  # type = ?
        sp1_outlet_9[0] = sp1_flonum_110[0]  # type = ?
        sp1_gswitch_179 = gswitch(parameter_enable=0)
        sp1_flonum_110[0] = sp1_gswitch_179[0]  # type = ?
        sp1_send_37 = send(text='s e2')
        sp1_outlet_12 = outlet(comment='', index=7)
        sp1_flonum_106 = flonum(format=6, parameter_enable=0)
        sp1_outlet_12[0] = sp1_flonum_106[0]  # type = ?
        sp1_send_37[0] = sp1_flonum_106[0]  # type = ?
        sp1_gswitch_187 = gswitch(parameter_enable=0)
        sp1_flonum_106[0] = sp1_gswitch_187[0]  # type = ?
        sp1_message_189 = message(text='0.28')
        sp1_gswitch_187[2] = sp1_message_189[0]  # type = ?
        sp1_send_19 = send(text='s f3')
        sp1_outlet_8 = outlet(comment='', index=3)
        sp1_number_31 = number(parameter_enable=0)
        sp1_send_19[0] = sp1_number_31[0]  # type = ?
        sp1_outlet_8[0] = sp1_number_31[0]  # type = ?
        sp1_p_outputcontrol_87 = _subpatcher(subpatcher_id=1, text='p Outputcontrol')
        sp1_number_31[0] = sp1_p_outputcontrol_87[0]  # type = int
        sp1_outlet_66 = outlet(comment='', index=12)
        sp1_send_40 = send(text='s ent4')
        sp1_flonum_70 = flonum(format=6, parameter_enable=0)
        sp1_send_40[0] = sp1_flonum_70[0]  # type = ?
        sp1_outlet_66[0] = sp1_flonum_70[0]  # type = ?
        sp1_gswitch_45 = gswitch(parameter_enable=0)
        sp1_flonum_70[0] = sp1_gswitch_45[0]  # type = ?
        sp1_message_171 = message(text='0')
        sp1_p_outputcontrol_87[1] = sp1_message_171[0]  # type = ?
        sp1_send_18 = send(text='s f2')
        sp1_outlet_7 = outlet(comment='', index=2)
        sp1_number_29 = number(parameter_enable=0)
        sp1_send_18[0] = sp1_number_29[0]  # type = ?
        sp1_outlet_7[0] = sp1_number_29[0]  # type = ?
        sp1_p_outputcontrol_82 = _subpatcher(subpatcher_id=2, text='p Outputcontrol')
        sp1_number_29[0] = sp1_p_outputcontrol_82[0]  # type = int
        sp1_message_164 = message(text='0')
        sp1_p_outputcontrol_82[1] = sp1_message_164[0]  # type = ?
        sp1_send_39 = send(text='s ent2')
        sp1_outlet_13 = outlet(comment='', index=8)
        sp1_flonum_176 = flonum(format=6, parameter_enable=0)
        sp1_outlet_13[0] = sp1_flonum_176[0]  # type = ?
        sp1_send_39[0] = sp1_flonum_176[0]  # type = ?
        sp1_gswitch_191 = gswitch(parameter_enable=0)
        sp1_flonum_176[0] = sp1_gswitch_191[0]  # type = ?
        sp1_message_193 = message(text='0.019')
        sp1_gswitch_191[2] = sp1_message_193[0]  # type = ?
        sp1_outlet_10 = outlet(comment='', index=4)
        sp1_send_20 = send(text='s f4')
        sp1_number_34 = number(parameter_enable=0)
        sp1_outlet_10[0] = sp1_number_34[0]  # type = ?
        sp1_send_20[0] = sp1_number_34[0]  # type = ?
        sp1_p_outputcontrol_91 = _subpatcher(subpatcher_id=0, text='p Outputcontrol')
        sp1_number_34[0] = sp1_p_outputcontrol_91[0]  # type = int
        sp1_message_177 = message(text='0')
        sp1_p_outputcontrol_91[1] = sp1_message_177[0]  # type = ?
        sp1_message_181 = message(text='0.46')
        sp1_gswitch_179[2] = sp1_message_181[0]  # type = ?
        sp1_send_22 = send(text='s ent1')
        sp1_outlet_11 = outlet(comment='', index=6)
        sp1_flonum_168 = flonum(format=6, parameter_enable=0)
        sp1_outlet_11[0] = sp1_flonum_168[0]  # type = ?
        sp1_send_22[0] = sp1_flonum_168[0]  # type = ?
        sp1_gswitch_183 = gswitch(parameter_enable=0)
        sp1_flonum_168[0] = sp1_gswitch_183[0]  # type = ?
        sp1_message_185 = message(text='0.66')
        sp1_gswitch_183[2] = sp1_message_185[0]  # type = ?
        sp1_outlet_6 = outlet(comment='', index=1)
        sp1_send_16 = send(text='s f1')
        sp1_number_25 = number(parameter_enable=0)
        sp1_send_16[0] = sp1_number_25[0]  # type = ?
        sp1_outlet_6[0] = sp1_number_25[0]  # type = ?
        sp1_p_outputcontrol_78 = _subpatcher(subpatcher_id=3, text='p Outputcontrol')
        sp1_number_25[0] = sp1_p_outputcontrol_78[0]  # type = int
        sp1_message_118 = message(text='1')
        sp1_p_outputcontrol_78[1] = sp1_message_118[0]  # type = ?
        sp1_metro_250_57 = newobj(text='metro 250')
        sp1_message_118[0] = sp1_metro_250_57[0]  # type = bang
        sp1_message_164[0] = sp1_metro_250_57[0]  # type = bang
        sp1_message_171[0] = sp1_metro_250_57[0]  # type = bang
        sp1_message_177[0] = sp1_metro_250_57[0]  # type = bang
        sp1_message_181[0] = sp1_metro_250_57[0]  # type = bang
        sp1_message_185[0] = sp1_metro_250_57[0]  # type = bang
        sp1_message_189[0] = sp1_metro_250_57[0]  # type = bang
        sp1_message_193[0] = sp1_metro_250_57[0]  # type = bang
        sp1_inlet_112 = inlet(comment='', index=3)
        sp1_metro_250_57[1] = sp1_inlet_112[0]  # type = ?
        sp1_message_141 = message(text='0')
        sp1_message_118[1] = sp1_message_141[0]  # type = ?
        sp1_message_164[1] = sp1_message_141[0]  # type = ?
        sp1_message_171[1] = sp1_message_141[0]  # type = ?
        sp1_if_i1__1_then_1_30 = newobj(text='if $i1 == 1 then 1')
        sp1_message_141[0] = sp1_if_i1__1_then_1_30[0]  # type = ?
        sp1_message_177[1] = sp1_if_i1__1_then_1_30[0]  # type = ?
        sp1_message_23 = message(text='0')
        sp1_message_118[1] = sp1_message_23[0]  # type = ?
        sp1_message_171[1] = sp1_message_23[0]  # type = ?
        sp1_message_177[1] = sp1_message_23[0]  # type = ?
        sp1_if_i1__1_then_1_24 = newobj(text='if $i1 == 1 then 1')
        sp1_message_164[1] = sp1_if_i1__1_then_1_24[0]  # type = ?
        sp1_message_23[0] = sp1_if_i1__1_then_1_24[0]  # type = ?
        sp1_message_26 = message(text='0')
        sp1_message_118[1] = sp1_message_26[0]  # type = ?
        sp1_message_164[1] = sp1_message_26[0]  # type = ?
        sp1_message_177[1] = sp1_message_26[0]  # type = ?
        sp1_if_i1__1_then_1_27 = newobj(text='if $i1 == 1 then 1')
        sp1_message_171[1] = sp1_if_i1__1_then_1_27[0]  # type = ?
        sp1_message_26[0] = sp1_if_i1__1_then_1_27[0]  # type = ?
        sp1_message_17 = message(text='0')
        sp1_message_164[1] = sp1_message_17[0]  # type = ?
        sp1_message_171[1] = sp1_message_17[0]  # type = ?
        sp1_message_177[1] = sp1_message_17[0]  # type = ?
        sp1_if_i1__1_then_1_5 = newobj(text='if $i1 == 1 then 1')
        sp1_message_118[1] = sp1_if_i1__1_then_1_5[0]  # type = ?
        sp1_message_17[0] = sp1_if_i1__1_then_1_5[0]  # type = ?
        sp1_gate_4_126 = newobj(text='gate 4')
        sp1_if_i1__1_then_1_5[0] = sp1_gate_4_126[0]  # type = ?
        sp1_if_i1__1_then_1_24[0] = sp1_gate_4_126[1]  # type = ?
        sp1_if_i1__1_then_1_27[0] = sp1_gate_4_126[2]  # type = ?
        sp1_if_i1__1_then_1_30[0] = sp1_gate_4_126[3]  # type = ?
        sp1_message_128 = message(text='1')
        sp1_gate_4_126[0] = sp1_message_128[0]  # type = ?
        sp1_outlet_68 = outlet(comment='', index=10)
        sp1_send_63 = send(text='s ent3')
        sp1_flonum_72 = flonum(format=6, parameter_enable=0)
        sp1_send_63[0] = sp1_flonum_72[0]  # type = ?
        sp1_outlet_68[0] = sp1_flonum_72[0]  # type = ?
        sp1_gswitch_54 = gswitch(parameter_enable=0)
        sp1_flonum_72[0] = sp1_gswitch_54[0]  # type = ?
        sp1_message_56 = message(text='0.079')
        sp1_gswitch_54[2] = sp1_message_56[0]  # type = ?
        sp1_flonum_55 = flonum(format=6, parameter_enable=0)
        sp1_gswitch_54[1] = sp1_flonum_55[0]  # type = ?
        sp1_fromsymbol_58 = fromsymbol(text='fromsymbol')
        sp1_flonum_55[0] = sp1_fromsymbol_58[0]  # type = ?
        sp1_message_56[1] = sp1_fromsymbol_58[0]  # type = ?
        sp1_message_47 = message(text='0.069161')
        sp1_gswitch_45[2] = sp1_message_47[0]  # type = ?
        sp1_flonum_46 = flonum(format=6, parameter_enable=0)
        sp1_gswitch_45[1] = sp1_flonum_46[0]  # type = ?
        sp1_fromsymbol_48 = fromsymbol(text='fromsymbol')
        sp1_flonum_46[0] = sp1_fromsymbol_48[0]  # type = ?
        sp1_message_47[1] = sp1_fromsymbol_48[0]  # type = ?
        sp1_fromsymbol_178 = fromsymbol(text='fromsymbol')
        sp1_if_i1__1_then_1_30[0] = sp1_fromsymbol_178[0]  # type = ?
        sp1_toggle_53 = toggle(parameter_enable=0)
        sp1_gswitch_179[0] = sp1_toggle_53[0]  # type = int
        sp1_gswitch_183[0] = sp1_toggle_53[0]  # type = int
        sp1_gswitch_187[0] = sp1_toggle_53[0]  # type = int
        sp1_gswitch_191[0] = sp1_toggle_53[0]  # type = int
        sp1_metro_250_57[0] = sp1_toggle_53[0]  # type = int
        sp1_p_outputcontrol_78[0] = sp1_toggle_53[0]  # type = int
        sp1_p_outputcontrol_82[0] = sp1_toggle_53[0]  # type = int
        sp1_p_outputcontrol_87[0] = sp1_toggle_53[0]  # type = int
        sp1_p_outputcontrol_91[0] = sp1_toggle_53[0]  # type = int
        sp1_inlet_109 = inlet(comment='', index=2)
        sp1_toggle_53[0] = sp1_inlet_109[0]  # type = ?
        sp1_flonum_192 = flonum(format=6, parameter_enable=0)
        sp1_gswitch_191[1] = sp1_flonum_192[0]  # type = ?
        sp1_send_64 = send(text='s e3')
        sp1_outlet_69 = outlet(comment='', index=9)
        sp1_flonum_73 = flonum(format=6, parameter_enable=0)
        sp1_send_64[0] = sp1_flonum_73[0]  # type = ?
        sp1_outlet_69[0] = sp1_flonum_73[0]  # type = ?
        sp1_gswitch_59 = gswitch(parameter_enable=0)
        sp1_flonum_73[0] = sp1_gswitch_59[0]  # type = ?
        sp1_message_61 = message(text='0.064')
        sp1_gswitch_59[2] = sp1_message_61[0]  # type = ?
        sp1_flonum_60 = flonum(format=6, parameter_enable=0)
        sp1_gswitch_59[1] = sp1_flonum_60[0]  # type = ?
        sp1_fromsymbol_62 = fromsymbol(text='fromsymbol')
        sp1_flonum_60[0] = sp1_fromsymbol_62[0]  # type = ?
        sp1_message_61[1] = sp1_fromsymbol_62[0]  # type = ?
        sp1_flonum_92 = flonum(format=6, parameter_enable=0)
        sp1_gswitch_179[1] = sp1_flonum_92[0]  # type = ?
        sp1_flonum_184 = flonum(format=6, parameter_enable=0)
        sp1_gswitch_183[1] = sp1_flonum_184[0]  # type = ?
        sp1_fromsymbol_172 = fromsymbol(text='fromsymbol')
        sp1_if_i1__1_then_1_27[0] = sp1_fromsymbol_172[0]  # type = ?
        sp1_fromsymbol_186 = fromsymbol(text='fromsymbol')
        sp1_flonum_184[0] = sp1_fromsymbol_186[0]  # type = ?
        sp1_message_185[1] = sp1_fromsymbol_186[0]  # type = ?
        sp1_fromsymbol_182 = fromsymbol(text='fromsymbol')
        sp1_message_181[1] = sp1_fromsymbol_182[0]  # type = ?
        sp1_flonum_92[0] = sp1_fromsymbol_182[0]  # type = ?
        sp1_fromsymbol_194 = fromsymbol(text='fromsymbol')
        sp1_flonum_192[0] = sp1_fromsymbol_194[0]  # type = ?
        sp1_message_193[1] = sp1_fromsymbol_194[0]  # type = ?
        sp1_fromsymbol_85 = fromsymbol(text='fromsymbol')
        sp1_if_i1__1_then_1_5[0] = sp1_fromsymbol_85[0]  # type = ?
        sp1_fromsymbol_165 = fromsymbol(text='fromsymbol')
        sp1_if_i1__1_then_1_24[0] = sp1_fromsymbol_165[0]  # type = ?
        sp1_outlet_67 = outlet(comment='', index=11)
        sp1_send_42 = send(text='s e4')
        sp1_flonum_71 = flonum(format=6, parameter_enable=0)
        sp1_send_42[0] = sp1_flonum_71[0]  # type = ?
        sp1_outlet_67[0] = sp1_flonum_71[0]  # type = ?
        sp1_gswitch_49 = gswitch(parameter_enable=0)
        sp1_flonum_71[0] = sp1_gswitch_49[0]  # type = ?
        sp1_message_51 = message(text='0.054168')
        sp1_gswitch_49[2] = sp1_message_51[0]  # type = ?
        sp1_flonum_50 = flonum(format=6, parameter_enable=0)
        sp1_gswitch_49[1] = sp1_flonum_50[0]  # type = ?
        sp1_fromsymbol_52 = fromsymbol(text='fromsymbol')
        sp1_flonum_50[0] = sp1_fromsymbol_52[0]  # type = ?
        sp1_message_51[1] = sp1_fromsymbol_52[0]  # type = ?
        sp1_flonum_188 = flonum(format=6, parameter_enable=0)
        sp1_gswitch_187[1] = sp1_flonum_188[0]  # type = ?
        sp1_fromsymbol_190 = fromsymbol(text='fromsymbol')
        sp1_flonum_188[0] = sp1_fromsymbol_190[0]  # type = ?
        sp1_message_189[1] = sp1_fromsymbol_190[0]  # type = ?
        sp1_p_oscclassf_157 = _subpatcher(subpatcher_id=4, text='p OscClassf')
        sp1_fromsymbol_85[0] = sp1_p_oscclassf_157[0]  # type = ?
        sp1_fromsymbol_165[0] = sp1_p_oscclassf_157[1]  # type = ?
        sp1_fromsymbol_172[0] = sp1_p_oscclassf_157[2]  # type = ?
        sp1_fromsymbol_178[0] = sp1_p_oscclassf_157[3]  # type = ?
        sp1_fromsymbol_182[0] = sp1_p_oscclassf_157[4]  # type = ?
        sp1_fromsymbol_186[0] = sp1_p_oscclassf_157[5]  # type = ?
        sp1_fromsymbol_190[0] = sp1_p_oscclassf_157[6]  # type = ?
        sp1_fromsymbol_194[0] = sp1_p_oscclassf_157[7]  # type = ?
        sp1_fromsymbol_62[0] = sp1_p_oscclassf_157[8]  # type = ?
        sp1_fromsymbol_58[0] = sp1_p_oscclassf_157[9]  # type = ?
        sp1_fromsymbol_52[0] = sp1_p_oscclassf_157[10]  # type = ?
        sp1_fromsymbol_48[0] = sp1_p_oscclassf_157[11]  # type = ?
        sp1_inlet_144 = inlet(comment='', index=4)
        sp1_p_oscclassf_157[0] = sp1_inlet_144[0]  # type = ?
        sp1_message_115 = message(text='1')
        sp1_gate_4_126[1] = sp1_message_115[0]  # type = ?
        sp1_message_130 = message(text='3')
        sp1_gate_4_126[0] = sp1_message_130[0]  # type = ?
        sp1_button_93 = button()
        sp1_message_118[0] = sp1_button_93[0]  # type = bang
        sp1_message_164[0] = sp1_button_93[0]  # type = bang
        sp1_message_171[0] = sp1_button_93[0]  # type = bang
        sp1_message_177[0] = sp1_button_93[0]  # type = bang
        sp1_receive_227 = receive(text='r LastNote')
        sp1_button_93[0] = sp1_receive_227[0]  # type = ?
        sp1_message_132 = message(text='4')
        sp1_gate_4_126[0] = sp1_message_132[0]  # type = ?
        sp1_message_129 = message(text='2')
        sp1_gate_4_126[0] = sp1_message_129[0]  # type = ?
# (comment) Frecuency 1 -up
# (comment) Gets Data by Osc and de-encapsulatei it
# (comment) Reboot
# (comment) Frecuency 2 -right
# (comment) Frecuency 3 - down
# (comment) Frecuency 4 - left
# (comment) Energy Wave 1
# (comment) Entrophy Wave 1
# (comment) Entrophy Wave 2
# (comment) Energy Wave 2
# (comment) Entrophy Wave 3
# (comment) Energy Wave 3
# (comment) Entrophy Wave 4
# (comment) Energy Wave 4
        sp1_inlet_137 = inlet(comment='', index=1)
        sp1_message_117 = message(text='0')
        return sp1_outlet_66, sp1_outlet_67, sp1_outlet_68, sp1_outlet_69, sp1_outlet_13, sp1_outlet_12, sp1_outlet_11, sp1_outlet_10, sp1_outlet_9, sp1_outlet_8, sp1_outlet_7, sp1_outlet_6

    #----------------------------------------------------------------------
    def subpatcher_0(sp1_sp0_inlet_88, sp1_sp0_inlet_89):
        sp1_sp0_outlet_90 = outlet(comment='', index=1)
        sp1_sp0_int_103 = int(text='int')
        sp1_sp0_outlet_90[0] = sp1_sp0_int_103[0]  # type = int
        sp1_sp0_button_91 = button()
        sp1_sp0_int_103[0] = sp1_sp0_button_91[0]  # type = bang
        sp1_sp0_if_i1__1__i2__1_then_bang_94 = newobj(text='if $i1 == 1 & $i2 == 1 then bang')
        sp1_sp0_button_91[0] = sp1_sp0_if_i1__1__i2__1_then_bang_94[0]  # type = ?
        sp1_sp0_zlchange_96 = newobj(text='zl.change')
        sp1_sp0_if_i1__1__i2__1_then_bang_94[0] = sp1_sp0_zlchange_96[1]  # type = ?
        sp1_sp0_number_140 = number(parameter_enable=0)
        sp1_sp0_gswitch_173 = gswitch(parameter_enable=0)
        sp1_sp0_int_103[1] = sp1_sp0_gswitch_173[0]  # type = ?
        sp1_sp0_number_140[0] = sp1_sp0_gswitch_173[0]  # type = ?
        sp1_sp0_if_i1__1__i2__1_then_bang_94[1] = sp1_sp0_gswitch_173[0]  # type = ?
        sp1_sp0_zlchange_96[0] = sp1_sp0_gswitch_173[0]  # type = ?
        sp1_sp0_inlet_88 = inlet(comment='', index=1)
        sp1_sp0_gswitch_173[0] = sp1_sp0_inlet_88[0]  # type = int
        sp1_sp0_inlet_89 = inlet(comment='', index=2)
        sp1_sp0_gswitch_173[1] = sp1_sp0_inlet_89[0]  # type = ?
        return sp1_sp0_outlet_90

    #----------------------------------------------------------------------
    def subpatcher_1(sp1_sp1_inlet_83, sp1_sp1_inlet_84):
        sp1_sp1_outlet_86 = outlet(comment='', index=1)
        sp1_sp1_int_102 = int(text='int')
        sp1_sp1_outlet_86[0] = sp1_sp1_int_102[0]  # type = int
        sp1_sp1_button_87 = button()
        sp1_sp1_int_102[0] = sp1_sp1_button_87[0]  # type = bang
        sp1_sp1_if_i1__1__i2__1_then_bang_88 = newobj(text='if $i1 == 1 & $i2 == 1 then bang')
        sp1_sp1_button_87[0] = sp1_sp1_if_i1__1__i2__1_then_bang_88[0]  # type = ?
        sp1_sp1_number_139 = number(parameter_enable=0)
        sp1_sp1_zlchange_90 = newobj(text='zl.change')
        sp1_sp1_if_i1__1__i2__1_then_bang_88[0] = sp1_sp1_zlchange_90[1]  # type = ?
        sp1_sp1_gswitch_167 = gswitch(parameter_enable=0)
        sp1_sp1_int_102[1] = sp1_sp1_gswitch_167[0]  # type = ?
        sp1_sp1_number_139[0] = sp1_sp1_gswitch_167[0]  # type = ?
        sp1_sp1_if_i1__1__i2__1_then_bang_88[1] = sp1_sp1_gswitch_167[0]  # type = ?
        sp1_sp1_zlchange_90[0] = sp1_sp1_gswitch_167[0]  # type = ?
        sp1_sp1_inlet_83 = inlet(comment='', index=1)
        sp1_sp1_gswitch_167[0] = sp1_sp1_inlet_83[0]  # type = int
        sp1_sp1_inlet_84 = inlet(comment='', index=2)
        sp1_sp1_gswitch_167[1] = sp1_sp1_inlet_84[0]  # type = ?
        return sp1_sp1_outlet_86

    #----------------------------------------------------------------------
    def subpatcher_2(sp1_sp2_inlet_79, sp1_sp2_inlet_80):
        sp1_sp2_outlet_81 = outlet(comment='', index=1)
        sp1_sp2_int_101 = int(text='int')
        sp1_sp2_outlet_81[0] = sp1_sp2_int_101[0]  # type = int
        sp1_sp2_button_82 = button()
        sp1_sp2_int_101[0] = sp1_sp2_button_82[0]  # type = bang
        sp1_sp2_if_i1__1__i2__1_then_bang_83 = newobj(text='if $i1 == 1 & $i2 == 1 then bang')
        sp1_sp2_button_82[0] = sp1_sp2_if_i1__1__i2__1_then_bang_83[0]  # type = ?
        sp1_sp2_zlchange_86 = newobj(text='zl.change')
        sp1_sp2_if_i1__1__i2__1_then_bang_83[0] = sp1_sp2_zlchange_86[1]  # type = ?
        sp1_sp2_number_138 = number(parameter_enable=0)
        sp1_sp2_gswitch_161 = gswitch(parameter_enable=0)
        sp1_sp2_int_101[1] = sp1_sp2_gswitch_161[0]  # type = ?
        sp1_sp2_number_138[0] = sp1_sp2_gswitch_161[0]  # type = ?
        sp1_sp2_if_i1__1__i2__1_then_bang_83[1] = sp1_sp2_gswitch_161[0]  # type = ?
        sp1_sp2_zlchange_86[0] = sp1_sp2_gswitch_161[0]  # type = ?
        sp1_sp2_inlet_80 = inlet(comment='', index=2)
        sp1_sp2_gswitch_161[1] = sp1_sp2_inlet_80[0]  # type = ?
        sp1_sp2_inlet_79 = inlet(comment='', index=1)
        sp1_sp2_gswitch_161[0] = sp1_sp2_inlet_79[0]  # type = int
        return sp1_sp2_outlet_81

    #----------------------------------------------------------------------
    def subpatcher_3(sp1_sp3_inlet_2, sp1_sp3_inlet_28):
        sp1_sp3_outlet_77 = outlet(comment='', index=1)
        sp1_sp3_int_80 = int(text='int')
        sp1_sp3_outlet_77[0] = sp1_sp3_int_80[0]  # type = int
        sp1_sp3_button_81 = button()
        sp1_sp3_int_80[0] = sp1_sp3_button_81[0]  # type = bang
        sp1_sp3_if_i1__1__i2__1_then_bang_79 = newobj(text='if $i1 == 1 & $i2 == 1 then bang')
        sp1_sp3_button_81[0] = sp1_sp3_if_i1__1__i2__1_then_bang_79[0]  # type = ?
        sp1_sp3_zlchange_14 = newobj(text='zl.change')
        sp1_sp3_if_i1__1__i2__1_then_bang_79[0] = sp1_sp3_zlchange_14[1]  # type = ?
        sp1_sp3_number_136 = number(parameter_enable=0)
        sp1_sp3_gswitch_160 = gswitch(parameter_enable=0)
        sp1_sp3_number_136[0] = sp1_sp3_gswitch_160[0]  # type = ?
        sp1_sp3_zlchange_14[0] = sp1_sp3_gswitch_160[0]  # type = ?
        sp1_sp3_if_i1__1__i2__1_then_bang_79[1] = sp1_sp3_gswitch_160[0]  # type = ?
        sp1_sp3_int_80[1] = sp1_sp3_gswitch_160[0]  # type = ?
        sp1_sp3_inlet_2 = inlet(comment='', index=1)
        sp1_sp3_gswitch_160[0] = sp1_sp3_inlet_2[0]  # type = int
        sp1_sp3_message_3 = message(text='1')
        sp1_sp3_outlet_77[0] = sp1_sp3_message_3[0]  # type = ?
        sp1_sp3_loadbang_4 = loadbang(text='loadbang')
        sp1_sp3_message_3[0] = sp1_sp3_loadbang_4[0]  # type = bang
        sp1_sp3_inlet_28 = inlet(comment='', index=2)
        sp1_sp3_gswitch_160[1] = sp1_sp3_inlet_28[0]  # type = ?
# (comment) Solo permite la salida de un dato referente a frecuencia si cambia, para evitar reinicios de procesamiento y parametrización en brain score

        return sp1_sp3_outlet_77

    #----------------------------------------------------------------------
    def subpatcher_4(sp1_sp4_inlet_146):
        sp1_sp4_outlet_155 = outlet(comment='', index=7)
        sp1_sp4_outlet_152 = outlet(comment='', index=5)
        sp1_sp4_outlet_148 = outlet(comment='', index=1)
        sp1_sp4_outlet_2 = outlet(comment='', index=10)
        sp1_sp4_outlet_4 = outlet(comment='', index=12)
        sp1_sp4_outlet_5 = outlet(comment='', index=13)
        sp1_sp4_outlet_150 = outlet(comment='', index=3)
        sp1_sp4_outlet_153 = outlet(comment='', index=6)
        sp1_sp4_outlet_1 = outlet(comment='', index=9)
        sp1_sp4_outlet_3 = outlet(comment='', index=11)
        sp1_sp4_outlet_156 = outlet(comment='', index=8)
        sp1_sp4_outlet_151 = outlet(comment='', index=4)
        sp1_sp4_outlet_149 = outlet(comment='', index=2)
        sp1_sp4_outlet_6 = outlet(comment='', index=14)
        sp1_sp4_unpack_s_s_s_s_s_s_s_s_s_s_s_s_s_s_82 = newobj(text='unpack s s s s s s s s s s s s s s')
        sp1_sp4_outlet_148[0] = sp1_sp4_unpack_s_s_s_s_s_s_s_s_s_s_s_s_s_s_82[0]  # type = ?
        sp1_sp4_outlet_149[0] = sp1_sp4_unpack_s_s_s_s_s_s_s_s_s_s_s_s_s_s_82[1]  # type = ?
        sp1_sp4_outlet_150[0] = sp1_sp4_unpack_s_s_s_s_s_s_s_s_s_s_s_s_s_s_82[2]  # type = ?
        sp1_sp4_outlet_151[0] = sp1_sp4_unpack_s_s_s_s_s_s_s_s_s_s_s_s_s_s_82[3]  # type = ?
        sp1_sp4_outlet_152[0] = sp1_sp4_unpack_s_s_s_s_s_s_s_s_s_s_s_s_s_s_82[4]  # type = ?
        sp1_sp4_outlet_153[0] = sp1_sp4_unpack_s_s_s_s_s_s_s_s_s_s_s_s_s_s_82[5]  # type = ?
        sp1_sp4_outlet_155[0] = sp1_sp4_unpack_s_s_s_s_s_s_s_s_s_s_s_s_s_s_82[6]  # type = ?
        sp1_sp4_outlet_156[0] = sp1_sp4_unpack_s_s_s_s_s_s_s_s_s_s_s_s_s_s_82[7]  # type = ?
        sp1_sp4_outlet_1[0] = sp1_sp4_unpack_s_s_s_s_s_s_s_s_s_s_s_s_s_s_82[8]  # type = ?
        sp1_sp4_outlet_2[0] = sp1_sp4_unpack_s_s_s_s_s_s_s_s_s_s_s_s_s_s_82[9]  # type = ?
        sp1_sp4_outlet_3[0] = sp1_sp4_unpack_s_s_s_s_s_s_s_s_s_s_s_s_s_s_82[10]  # type = ?
        sp1_sp4_outlet_4[0] = sp1_sp4_unpack_s_s_s_s_s_s_s_s_s_s_s_s_s_s_82[11]  # type = ?
        sp1_sp4_outlet_5[0] = sp1_sp4_unpack_s_s_s_s_s_s_s_s_s_s_s_s_s_s_82[12]  # type = ?
        sp1_sp4_outlet_6[0] = sp1_sp4_unpack_s_s_s_s_s_s_s_s_s_s_s_s_s_s_82[13]  # type = ?
        sp1_sp4_route_brainrhythms_79 = newobj(text='route /BrainRhythms')
        sp1_sp4_unpack_s_s_s_s_s_s_s_s_s_s_s_s_s_s_82[0] = sp1_sp4_route_brainrhythms_79[0]  # type = ?
        sp1_sp4_message_8 = message(text='/BrainRhythms "0" "0" "0" "0" "0.4600" "0.6600" "0.2800" "0.0190" "0.0640" "0.0790"')
        sp1_sp4_udpreceive_2020_25 = newobj(text='udpreceive 2020')
        sp1_sp4_route_brainrhythms_79[0] = sp1_sp4_udpreceive_2020_25[0]  # type = ?
        sp1_sp4_message_8[1] = sp1_sp4_udpreceive_2020_25[0]  # type = ?
        sp1_sp4_message_41 = message(text='port 2021')
        sp1_sp4_udpreceive_2020_25[0] = sp1_sp4_message_41[0]  # type = ?
        sp1_sp4_button_76 = button()
        sp1_sp4_message_41[0] = sp1_sp4_button_76[0]  # type = bang
        sp1_sp4_message_39 = message(text='prepend port')
        sp1_sp4_message_41[0] = sp1_sp4_message_39[0]  # type = ?
        sp1_sp4_button_76[0] = sp1_sp4_message_39[0]  # type = ?
        sp1_sp4_t_b_l_52 = newobj(text='t b l')
        sp1_sp4_message_39[0] = sp1_sp4_t_b_l_52[0]  # type = bang
        sp1_sp4_message_41[1] = sp1_sp4_t_b_l_52[1]  # type = ?
        sp1_sp4_message_50 = message(text='2021')
        sp1_sp4_t_b_l_52[0] = sp1_sp4_message_50[0]  # type = ?
        sp1_sp4_t_b_l_145 = newobj(text='t b l')
        sp1_sp4_message_50[0] = sp1_sp4_t_b_l_145[0]  # type = bang
        sp1_sp4_message_50[1] = sp1_sp4_t_b_l_145[1]  # type = ?
        sp1_sp4_inlet_146 = inlet(comment='', index=1)
        sp1_sp4_t_b_l_145[0] = sp1_sp4_inlet_146[0]  # type = ?
# (comment) recibe los datos del pc de procesamiento via ip/udp/puerto. Desempaqueta los datos
        return sp1_sp4_outlet_5, sp1_sp4_outlet_6, sp1_sp4_outlet_1, sp1_sp4_outlet_2, sp1_sp4_outlet_3, sp1_sp4_outlet_4, sp1_sp4_outlet_148, sp1_sp4_outlet_149, sp1_sp4_outlet_150, sp1_sp4_outlet_151, sp1_sp4_outlet_152, sp1_sp4_outlet_153, sp1_sp4_outlet_155, sp1_sp4_outlet_156

    #----------------------------------------------------------------------
    def subpatcher_2(sp2_inlet_201):
        sp2_send_198 = send(text='s IInit')
        sp2_message_197 = message(text='1')
        sp2_send_198[0] = sp2_message_197[0]  # type = ?
        sp2_button_200 = button()
        sp2_message_197[0] = sp2_button_200[0]  # type = bang
        sp2_sel_9_195 = newobj(text='sel 9')
        sp2_button_200[0] = sp2_sel_9_195[0]  # type = bang
        sp2_print_idone_1 = newobj(text='print idone')
        sp2_message_3 = message(text='0')
        sp2_counter_0_9_192 = newobj(text='counter 0 9')
        sp2_print_idone_1[0] = sp2_counter_0_9_192[0]  # type = int
        sp2_sel_9_195[0] = sp2_counter_0_9_192[0]  # type = int
        sp2_message_3[1] = sp2_counter_0_9_192[0]  # type = int
        sp2_message_2 = message(text='0')
        sp2_counter_0_9_192[3] = sp2_message_2[0]  # type = ?
        sp2_receive_205 = receive(text='r cardinal')
        sp2_counter_0_9_192[4] = sp2_receive_205[0]  # type = ?
        sp2_sel_9_195[1] = sp2_receive_205[0]  # type = ?
        sp2_receive_8 = receive(text='r Reinicio-cont')
        sp2_message_2[0] = sp2_receive_8[0]  # type = ?
        sp2_inlet_201 = inlet(comment='', index=1)
        sp2_counter_0_9_192[0] = sp2_inlet_201[0]  # type = bang
# (comment) Solo envía la elección inicial de matriz cuando se terminan de generar todas las filas

    #----------------------------------------------------------------------
    def subpatcher_3():
        sp3_outlet_11 = outlet(comment='', index=1)
        sp3_number_10 = number(parameter_enable=0)
        sp3_outlet_11[0] = sp3_number_10[0]  # type = ?
        sp3_receive_1 = receive(text='r Offby4')
        sp3_number_10[0] = sp3_receive_1[0]  # type = ?
        sp3_receive_3 = receive(text='r Onby3')
        sp3_number_10[0] = sp3_receive_3[0]  # type = ?
        sp3_receive_5 = receive(text='r Offby2')
        sp3_number_10[0] = sp3_receive_5[0]  # type = ?
        sp3_receive_8 = receive(text='r Offby1')
        sp3_number_10[0] = sp3_receive_8[0]  # type = ?
        return sp3_outlet_11

    #----------------------------------------------------------------------
    def subpatcher_4():
        sp4_outlet_11 = outlet(comment='', index=1)
        sp4_number_10 = number(parameter_enable=0)
        sp4_outlet_11[0] = sp4_number_10[0]  # type = ?
        sp4_receive_1 = receive(text='r Offby4')
        sp4_number_10[0] = sp4_receive_1[0]  # type = ?
        sp4_receive_3 = receive(text='r Offby3')
        sp4_number_10[0] = sp4_receive_3[0]  # type = ?
        sp4_receive_5 = receive(text='r Onby2')
        sp4_number_10[0] = sp4_receive_5[0]  # type = ?
        sp4_receive_8 = receive(text='r Offby1')
        sp4_number_10[0] = sp4_receive_8[0]  # type = ?
        return sp4_outlet_11

    #----------------------------------------------------------------------
    def subpatcher_5():
        sp5_outlet_11 = outlet(comment='', index=1)
        sp5_number_10 = number(parameter_enable=0)
        sp5_outlet_11[0] = sp5_number_10[0]  # type = ?
        sp5_receive_1 = receive(text='r Onby4')
        sp5_number_10[0] = sp5_receive_1[0]  # type = ?
        sp5_receive_3 = receive(text='r Offby3')
        sp5_number_10[0] = sp5_receive_3[0]  # type = ?
        sp5_receive_5 = receive(text='r Offby2')
        sp5_number_10[0] = sp5_receive_5[0]  # type = ?
        sp5_receive_8 = receive(text='r Onby1')
        sp5_number_10[0] = sp5_receive_8[0]  # type = ?
        return sp5_outlet_11

    #----------------------------------------------------------------------
    def subpatcher_6(sp6_inlet_243, sp6_inlet_244, sp6_inlet_245, sp6_inlet_246, sp6_inlet_247, sp6_inlet_248, sp6_inlet_249, sp6_inlet_250, sp6_inlet_251):
        sp6_outlet_256 = outlet(comment='', index=5)
        sp6_scale_0_1_0_12_155 = newobj(text='scale 0. 1. 0 12')
        sp6_outlet_256[0] = sp6_scale_0_1_0_12_155[0]  # type = ?
        sp6___3_157 = newobj(text='+ 3')
        sp6_scale_0_1_0_12_155[4] = sp6___3_157[0]  # type = int
        sp6_receive_152 = receive(text='r cardinal')
        sp6___3_157[0] = sp6_receive_152[0]  # type = ?
        sp6_outlet_254 = outlet(comment='', index=3)
        sp6_scale_0_1_1_9_123 = newobj(text='scale 0. 1. 1 9')
        sp6_outlet_254[0] = sp6_scale_0_1_1_9_123[0]  # type = ?
        sp6_receive_142 = receive(text='r cardinal')
        sp6_scale_0_1_1_9_123[4] = sp6_receive_142[0]  # type = ?
        sp6_message_159 = message(linecount=3, text='0.712475')
        sp6_scale_0_1_0_12_155[0] = sp6_message_159[0]  # type = ?
        sp6_message_103 = message(linecount=2, text='0.712475')
        sp6_scale_0_1_1_9_123[0] = sp6_message_103[0]  # type = ?
        sp6_inlet_244 = inlet(comment='', index=2)
        sp6_message_103[1] = sp6_inlet_244[0]  # type = ?
        sp6_message_159[1] = sp6_inlet_244[0]  # type = ?
        sp6_outlet_259 = outlet(comment='', index=8)
        sp6_scale_0_1_1_9_130 = newobj(text='scale 0. 1. 1 9')
        sp6_outlet_259[0] = sp6_scale_0_1_1_9_130[0]  # type = ?
        sp6_message_108 = message(text='0.181137')
        sp6_scale_0_1_1_9_130[0] = sp6_message_108[0]  # type = ?
        sp6_gswitch2_234 = gswitch2(int=1, parameter_enable=0)
        sp6_message_108[0] = sp6_gswitch2_234[0]  # type = ?
        sp6_delay_5_10 = newobj(text='delay 5')
        sp6_gswitch2_234[1] = sp6_delay_5_10[0]  # type = bang
        sp6_button_109 = button()
        sp6_delay_5_10[0] = sp6_button_109[0]  # type = bang
        sp6_receive_112 = receive(text='r LastNote')
        sp6_button_109[0] = sp6_receive_112[0]  # type = ?
        sp6_outlet_255 = outlet(comment='', index=4)
        sp6_scale_0_1_3125_250_57 = newobj(text='scale 0. 1. 31.25 250.')
        sp6_outlet_255[0] = sp6_scale_0_1_3125_250_57[0]  # type = ?
        sp6_p_firstdata_219 = _subpatcher(subpatcher_id=12, text='p FirstData')
        sp6_scale_0_1_3125_250_57[0] = sp6_p_firstdata_219[0]  # type = ?
        sp6_inlet_245 = inlet(comment='', index=3)
        sp6_p_firstdata_219[1] = sp6_inlet_245[0]  # type = ?
        sp6_outlet_262 = outlet(comment='', index=11)
        sp6_scale_0_1_1_17_83 = newobj(text='scale 0. 1. 1 17')
        sp6_outlet_262[0] = sp6_scale_0_1_1_17_83[0]  # type = ?
        sp6_p_firstdata_82 = _subpatcher(subpatcher_id=10, text='p FirstData')
        sp6_scale_0_1_1_17_83[0] = sp6_p_firstdata_82[0]  # type = ?
        sp6_gswitch2_240 = gswitch2(int=1, parameter_enable=0)
        sp6_p_firstdata_82[0] = sp6_gswitch2_240[0]  # type = ?
        sp6_outlet_258 = outlet(comment='', index=7)
        sp6_scale_0_1_0_11_151 = newobj(text='scale 0. 1. 0 11')
        sp6_outlet_258[0] = sp6_scale_0_1_0_11_151[0]  # type = ?
        sp6_p_firstdata_208 = _subpatcher(subpatcher_id=14, text='p FirstData')
        sp6_scale_0_1_0_11_151[0] = sp6_p_firstdata_208[0]  # type = ?
        sp6_gswitch2_202 = gswitch2(int=1, parameter_enable=0)
        sp6_p_firstdata_208[0] = sp6_gswitch2_202[0]  # type = ?
        sp6_p_inout2_203 = _subpatcher(subpatcher_id=7, text='p InOut2')
        sp6_gswitch2_202[0] = sp6_p_inout2_203[0]  # type = ?
        sp6_outlet_257 = outlet(comment='', index=6)
        sp6_number_34 = number(parameter_enable=0)
        sp6_outlet_257[0] = sp6_number_34[0]  # type = ?
        sp6___12_31 = newobj(text='* 12')
        sp6_number_34[0] = sp6___12_31[0]  # type = int
        sp6_number_13 = number(parameter_enable=0)
        sp6___12_31[0] = sp6_number_13[0]  # type = ?
        sp6_scale_0_01_1_7_186 = newobj(text='scale 0. 0.1 1 7')
        
        #sp6_scale_0_01_1_7_186 = newobj(text='scale 0. 0.1 1 7')
        sp6_scale_0_01_1_7_186 = Max.scale('scale 0. 0.1 1 7')
        
        sp6_number_13[0] = sp6_scale_0_01_1_7_186[0]  # type = ?
        sp6_p_firstdata_220 = _subpatcher(subpatcher_id=11, text='p FirstData')
        sp6_scale_0_01_1_7_186[0] = sp6_p_firstdata_220[0]  # type = ?
        sp6_gswitch2_229 = gswitch2(parameter_enable=0)
        sp6_p_firstdata_220[0] = sp6_gswitch2_229[0]  # type = ?
        sp6_gswitch2_224 = gswitch2(parameter_enable=0)
        sp6_p_firstdata_219[0] = sp6_gswitch2_224[0]  # type = ?
        sp6_button_73 = button()
        sp6_gswitch2_224[1] = sp6_button_73[0]  # type = bang
        sp6_receive_74 = receive(text='r bang1')
        sp6_button_73[0] = sp6_receive_74[0]  # type = ?
        sp6_outlet_252 = outlet(comment='', index=1)
        sp6_scale_0_1_8_215_184 = newobj(text='scale 0. 1. 8 215')
        sp6_outlet_252[0] = sp6_scale_0_1_8_215_184[0]  # type = ?
        sp6_p_firstdata_207 = _subpatcher(subpatcher_id=15, text='p FirstData')
        sp6_scale_0_1_8_215_184[0] = sp6_p_firstdata_207[0]  # type = ?
        sp6_gswitch2_192 = gswitch2(parameter_enable=0)
        sp6_p_firstdata_207[0] = sp6_gswitch2_192[0]  # type = ?
        sp6_button_58 = button()
        sp6_gswitch2_192[1] = sp6_button_58[0]  # type = bang
        sp6_button_79 = button()
        sp6_gswitch2_240[1] = sp6_button_79[0]  # type = bang
        sp6_receive_80 = receive(text='r bang4')
        sp6_button_79[0] = sp6_receive_80[0]  # type = ?
        sp6_inlet_248 = inlet(comment='', index=6)
        sp6_message_108[1] = sp6_inlet_248[0]  # type = ?
        sp6_message_78 = message(text='0')
        sp6_outlet_257[0] = sp6_message_78[0]  # type = ?
        sp6_scale_0_01_1_4_35 = newobj(text='scale 0. 0.1 1 4')
        sp6_number_13[0] = sp6_scale_0_01_1_4_35[0]  # type = ?
        sp6_message_37 = message(linecount=2, text='0.099982')
        sp6_scale_0_01_1_4_35[0] = sp6_message_37[0]  # type = ?
        sp6_inlet_246 = inlet(comment='', index=4)
        sp6_p_firstdata_220[1] = sp6_inlet_246[0]  # type = ?
        sp6_message_37[1] = sp6_inlet_246[0]  # type = ?
        sp6_outlet_261 = outlet(comment='', index=10)
        sp6_scale_0_1_1_9_138 = newobj(text='scale 0. 1. 1 9')
        sp6_outlet_261[0] = sp6_scale_0_1_1_9_138[0]  # type = ?
        sp6_message_116 = message(linecount=2, text='0.044019')
        sp6_scale_0_1_1_9_138[0] = sp6_message_116[0]  # type = ?
        sp6_gswitch2_197 = gswitch2(parameter_enable=0)
        sp6_message_103[0] = sp6_gswitch2_197[0]  # type = ?
        sp6_delay_5_1 = newobj(text='delay 5')
        sp6_gswitch2_197[1] = sp6_delay_5_1[0]  # type = bang
        sp6_inlet_243 = inlet(comment='', index=1)
        sp6_p_firstdata_207[1] = sp6_inlet_243[0]  # type = ?
        sp6_p_inout4_239 = _subpatcher(subpatcher_id=1, text='p InOut4')
        sp6_gswitch2_240[0] = sp6_p_inout4_239[0]  # type = ?
        sp6_button_69 = button()
        sp6_gswitch2_202[1] = sp6_button_69[0]  # type = bang
        sp6_outlet_260 = outlet(comment='', index=9)
        sp6_scale_0_1_0_11_212 = newobj(text='scale 0. 1. 0 11')
        sp6_outlet_260[0] = sp6_scale_0_1_0_11_212[0]  # type = ?
        sp6_p_firstdata_209 = _subpatcher(subpatcher_id=13, text='p FirstData')
        sp6_scale_0_1_0_11_212[0] = sp6_p_firstdata_209[0]  # type = ?
        sp6_gswitch2_236 = gswitch2(int=1, parameter_enable=0)
        sp6_message_116[0] = sp6_gswitch2_236[0]  # type = ?
        sp6_delay_5_11 = newobj(text='delay 5')
        sp6_gswitch2_236[1] = sp6_delay_5_11[0]  # type = bang
        sp6_button_226 = button()
        sp6_delay_5_1[0] = sp6_button_226[0]  # type = bang
        sp6_receive_144 = receive(text='r cardinal')
        sp6_scale_0_1_1_9_130[4] = sp6_receive_144[0]  # type = ?
        sp6_p_inout1_223 = _subpatcher(subpatcher_id=5, text='p InOut1')
        sp6_gswitch2_224[0] = sp6_p_inout1_223[0]  # type = ?
        sp6_gswitch2_205 = gswitch2(int=1, parameter_enable=0)
        sp6_p_firstdata_209[0] = sp6_gswitch2_205[0]  # type = ?
        sp6_button_71 = button()
        sp6_gswitch2_205[1] = sp6_button_71[0]  # type = bang
        sp6_outlet_253 = outlet(comment='', index=2)
        sp6_receive_115 = receive(text='r ComplSet')
        sp6_outlet_253[0] = sp6_receive_115[0]  # type = ?
        sp6_receive_72 = receive(text='r bang3')
        sp6_button_71[0] = sp6_receive_72[0]  # type = ?
        sp6_receive_70 = receive(text='r bang2')
        sp6_button_69[0] = sp6_receive_70[0]  # type = ?
        sp6_p_inout3_235 = _subpatcher(subpatcher_id=2, text='p InOut3')
        sp6_gswitch2_236[0] = sp6_p_inout3_235[0]  # type = ?
        sp6_p_inout1_77 = _subpatcher(subpatcher_id=9, text='p InOut1')
        sp6_gswitch2_192[0] = sp6_p_inout1_77[0]  # type = ?
        sp6_button_121 = button()
        sp6_delay_5_11[0] = sp6_button_121[0]  # type = bang
        sp6_receive_143 = receive(text='r LastNote')
        sp6_button_121[0] = sp6_receive_143[0]  # type = ?
        sp6_gswitch2_33 = gswitch2(parameter_enable=0)
        sp6_message_37[0] = sp6_gswitch2_33[0]  # type = ?
        sp6_delay_5_15 = newobj(text='delay 5')
        sp6_gswitch2_33[1] = sp6_delay_5_15[0]  # type = bang
        sp6_button_24 = button()
        sp6_delay_5_15[0] = sp6_button_24[0]  # type = bang
        sp6_receive_16 = receive(text='r LastNote')
        sp6_button_24[0] = sp6_receive_16[0]  # type = ?
        sp6_receive_106 = receive(text='r Rhy')
        sp6_button_58[0] = sp6_receive_106[0]  # type = ?
        sp6_button_75 = button()
        sp6_gswitch2_229[1] = sp6_button_75[0]  # type = bang
        sp6_p_inout23_14 = _subpatcher(subpatcher_id=0, text='p InOut2-3')
        sp6_gswitch2_33[0] = sp6_p_inout23_14[0]  # type = ?
        sp6_p_inout2_233 = _subpatcher(subpatcher_id=3, text='p InOut2')
        sp6_gswitch2_234[0] = sp6_p_inout2_233[0]  # type = ?
        sp6_receive_227 = receive(text='r LastNote')
        sp6_button_226[0] = sp6_receive_227[0]  # type = ?
        sp6_button_161 = button()
        sp6_message_159[0] = sp6_button_161[0]  # type = bang
        sp6_receive_145 = receive(text='r cardinal')
        sp6_scale_0_1_1_9_138[4] = sp6_receive_145[0]  # type = ?
        sp6_p_inout3_222 = _subpatcher(subpatcher_id=6, text='p InOut3')
        sp6_gswitch2_205[0] = sp6_p_inout3_222[0]  # type = ?
        sp6_inlet_247 = inlet(comment='', index=5)
        sp6_p_firstdata_208[1] = sp6_inlet_247[0]  # type = ?
        sp6_inlet_251 = inlet(comment='', index=9)
        sp6_p_firstdata_82[1] = sp6_inlet_251[0]  # type = ?
        sp6_p_inout1_200 = _subpatcher(subpatcher_id=8, text='p InOut1')
        sp6_gswitch2_197[0] = sp6_p_inout1_200[0]  # type = ?
        sp6_p_inout1_230 = _subpatcher(subpatcher_id=4, text='p InOut1')
        sp6_gswitch2_229[0] = sp6_p_inout1_230[0]  # type = ?
        sp6_receive_76 = receive(text='r bang1')
        sp6_button_75[0] = sp6_receive_76[0]  # type = ?
        sp6_inlet_249 = inlet(comment='', index=7)
        sp6_p_firstdata_209[1] = sp6_inlet_249[0]  # type = ?
        sp6_receive_162 = receive(text='r LastNote')
        sp6_button_161[0] = sp6_receive_162[0]  # type = ?
        sp6_inlet_250 = inlet(comment='', index=8)
        sp6_message_116[1] = sp6_inlet_250[0]  # type = ?
        sp6_message_117 = message(text='11')
        sp6_receive_118 = receive(text='r ComplLen')
        sp6_message_117[1] = sp6_receive_118[0]  # type = ?
        sp6_scale_0_1_1_17_83[4] = sp6_receive_118[0]  # type = ?
# (comment) Pitch Class Set
# (comment) f1 - e2
# (comment) Rhythm Variation
# (comment) f1 - ent2
# (comment) PrimeForm
# (comment) Range
# (comment) f2 - e1
# (comment) Transposition Base
# (comment) f2 - e3
# (comment) Transposition Variation
# (comment) f3 - e1
# (comment) f3 - e3
# (comment) Inversion Base
# (comment) f4 - e1
# (comment) f1-e1
# (comment) Inversion Variation
# (comment) Complement 
# (comment) Complement
# (comment) PrimeForm 
Variation
# (comment) f1-e3
# (comment) RhythmBase
        return sp6_outlet_252, sp6_outlet_253, sp6_outlet_254, sp6_outlet_255, sp6_outlet_256, sp6_outlet_257, sp6_outlet_258, sp6_outlet_259, sp6_outlet_260, sp6_outlet_261, sp6_outlet_262

    #----------------------------------------------------------------------
    def subpatcher_0():
        sp6_sp0_outlet_11 = outlet(comment='', index=1)
        sp6_sp0_number_10 = number(parameter_enable=0)
        sp6_sp0_outlet_11[0] = sp6_sp0_number_10[0]  # type = ?
        sp6_sp0_receive_1 = receive(text='r Offby4')
        sp6_sp0_number_10[0] = sp6_sp0_receive_1[0]  # type = ?
        sp6_sp0_receive_3 = receive(text='r Onby3')
        sp6_sp0_number_10[0] = sp6_sp0_receive_3[0]  # type = ?
        sp6_sp0_receive_5 = receive(text='r Onby2')
        sp6_sp0_number_10[0] = sp6_sp0_receive_5[0]  # type = ?
        sp6_sp0_receive_8 = receive(text='r Onby1')
        sp6_sp0_number_10[0] = sp6_sp0_receive_8[0]  # type = ?
        return sp6_sp0_outlet_11

    #----------------------------------------------------------------------
    def subpatcher_1():
        sp6_sp1_outlet_11 = outlet(comment='', index=1)
        sp6_sp1_number_10 = number(parameter_enable=0)
        sp6_sp1_outlet_11[0] = sp6_sp1_number_10[0]  # type = ?
        sp6_sp1_receive_1 = receive(text='r Onby4')
        sp6_sp1_number_10[0] = sp6_sp1_receive_1[0]  # type = ?
        sp6_sp1_receive_3 = receive(text='r Offby3')
        sp6_sp1_number_10[0] = sp6_sp1_receive_3[0]  # type = ?
        sp6_sp1_receive_5 = receive(text='r Offby2')
        sp6_sp1_number_10[0] = sp6_sp1_receive_5[0]  # type = ?
        sp6_sp1_receive_8 = receive(text='r Offby1')
        sp6_sp1_number_10[0] = sp6_sp1_receive_8[0]  # type = ?
        return sp6_sp1_outlet_11

    #----------------------------------------------------------------------
    def subpatcher_2():
        sp6_sp2_outlet_11 = outlet(comment='', index=1)
        sp6_sp2_number_10 = number(parameter_enable=0)
        sp6_sp2_outlet_11[0] = sp6_sp2_number_10[0]  # type = ?
        sp6_sp2_receive_1 = receive(text='r Offby4')
        sp6_sp2_number_10[0] = sp6_sp2_receive_1[0]  # type = ?
        sp6_sp2_receive_3 = receive(text='r Onby3')
        sp6_sp2_number_10[0] = sp6_sp2_receive_3[0]  # type = ?
        sp6_sp2_receive_5 = receive(text='r Offby2')
        sp6_sp2_number_10[0] = sp6_sp2_receive_5[0]  # type = ?
        sp6_sp2_receive_8 = receive(text='r Offby1')
        sp6_sp2_number_10[0] = sp6_sp2_receive_8[0]  # type = ?
        return sp6_sp2_outlet_11

    #----------------------------------------------------------------------
    def subpatcher_3():
        sp6_sp3_outlet_11 = outlet(comment='', index=1)
        sp6_sp3_number_10 = number(parameter_enable=0)
        sp6_sp3_outlet_11[0] = sp6_sp3_number_10[0]  # type = ?
        sp6_sp3_receive_1 = receive(text='r Offby4')
        sp6_sp3_number_10[0] = sp6_sp3_receive_1[0]  # type = ?
        sp6_sp3_receive_3 = receive(text='r Offby3')
        sp6_sp3_number_10[0] = sp6_sp3_receive_3[0]  # type = ?
        sp6_sp3_receive_5 = receive(text='r Onby2')
        sp6_sp3_number_10[0] = sp6_sp3_receive_5[0]  # type = ?
        sp6_sp3_receive_8 = receive(text='r Offby1')
        sp6_sp3_number_10[0] = sp6_sp3_receive_8[0]  # type = ?
        return sp6_sp3_outlet_11

    #----------------------------------------------------------------------
    def subpatcher_4():
        sp6_sp4_outlet_11 = outlet(comment='', index=1)
        sp6_sp4_number_10 = number(parameter_enable=0)
        sp6_sp4_outlet_11[0] = sp6_sp4_number_10[0]  # type = ?
        sp6_sp4_receive_1 = receive(text='r Offby4')
        sp6_sp4_number_10[0] = sp6_sp4_receive_1[0]  # type = ?
        sp6_sp4_receive_3 = receive(text='r Offby3')
        sp6_sp4_number_10[0] = sp6_sp4_receive_3[0]  # type = ?
        sp6_sp4_receive_5 = receive(text='r Offby2')
        sp6_sp4_number_10[0] = sp6_sp4_receive_5[0]  # type = ?
        sp6_sp4_receive_8 = receive(text='r Onby1')
        sp6_sp4_number_10[0] = sp6_sp4_receive_8[0]  # type = ?
        return sp6_sp4_outlet_11

    #----------------------------------------------------------------------
    def subpatcher_5():
        sp6_sp5_outlet_11 = outlet(comment='', index=1)
        sp6_sp5_number_10 = number(parameter_enable=0)
        sp6_sp5_outlet_11[0] = sp6_sp5_number_10[0]  # type = ?
        sp6_sp5_receive_1 = receive(text='r Offby4')
        sp6_sp5_number_10[0] = sp6_sp5_receive_1[0]  # type = ?
        sp6_sp5_receive_3 = receive(text='r Offby3')
        sp6_sp5_number_10[0] = sp6_sp5_receive_3[0]  # type = ?
        sp6_sp5_receive_5 = receive(text='r Offby2')
        sp6_sp5_number_10[0] = sp6_sp5_receive_5[0]  # type = ?
        sp6_sp5_receive_8 = receive(text='r Onby1')
        sp6_sp5_number_10[0] = sp6_sp5_receive_8[0]  # type = ?
        return sp6_sp5_outlet_11

    #----------------------------------------------------------------------
    def subpatcher_6():
        sp6_sp6_outlet_11 = outlet(comment='', index=1)
        sp6_sp6_number_10 = number(parameter_enable=0)
        sp6_sp6_outlet_11[0] = sp6_sp6_number_10[0]  # type = ?
        sp6_sp6_receive_1 = receive(text='r Offby4')
        sp6_sp6_number_10[0] = sp6_sp6_receive_1[0]  # type = ?
        sp6_sp6_receive_3 = receive(text='r Onby3')
        sp6_sp6_number_10[0] = sp6_sp6_receive_3[0]  # type = ?
        sp6_sp6_receive_5 = receive(text='r Offby2')
        sp6_sp6_number_10[0] = sp6_sp6_receive_5[0]  # type = ?
        sp6_sp6_receive_8 = receive(text='r Offby1')
        sp6_sp6_number_10[0] = sp6_sp6_receive_8[0]  # type = ?
        return sp6_sp6_outlet_11

    #----------------------------------------------------------------------
    def subpatcher_7():
        sp6_sp7_outlet_11 = outlet(comment='', index=1)
        sp6_sp7_number_10 = number(parameter_enable=0)
        sp6_sp7_outlet_11[0] = sp6_sp7_number_10[0]  # type = ?
        sp6_sp7_receive_1 = receive(text='r Offby4')
        sp6_sp7_number_10[0] = sp6_sp7_receive_1[0]  # type = ?
        sp6_sp7_receive_3 = receive(text='r Offby3')
        sp6_sp7_number_10[0] = sp6_sp7_receive_3[0]  # type = ?
        sp6_sp7_receive_5 = receive(text='r Onby2')
        sp6_sp7_number_10[0] = sp6_sp7_receive_5[0]  # type = ?
        sp6_sp7_receive_8 = receive(text='r Offby1')
        sp6_sp7_number_10[0] = sp6_sp7_receive_8[0]  # type = ?
        return sp6_sp7_outlet_11

    #----------------------------------------------------------------------
    def subpatcher_8():
        sp6_sp8_outlet_11 = outlet(comment='', index=1)
        sp6_sp8_number_10 = number(parameter_enable=0)
        sp6_sp8_outlet_11[0] = sp6_sp8_number_10[0]  # type = ?
        sp6_sp8_receive_1 = receive(text='r Onby4')
        sp6_sp8_number_10[0] = sp6_sp8_receive_1[0]  # type = ?
        sp6_sp8_receive_3 = receive(text='r Offby3')
        sp6_sp8_number_10[0] = sp6_sp8_receive_3[0]  # type = ?
        sp6_sp8_receive_5 = receive(text='r Offby2')
        sp6_sp8_number_10[0] = sp6_sp8_receive_5[0]  # type = ?
        sp6_sp8_receive_8 = receive(text='r Onby1')
        sp6_sp8_number_10[0] = sp6_sp8_receive_8[0]  # type = ?
        return sp6_sp8_outlet_11

    #----------------------------------------------------------------------
    def subpatcher_9():
        sp6_sp9_outlet_11 = outlet(comment='', index=1)
        sp6_sp9_number_10 = number(parameter_enable=0)
        sp6_sp9_outlet_11[0] = sp6_sp9_number_10[0]  # type = ?
        sp6_sp9_receive_1 = receive(text='r Offby4')
        sp6_sp9_number_10[0] = sp6_sp9_receive_1[0]  # type = ?
        sp6_sp9_receive_3 = receive(text='r Offby3')
        sp6_sp9_number_10[0] = sp6_sp9_receive_3[0]  # type = ?
        sp6_sp9_receive_5 = receive(text='r Offby2')
        sp6_sp9_number_10[0] = sp6_sp9_receive_5[0]  # type = ?
        sp6_sp9_receive_8 = receive(text='r Onby1')
        sp6_sp9_number_10[0] = sp6_sp9_receive_8[0]  # type = ?
        return sp6_sp9_outlet_11

    #----------------------------------------------------------------------
    def subpatcher_10(sp6_sp10_inlet_204, sp6_sp10_inlet_205):
        sp6_sp10_outlet_206 = outlet(comment='', index=1)
        sp6_sp10_message_164 = message(text='0')
        sp6_sp10_if_i1__1_then_f2_162 = newobj(text='if $i1 == 1 then $f2')
        sp6_sp10_message_164[1] = sp6_sp10_if_i1__1_then_f2_162[0]  # type = ?
        sp6_sp10_outlet_206[0] = sp6_sp10_if_i1__1_then_f2_162[0]  # type = ?
        sp6_sp10_number_166 = number(parameter_enable=0)
        sp6_sp10_accum_1_157 = newobj(text='accum 1')
        sp6_sp10_if_i1__1_then_f2_162[0] = sp6_sp10_accum_1_157[0]  # type = int
        sp6_sp10_number_166[0] = sp6_sp10_accum_1_157[0]  # type = int
        sp6_sp10_message_161 = message(text='1')
        sp6_sp10_accum_1_157[1] = sp6_sp10_message_161[0]  # type = ?
        sp6_sp10_button_168 = button()
        sp6_sp10_accum_1_157[0] = sp6_sp10_button_168[0]  # type = bang
        sp6_sp10_message_161[0] = sp6_sp10_button_168[0]  # type = bang
        sp6_sp10_inlet_205 = inlet(comment='', index=2)
        sp6_sp10_if_i1__1_then_f2_162[1] = sp6_sp10_inlet_205[0]  # type = ?
        sp6_sp10_button_168[0] = sp6_sp10_inlet_205[0]  # type = ?
        sp6_sp10_message_159 = message(text='set 0')
        sp6_sp10_accum_1_157[0] = sp6_sp10_message_159[0]  # type = ?
        sp6_sp10_inlet_204 = inlet(comment='', index=1)
        sp6_sp10_message_159[0] = sp6_sp10_inlet_204[0]  # type = ?
        return sp6_sp10_outlet_206

    #----------------------------------------------------------------------
    def subpatcher_11(sp6_sp11_inlet_204, sp6_sp11_inlet_205):
        sp6_sp11_outlet_206 = outlet(comment='', index=1)
        sp6_sp11_message_164 = message(linecount=2, text='0.064979')
        sp6_sp11_if_i1__1_then_f2_162 = newobj(text='if $i1 == 1 then $f2')
        sp6_sp11_message_164[1] = sp6_sp11_if_i1__1_then_f2_162[0]  # type = ?
        sp6_sp11_outlet_206[0] = sp6_sp11_if_i1__1_then_f2_162[0]  # type = ?
        sp6_sp11_number_166 = number(parameter_enable=0)
        sp6_sp11_accum_1_157 = newobj(text='accum 1')
        sp6_sp11_if_i1__1_then_f2_162[0] = sp6_sp11_accum_1_157[0]  # type = int
        sp6_sp11_number_166[0] = sp6_sp11_accum_1_157[0]  # type = int
        sp6_sp11_message_161 = message(text='1')
        sp6_sp11_accum_1_157[1] = sp6_sp11_message_161[0]  # type = ?
        sp6_sp11_button_168 = button()
        sp6_sp11_accum_1_157[0] = sp6_sp11_button_168[0]  # type = bang
        sp6_sp11_message_161[0] = sp6_sp11_button_168[0]  # type = bang
        sp6_sp11_inlet_205 = inlet(comment='', index=2)
        sp6_sp11_if_i1__1_then_f2_162[1] = sp6_sp11_inlet_205[0]  # type = ?
        sp6_sp11_button_168[0] = sp6_sp11_inlet_205[0]  # type = ?
        sp6_sp11_message_159 = message(text='set 0')
        sp6_sp11_accum_1_157[0] = sp6_sp11_message_159[0]  # type = ?
        sp6_sp11_inlet_204 = inlet(comment='', index=1)
        sp6_sp11_message_159[0] = sp6_sp11_inlet_204[0]  # type = ?
        return sp6_sp11_outlet_206

    #----------------------------------------------------------------------
    def subpatcher_12(sp6_sp12_inlet_204, sp6_sp12_inlet_205):
        sp6_sp12_outlet_206 = outlet(comment='', index=1)
        sp6_sp12_message_164 = message(linecount=2, text='0.591728')
        sp6_sp12_if_i1__1_then_f2_162 = newobj(text='if $i1 == 1 then $f2')
        sp6_sp12_message_164[1] = sp6_sp12_if_i1__1_then_f2_162[0]  # type = ?
        sp6_sp12_outlet_206[0] = sp6_sp12_if_i1__1_then_f2_162[0]  # type = ?
        sp6_sp12_number_166 = number(parameter_enable=0)
        sp6_sp12_accum_1_157 = newobj(text='accum 1')
        sp6_sp12_if_i1__1_then_f2_162[0] = sp6_sp12_accum_1_157[0]  # type = int
        sp6_sp12_number_166[0] = sp6_sp12_accum_1_157[0]  # type = int
        sp6_sp12_message_161 = message(text='1')
        sp6_sp12_accum_1_157[1] = sp6_sp12_message_161[0]  # type = ?
        sp6_sp12_button_168 = button()
        sp6_sp12_accum_1_157[0] = sp6_sp12_button_168[0]  # type = bang
        sp6_sp12_message_161[0] = sp6_sp12_button_168[0]  # type = bang
        sp6_sp12_inlet_205 = inlet(comment='', index=2)
        sp6_sp12_if_i1__1_then_f2_162[1] = sp6_sp12_inlet_205[0]  # type = ?
        sp6_sp12_button_168[0] = sp6_sp12_inlet_205[0]  # type = ?
        sp6_sp12_message_159 = message(text='set 0')
        sp6_sp12_accum_1_157[0] = sp6_sp12_message_159[0]  # type = ?
        sp6_sp12_inlet_204 = inlet(comment='', index=1)
        sp6_sp12_message_159[0] = sp6_sp12_inlet_204[0]  # type = ?
        return sp6_sp12_outlet_206

    #----------------------------------------------------------------------
    def subpatcher_13(sp6_sp13_inlet_204, sp6_sp13_inlet_205):
        sp6_sp13_outlet_206 = outlet(comment='', index=1)
        sp6_sp13_message_164 = message(linecount=2, text='0.644658')
        sp6_sp13_if_i1__1_then_f2_162 = newobj(text='if $i1 == 1 then $f2')
        sp6_sp13_message_164[1] = sp6_sp13_if_i1__1_then_f2_162[0]  # type = ?
        sp6_sp13_outlet_206[0] = sp6_sp13_if_i1__1_then_f2_162[0]  # type = ?
        sp6_sp13_number_166 = number(parameter_enable=0)
        sp6_sp13_accum_1_157 = newobj(text='accum 1')
        sp6_sp13_if_i1__1_then_f2_162[0] = sp6_sp13_accum_1_157[0]  # type = int
        sp6_sp13_number_166[0] = sp6_sp13_accum_1_157[0]  # type = int
        sp6_sp13_message_161 = message(text='1')
        sp6_sp13_accum_1_157[1] = sp6_sp13_message_161[0]  # type = ?
        sp6_sp13_button_168 = button()
        sp6_sp13_accum_1_157[0] = sp6_sp13_button_168[0]  # type = bang
        sp6_sp13_message_161[0] = sp6_sp13_button_168[0]  # type = bang
        sp6_sp13_inlet_205 = inlet(comment='', index=2)
        sp6_sp13_if_i1__1_then_f2_162[1] = sp6_sp13_inlet_205[0]  # type = ?
        sp6_sp13_button_168[0] = sp6_sp13_inlet_205[0]  # type = ?
        sp6_sp13_message_159 = message(text='set 0')
        sp6_sp13_accum_1_157[0] = sp6_sp13_message_159[0]  # type = ?
        sp6_sp13_inlet_204 = inlet(comment='', index=1)
        sp6_sp13_message_159[0] = sp6_sp13_inlet_204[0]  # type = ?
        return sp6_sp13_outlet_206

    #----------------------------------------------------------------------
    def subpatcher_14(sp6_sp14_inlet_204, sp6_sp14_inlet_205):
        sp6_sp14_outlet_206 = outlet(comment='', index=1)
        sp6_sp14_message_164 = message(linecount=2, text='0.308636')
        sp6_sp14_if_i1__1_then_f2_162 = newobj(text='if $i1 == 1 then $f2')
        sp6_sp14_message_164[1] = sp6_sp14_if_i1__1_then_f2_162[0]  # type = ?
        sp6_sp14_outlet_206[0] = sp6_sp14_if_i1__1_then_f2_162[0]  # type = ?
        sp6_sp14_number_166 = number(parameter_enable=0)
        sp6_sp14_accum_1_157 = newobj(text='accum 1')
        sp6_sp14_if_i1__1_then_f2_162[0] = sp6_sp14_accum_1_157[0]  # type = int
        sp6_sp14_number_166[0] = sp6_sp14_accum_1_157[0]  # type = int
        sp6_sp14_message_161 = message(text='1')
        sp6_sp14_accum_1_157[1] = sp6_sp14_message_161[0]  # type = ?
        sp6_sp14_button_168 = button()
        sp6_sp14_accum_1_157[0] = sp6_sp14_button_168[0]  # type = bang
        sp6_sp14_message_161[0] = sp6_sp14_button_168[0]  # type = bang
        sp6_sp14_inlet_205 = inlet(comment='', index=2)
        sp6_sp14_if_i1__1_then_f2_162[1] = sp6_sp14_inlet_205[0]  # type = ?
        sp6_sp14_button_168[0] = sp6_sp14_inlet_205[0]  # type = ?
        sp6_sp14_message_159 = message(text='set 0')
        sp6_sp14_accum_1_157[0] = sp6_sp14_message_159[0]  # type = ?
        sp6_sp14_inlet_204 = inlet(comment='', index=1)
        sp6_sp14_message_159[0] = sp6_sp14_inlet_204[0]  # type = ?
        return sp6_sp14_outlet_206

    #----------------------------------------------------------------------
    def subpatcher_15(sp6_sp15_inlet_204, sp6_sp15_inlet_205):
        sp6_sp15_outlet_206 = outlet(comment='', index=1)
        sp6_sp15_message_164 = message(linecount=2, text='0.683299')
        sp6_sp15_if_i1__1_then_f2_162 = newobj(text='if $i1 == 1 then $f2')
        sp6_sp15_message_164[1] = sp6_sp15_if_i1__1_then_f2_162[0]  # type = ?
        sp6_sp15_outlet_206[0] = sp6_sp15_if_i1__1_then_f2_162[0]  # type = ?
        sp6_sp15_number_166 = number(parameter_enable=0)
        sp6_sp15_accum_1_157 = newobj(text='accum 1')
        sp6_sp15_if_i1__1_then_f2_162[0] = sp6_sp15_accum_1_157[0]  # type = int
        sp6_sp15_number_166[0] = sp6_sp15_accum_1_157[0]  # type = int
        sp6_sp15_message_161 = message(text='1')
        sp6_sp15_accum_1_157[1] = sp6_sp15_message_161[0]  # type = ?
        sp6_sp15_button_168 = button()
        sp6_sp15_accum_1_157[0] = sp6_sp15_button_168[0]  # type = bang
        sp6_sp15_message_161[0] = sp6_sp15_button_168[0]  # type = bang
        sp6_sp15_inlet_205 = inlet(comment='', index=2)
        sp6_sp15_if_i1__1_then_f2_162[1] = sp6_sp15_inlet_205[0]  # type = ?
        sp6_sp15_button_168[0] = sp6_sp15_inlet_205[0]  # type = ?
        sp6_sp15_message_159 = message(text='set 0')
        sp6_sp15_accum_1_157[0] = sp6_sp15_message_159[0]  # type = ?
        sp6_sp15_inlet_204 = inlet(comment='', index=1)
        sp6_sp15_message_159[0] = sp6_sp15_inlet_204[0]  # type = ?
        return sp6_sp15_outlet_206

    #----------------------------------------------------------------------
    def subpatcher_7():
        sp7_send_15 = send(text='s Onby1')
        sp7_message_4 = message(text='0')
        sp7_send_15[0] = sp7_message_4[0]  # type = ?
        sp7_send_16 = send(text='s Offby1')
        sp7_message_6 = message(text='1')
        sp7_send_16[0] = sp7_message_6[0]  # type = ?
        sp7_message_126 = message(text='1')
        sp7_message_4[0] = sp7_message_126[0]  # type = ?
        sp7_message_6[0] = sp7_message_126[0]  # type = ?
        sp7_receive_31 = receive(text='r bang1')
        sp7_message_126[0] = sp7_receive_31[0]  # type = ?
        sp7_send_17 = send(text='s Offby2')
        sp7_message_7 = message(text='1')
        sp7_send_17[0] = sp7_message_7[0]  # type = ?
        sp7_send_21 = send(text='s Offby4')
        sp7_message_11 = message(text='1')
        sp7_send_21[0] = sp7_message_11[0]  # type = ?
        sp7_send_22 = send(text='s Onby4')
        sp7_message_12 = message(text='0')
        sp7_send_22[0] = sp7_message_12[0]  # type = ?
        sp7_send_19 = send(text='s Offby3')
        sp7_message_9 = message(text='1')
        sp7_send_19[0] = sp7_message_9[0]  # type = ?
        sp7_send_20 = send(text='s Onby3')
        sp7_message_10 = message(text='0')
        sp7_send_20[0] = sp7_message_10[0]  # type = ?
        sp7_message_128 = message(text='3')
        sp7_message_10[0] = sp7_message_128[0]  # type = ?
        sp7_message_9[0] = sp7_message_128[0]  # type = ?
        sp7_message_129 = message(text='4')
        sp7_message_11[0] = sp7_message_129[0]  # type = ?
        sp7_message_12[0] = sp7_message_129[0]  # type = ?
        sp7_receive_67 = receive(text='r bang4')
        sp7_message_129[0] = sp7_receive_67[0]  # type = ?
        sp7_send_18 = send(text='s Onby2')
        sp7_message_8 = message(text='0')
        sp7_send_18[0] = sp7_message_8[0]  # type = ?
        sp7_message_127 = message(text='2')
        sp7_message_7[0] = sp7_message_127[0]  # type = ?
        sp7_message_8[0] = sp7_message_127[0]  # type = ?
        sp7_receive_65 = receive(text='r bang3')
        sp7_message_128[0] = sp7_receive_65[0]  # type = ?
        sp7_receive_63 = receive(text='r bang2')
        sp7_message_127[0] = sp7_receive_63[0]  # type = ?

    #----------------------------------------------------------------------
    def subpatcher_8(sp8_inlet_201):
        sp8_send_198 = send(text='s TInit')
        sp8_message_197 = message(text='1')
        sp8_send_198[0] = sp8_message_197[0]  # type = ?
        sp8_button_200 = button()
        sp8_message_197[0] = sp8_button_200[0]  # type = bang
        sp8_sel_9_195 = newobj(text='sel 9')
        sp8_button_200[0] = sp8_sel_9_195[0]  # type = bang
        sp8_print_tdone_1 = newobj(text='print tdone')
        sp8_message_4 = message(text='0')
        sp8_counter_0_9_192 = newobj(text='counter 0 9')
        sp8_print_tdone_1[0] = sp8_counter_0_9_192[0]  # type = int
        sp8_sel_9_195[0] = sp8_counter_0_9_192[0]  # type = int
        sp8_message_4[1] = sp8_counter_0_9_192[0]  # type = int
        sp8_message_2 = message(text='0')
        sp8_counter_0_9_192[3] = sp8_message_2[0]  # type = ?
        sp8_receive_205 = receive(text='r cardinal')
        sp8_counter_0_9_192[4] = sp8_receive_205[0]  # type = ?
        sp8_sel_9_195[1] = sp8_receive_205[0]  # type = ?
        sp8_receive_8 = receive(text='r Reinicio-cont')
        sp8_message_2[0] = sp8_receive_8[0]  # type = ?
        sp8_inlet_201 = inlet(comment='', index=1)
        sp8_counter_0_9_192[0] = sp8_inlet_201[0]  # type = bang
# (comment) Solo envía la elección inicial de matriz cuando se terminan de generar todas las filas

    #----------------------------------------------------------------------
    def subpatcher_9(sp9_inlet_201):
        sp9_send_198 = send(text='s VarInit')
        sp9_message_197 = message(text='1')
        sp9_send_198[0] = sp9_message_197[0]  # type = ?
        sp9_delay_5_2 = newobj(text='delay 5')
        sp9_message_197[0] = sp9_delay_5_2[0]  # type = bang
        sp9_sel_12_195 = newobj(text='sel 12')
        sp9_message_10 = message(text='0')
        sp9_counter_0_12_192 = newobj(text='counter 0 12')
        sp9_message_10[1] = sp9_counter_0_12_192[0]  # type = int
        sp9_sel_12_195[0] = sp9_counter_0_12_192[0]  # type = int
        sp9_receive_205 = receive(text='r RhyCardinal')
        sp9_counter_0_12_192[4] = sp9_receive_205[0]  # type = ?
        sp9_sel_12_195[1] = sp9_receive_205[0]  # type = ?
        sp9_button_200 = button()
        sp9_delay_5_2[0] = sp9_button_200[0]  # type = bang
        sp9_message_1 = message(text='0')
        sp9_counter_0_12_192[3] = sp9_message_1[0]  # type = ?
        sp9_receive_8 = receive(text='r Reinicio-cont')
        sp9_message_1[0] = sp9_receive_8[0]  # type = ?
        sp9_inlet_201 = inlet(comment='', index=1)
        sp9_counter_0_12_192[0] = sp9_inlet_201[0]  # type = bang
        sp9_button_200[0] = sp9_inlet_201[0]  # type = bang

    #----------------------------------------------------------------------
    def subpatcher_10(sp10_inlet_78, sp10_inlet_18):
        sp10_outlet_1 = outlet(comment='', index=1)
        sp10_int_95 = int(text='int')
        sp10_outlet_1[0] = sp10_int_95[0]  # type = int
        sp10_number_9 = number(parameter_enable=0)
        sp10_int_95[1] = sp10_number_9[0]  # type = ?
        sp10_gswitch2_41 = gswitch2(parameter_enable=0)
        sp10_number_9[0] = sp10_gswitch2_41[0]  # type = ?
        sp10_int_89 = int(text='int')
        sp10_outlet_1[0] = sp10_int_89[0]  # type = int
        sp10_button_88 = button()
        sp10_int_89[0] = sp10_button_88[0]  # type = bang
        sp10_int_103 = int(text='int')
        sp10_outlet_1[0] = sp10_int_103[0]  # type = int
        sp10_button_102 = button()
        sp10_int_103[0] = sp10_button_102[0]  # type = bang
        sp10_int_105 = int(text='int')
        sp10_outlet_1[0] = sp10_int_105[0]  # type = int
        sp10_button_104 = button()
        sp10_int_105[0] = sp10_button_104[0]  # type = bang
        sp10_int_99 = int(text='int')
        sp10_outlet_1[0] = sp10_int_99[0]  # type = int
        sp10_button_98 = button()
        sp10_int_99[0] = sp10_button_98[0]  # type = bang
        sp10_int_113 = int(text='int')
        sp10_outlet_1[0] = sp10_int_113[0]  # type = int
        sp10_button_112 = button()
        sp10_int_113[0] = sp10_button_112[0]  # type = bang
        sp10_int_107 = int(text='int')
        sp10_outlet_1[0] = sp10_int_107[0]  # type = int
        sp10_button_106 = button()
        sp10_int_107[0] = sp10_button_106[0]  # type = bang
        sp10_int_117 = int(text='int')
        sp10_outlet_1[0] = sp10_int_117[0]  # type = int
        sp10_button_116 = button()
        sp10_int_117[0] = sp10_button_116[0]  # type = bang
        sp10_int_83 = int(text='int')
        sp10_outlet_1[0] = sp10_int_83[0]  # type = int
        sp10_button_85 = button()
        sp10_int_83[0] = sp10_button_85[0]  # type = bang
        sp10_int_97 = int(text='int')
        sp10_outlet_1[0] = sp10_int_97[0]  # type = int
        sp10_button_96 = button()
        sp10_int_97[0] = sp10_button_96[0]  # type = bang
        sp10_int_109 = int(text='int')
        sp10_outlet_1[0] = sp10_int_109[0]  # type = int
        sp10_button_108 = button()
        sp10_int_109[0] = sp10_button_108[0]  # type = bang
        sp10_int_111 = int(text='int')
        sp10_outlet_1[0] = sp10_int_111[0]  # type = int
        sp10_button_110 = button()
        sp10_int_111[0] = sp10_button_110[0]  # type = bang
        sp10_int_87 = int(text='int')
        sp10_outlet_1[0] = sp10_int_87[0]  # type = int
        sp10_button_86 = button()
        sp10_int_87[0] = sp10_button_86[0]  # type = bang
        sp10_button_94 = button()
        sp10_int_95[0] = sp10_button_94[0]  # type = bang
        sp10_int_93 = int(text='int')
        sp10_outlet_1[0] = sp10_int_93[0]  # type = int
        sp10_button_92 = button()
        sp10_int_93[0] = sp10_button_92[0]  # type = bang
        sp10_int_91 = int(text='int')
        sp10_outlet_1[0] = sp10_int_91[0]  # type = int
        sp10_button_90 = button()
        sp10_int_91[0] = sp10_button_90[0]  # type = bang
        sp10_int_101 = int(text='int')
        sp10_outlet_1[0] = sp10_int_101[0]  # type = int
        sp10_button_100 = button()
        sp10_int_101[0] = sp10_button_100[0]  # type = bang
        sp10_int_115 = int(text='int')
        sp10_outlet_1[0] = sp10_int_115[0]  # type = int
        sp10_button_114 = button()
        sp10_int_115[0] = sp10_button_114[0]  # type = bang
        sp10_sel_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_82 = newobj(text='sel 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17')
        sp10_button_85[0] = sp10_sel_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_82[0]  # type = bang
        sp10_button_86[0] = sp10_sel_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_82[1]  # type = bang
        sp10_button_90[0] = sp10_sel_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_82[2]  # type = bang
        sp10_button_88[0] = sp10_sel_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_82[3]  # type = bang
        sp10_button_94[0] = sp10_sel_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_82[4]  # type = bang
        sp10_button_92[0] = sp10_sel_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_82[5]  # type = bang
        sp10_button_106[0] = sp10_sel_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_82[6]  # type = bang
        sp10_button_104[0] = sp10_sel_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_82[7]  # type = bang
        sp10_button_102[0] = sp10_sel_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_82[8]  # type = bang
        sp10_button_100[0] = sp10_sel_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_82[9]  # type = bang
        sp10_button_98[0] = sp10_sel_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_82[10]  # type = bang
        sp10_button_96[0] = sp10_sel_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_82[11]  # type = bang
        sp10_button_116[0] = sp10_sel_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_82[12]  # type = bang
        sp10_button_114[0] = sp10_sel_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_82[13]  # type = bang
        sp10_button_112[0] = sp10_sel_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_82[14]  # type = bang
        sp10_button_110[0] = sp10_sel_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_82[15]  # type = bang
        sp10_button_108[0] = sp10_sel_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_82[16]  # type = bang
        sp10_number_120 = number(parameter_enable=0)
        sp10_sel_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_82[0] = sp10_number_120[0]  # type = ?
        sp10_number_46 = number(parameter_enable=0)
        sp10_int_97[1] = sp10_number_46[0]  # type = ?
        sp10_gswitch2_19 = gswitch2(int=1, parameter_enable=0)
        sp10_number_46[0] = sp10_gswitch2_19[0]  # type = ?
        sp10_toggle_20 = toggle(parameter_enable=0)
        sp10_gswitch2_19[0] = sp10_toggle_20[0]  # type = int
        sp10_number_15 = number(parameter_enable=0)
        sp10_int_103[1] = sp10_number_15[0]  # type = ?
        sp10_gswitch2_43 = gswitch2(parameter_enable=0)
        sp10_number_15[0] = sp10_gswitch2_43[0]  # type = ?
        sp10_toggle_44 = toggle(parameter_enable=0)
        sp10_gswitch2_43[0] = sp10_toggle_44[0]  # type = int
        sp10_number_66 = number(parameter_enable=0)
        sp10_int_113[1] = sp10_number_66[0]  # type = ?
        sp10_gswitch2_57 = gswitch2(int=1, parameter_enable=0)
        sp10_number_66[0] = sp10_gswitch2_57[0]  # type = ?
        sp10_toggle_58 = toggle(parameter_enable=0)
        sp10_gswitch2_57[0] = sp10_toggle_58[0]  # type = int
        sp10_message_79 = message(text='11')
        sp10_message_69 = message(linecount=2, text='0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1')
        sp10_number_17 = number(parameter_enable=0)
        sp10_int_105[1] = sp10_number_17[0]  # type = ?
        sp10_gswitch2_35 = gswitch2(parameter_enable=0)
        sp10_number_17[0] = sp10_gswitch2_35[0]  # type = ?
        sp10_toggle_36 = toggle(parameter_enable=0)
        sp10_gswitch2_35[0] = sp10_toggle_36[0]  # type = int
        sp10_number_7 = number(parameter_enable=0)
        sp10_int_89[1] = sp10_number_7[0]  # type = ?
        sp10_gswitch2_33 = gswitch2(parameter_enable=0)
        sp10_number_7[0] = sp10_gswitch2_33[0]  # type = ?
        sp10_toggle_34 = toggle(parameter_enable=0)
        sp10_gswitch2_33[0] = sp10_toggle_34[0]  # type = int
        sp10_number_25 = number(parameter_enable=0)
        sp10_int_83[1] = sp10_number_25[0]  # type = ?
        sp10_gswitch2_22 = gswitch2(parameter_enable=0)
        sp10_number_25[0] = sp10_gswitch2_22[0]  # type = ?
        sp10_toggle_21 = toggle(parameter_enable=0)
        sp10_gswitch2_22[0] = sp10_toggle_21[0]  # type = int
        sp10_number_6 = number(parameter_enable=0)
        sp10_int_91[1] = sp10_number_6[0]  # type = ?
        sp10_gswitch2_31 = gswitch2(parameter_enable=0)
        sp10_number_6[0] = sp10_gswitch2_31[0]  # type = ?
        sp10_toggle_32 = toggle(parameter_enable=0)
        sp10_gswitch2_31[0] = sp10_toggle_32[0]  # type = int
        sp10_number_13 = number(parameter_enable=0)
        sp10_int_93[1] = sp10_number_13[0]  # type = ?
        sp10_gswitch2_39 = gswitch2(parameter_enable=0)
        sp10_number_13[0] = sp10_gswitch2_39[0]  # type = ?
        sp10_toggle_40 = toggle(parameter_enable=0)
        sp10_gswitch2_39[0] = sp10_toggle_40[0]  # type = int
        sp10_number_75 = number(parameter_enable=0)
        sp10_int_115[1] = sp10_number_75[0]  # type = ?
        sp10_gswitch2_68 = gswitch2(int=1, parameter_enable=0)
        sp10_number_75[0] = sp10_gswitch2_68[0]  # type = ?
        sp10_number_61 = number(parameter_enable=0)
        sp10_int_109[1] = sp10_number_61[0]  # type = ?
        sp10_gswitch2_53 = gswitch2(int=1, parameter_enable=0)
        sp10_number_61[0] = sp10_gswitch2_53[0]  # type = ?
        sp10_number_24 = number(parameter_enable=0)
        sp10_int_87[1] = sp10_number_24[0]  # type = ?
        sp10_gswitch2_29 = gswitch2(parameter_enable=0)
        sp10_number_24[0] = sp10_gswitch2_29[0]  # type = ?
        sp10_number_11 = number(parameter_enable=0)
        sp10_int_107[1] = sp10_number_11[0]  # type = ?
        sp10_gswitch2_37 = gswitch2(parameter_enable=0)
        sp10_number_11[0] = sp10_gswitch2_37[0]  # type = ?
        sp10_number_48 = number(parameter_enable=0)
        sp10_int_99[1] = sp10_number_48[0]  # type = ?
        sp10_gswitch2_23 = gswitch2(parameter_enable=0)
        sp10_number_48[0] = sp10_gswitch2_23[0]  # type = ?
        sp10_number_63 = number(parameter_enable=0)
        sp10_int_111[1] = sp10_number_63[0]  # type = ?
        sp10_gswitch2_55 = gswitch2(int=1, parameter_enable=0)
        sp10_number_63[0] = sp10_gswitch2_55[0]  # type = ?
        sp10_number_51 = number(parameter_enable=0)
        sp10_int_101[1] = sp10_number_51[0]  # type = ?
        sp10_gswitch2_27 = gswitch2(parameter_enable=0)
        sp10_number_51[0] = sp10_gswitch2_27[0]  # type = ?
        sp10_number_77 = number(parameter_enable=0)
        sp10_int_117[1] = sp10_number_77[0]  # type = ?
        sp10_gswitch2_71 = gswitch2(int=1, parameter_enable=0)
        sp10_number_77[0] = sp10_gswitch2_71[0]  # type = ?
        sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_2 = newobj(text='unpack i i i i i i i i i i i i i i i i i')
        sp10_gswitch2_22[1] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_2[0]  # type = int
        sp10_gswitch2_29[1] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_2[1]  # type = int
        sp10_gswitch2_31[1] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_2[2]  # type = int
        sp10_gswitch2_33[1] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_2[3]  # type = int
        sp10_gswitch2_41[1] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_2[4]  # type = int
        sp10_gswitch2_39[1] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_2[5]  # type = int
        sp10_gswitch2_37[1] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_2[6]  # type = int
        sp10_gswitch2_35[1] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_2[7]  # type = int
        sp10_gswitch2_43[1] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_2[8]  # type = int
        sp10_gswitch2_27[1] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_2[9]  # type = int
        sp10_gswitch2_23[1] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_2[10]  # type = int
        sp10_gswitch2_19[1] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_2[11]  # type = int
        sp10_gswitch2_71[1] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_2[12]  # type = int
        sp10_gswitch2_68[1] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_2[13]  # type = int
        sp10_gswitch2_57[1] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_2[14]  # type = int
        sp10_gswitch2_55[1] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_2[15]  # type = int
        sp10_gswitch2_53[1] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_2[16]  # type = int
        sp10_message_50 = message(text='61 137 143 159 169 171 145 146 150 162 166')
        sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_2[0] = sp10_message_50[0]  # type = ?
        sp10_button_72 = button()
        sp10_message_50[0] = sp10_button_72[0]  # type = bang
        sp10_toggle_28 = toggle(parameter_enable=0)
        sp10_gswitch2_27[0] = sp10_toggle_28[0]  # type = int
        sp10_toggle_56 = toggle(parameter_enable=0)
        sp10_gswitch2_55[0] = sp10_toggle_56[0]  # type = int
        sp10_toggle_54 = toggle(parameter_enable=0)
        sp10_gswitch2_53[0] = sp10_toggle_54[0]  # type = int
        sp10_toggle_70 = toggle(parameter_enable=0)
        sp10_gswitch2_68[0] = sp10_toggle_70[0]  # type = int
        sp10_toggle_26 = toggle(parameter_enable=0)
        sp10_gswitch2_23[0] = sp10_toggle_26[0]  # type = int
        sp10_toggle_38 = toggle(parameter_enable=0)
        sp10_gswitch2_37[0] = sp10_toggle_38[0]  # type = int
        sp10_toggle_30 = toggle(parameter_enable=0)
        sp10_gswitch2_29[0] = sp10_toggle_30[0]  # type = int
        sp10_toggle_73 = toggle(parameter_enable=0)
        sp10_gswitch2_71[0] = sp10_toggle_73[0]  # type = int
        sp10_toggle_42 = toggle(parameter_enable=0)
        sp10_gswitch2_41[0] = sp10_toggle_42[0]  # type = int
        sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_60 = newobj(text='unpack i i i i i i i i i i i i i i i i i')
        sp10_toggle_21[0] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_60[0]  # type = int
        sp10_button_72[0] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_60[0]  # type = int
        sp10_toggle_30[0] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_60[1]  # type = int
        sp10_toggle_32[0] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_60[2]  # type = int
        sp10_toggle_34[0] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_60[3]  # type = int
        sp10_toggle_42[0] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_60[4]  # type = int
        sp10_toggle_40[0] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_60[5]  # type = int
        sp10_toggle_38[0] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_60[6]  # type = int
        sp10_toggle_36[0] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_60[7]  # type = int
        sp10_toggle_44[0] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_60[8]  # type = int
        sp10_toggle_28[0] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_60[9]  # type = int
        sp10_toggle_26[0] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_60[10]  # type = int
        sp10_toggle_20[0] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_60[11]  # type = int
        sp10_toggle_73[0] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_60[12]  # type = int
        sp10_toggle_70[0] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_60[13]  # type = int
        sp10_toggle_58[0] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_60[14]  # type = int
        sp10_toggle_56[0] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_60[15]  # type = int
        sp10_toggle_54[0] = sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_60[16]  # type = int
        sp10_coll_classcomp_65 = newobj(text='coll ClassComp')
        sp10_unpack_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_60[0] = sp10_coll_classcomp_65[0]  # type = ?
        sp10_message_69[1] = sp10_coll_classcomp_65[0]  # type = ?
        sp10_send_118 = send(text='s ComplLen')
        sp10_zllen_52 = newobj(text='zl.len')
        sp10_send_118[0] = sp10_zllen_52[0]  # type = ?
        sp10_coll_classcomp_65[0] = sp10_zllen_52[0]  # type = ?
        sp10_message_79[1] = sp10_zllen_52[0]  # type = ?
        sp10_fromsymbol_80 = fromsymbol(text='fromsymbol')
        sp10_message_50[1] = sp10_fromsymbol_80[0]  # type = ?
        sp10_zllen_52[0] = sp10_fromsymbol_80[0]  # type = ?
        sp10_inlet_78 = inlet(comment='', index=2)
        sp10_number_120[0] = sp10_inlet_78[0]  # type = ?
        sp10_route_symbol_81 = newobj(text='route symbol')
        sp10_fromsymbol_80[0] = sp10_route_symbol_81[0]  # type = ?
        sp10_inlet_18 = inlet(comment='', index=1)
        sp10_route_symbol_81[0] = sp10_inlet_18[0]  # type = ?
        sp10_message_67 = message(text='open')
        sp10_coll_classcomp_65[0] = sp10_message_67[0]  # type = ?
        return sp10_outlet_1

    #----------------------------------------------------------------------
    def subpatcher_11(sp11_inlet_70):
        sp11_outlet_102 = outlet(comment='', index=5)
        sp11_flonum_77 = flonum(format=6, parameter_enable=0)
        sp11_outlet_102[0] = sp11_flonum_77[0]  # type = ?
        sp11_outlet_98 = outlet(comment='', index=2)
        sp11_flonum_80 = flonum(format=6, parameter_enable=0)
        sp11_outlet_98[0] = sp11_flonum_80[0]  # type = ?
        sp11_outlet_110 = outlet(comment='', index=13)
        sp11_flonum_83 = flonum(format=6, parameter_enable=0)
        sp11_outlet_110[0] = sp11_flonum_83[0]  # type = ?
        sp11_outlet_101 = outlet(comment='', index=4)
        sp11_flonum_78 = flonum(format=6, parameter_enable=0)
        sp11_outlet_101[0] = sp11_flonum_78[0]  # type = ?
        sp11_outlet_107 = outlet(comment='', index=10)
        sp11_flonum_72 = flonum(format=6, parameter_enable=0)
        sp11_outlet_107[0] = sp11_flonum_72[0]  # type = ?
        sp11_outlet_104 = outlet(comment='', index=7)
        sp11_flonum_75 = flonum(format=6, parameter_enable=0)
        sp11_outlet_104[0] = sp11_flonum_75[0]  # type = ?
        sp11_outlet_103 = outlet(comment='', index=6)
        sp11_flonum_76 = flonum(format=6, parameter_enable=0)
        sp11_outlet_103[0] = sp11_flonum_76[0]  # type = ?
        sp11_outlet_109 = outlet(comment='', index=12)
        sp11_flonum_82 = flonum(format=6, parameter_enable=0)
        sp11_outlet_109[0] = sp11_flonum_82[0]  # type = ?
        sp11_outlet_100 = outlet(comment='', index=3)
        sp11_flonum_79 = flonum(format=6, parameter_enable=0)
        sp11_outlet_100[0] = sp11_flonum_79[0]  # type = ?
        sp11_outlet_106 = outlet(comment='', index=9)
        sp11_flonum_73 = flonum(format=6, parameter_enable=0)
        sp11_outlet_106[0] = sp11_flonum_73[0]  # type = ?
        sp11_outlet_105 = outlet(comment='', index=8)
        sp11_flonum_74 = flonum(format=6, parameter_enable=0)
        sp11_outlet_105[0] = sp11_flonum_74[0]  # type = ?
        sp11_outlet_108 = outlet(comment='', index=11)
        sp11_flonum_71 = flonum(format=6, parameter_enable=0)
        sp11_outlet_108[0] = sp11_flonum_71[0]  # type = ?
        sp11_p_pcsetrhy_81 = _subpatcher(subpatcher_id=0, text='p PCSetRhy')
        sp11_flonum_80[0] = sp11_p_pcsetrhy_81[0]  # type = ?
        sp11_flonum_79[0] = sp11_p_pcsetrhy_81[1]  # type = ?
        sp11_flonum_78[0] = sp11_p_pcsetrhy_81[2]  # type = ?
        sp11_flonum_77[0] = sp11_p_pcsetrhy_81[3]  # type = ?
        sp11_flonum_76[0] = sp11_p_pcsetrhy_81[4]  # type = ?
        sp11_flonum_75[0] = sp11_p_pcsetrhy_81[5]  # type = ?
        sp11_flonum_74[0] = sp11_p_pcsetrhy_81[6]  # type = ?
        sp11_flonum_73[0] = sp11_p_pcsetrhy_81[7]  # type = ?
        sp11_flonum_72[0] = sp11_p_pcsetrhy_81[8]  # type = ?
        sp11_flonum_71[0] = sp11_p_pcsetrhy_81[9]  # type = ?
        sp11_flonum_82[0] = sp11_p_pcsetrhy_81[10]  # type = ?
        sp11_flonum_83[0] = sp11_p_pcsetrhy_81[11]  # type = ?
        sp11_outlet_99 = outlet(comment='', index=14)
        sp11_flonum_1 = flonum(format=6, parameter_enable=0)
        sp11_outlet_99[0] = sp11_flonum_1[0]  # type = ?
        sp11_maximum_63 = maximum(text='maximum')
        sp11_flonum_1[0] = sp11_maximum_63[0]  # type = int
        sp11_message_69 = message(text='27')
        sp11_zllen_67 = newobj(text='zl.len')
        sp11_message_69[1] = sp11_zllen_67[0]  # type = ?
        sp11_route_rhythm_55 = newobj(text='route /rhythm')
        sp11_maximum_63[0] = sp11_route_rhythm_55[0]  # type = ?
        sp11_zllen_67[0] = sp11_route_rhythm_55[0]  # type = ?
        sp11_p_pcsetrhy_81[1] = sp11_route_rhythm_55[0]  # type = ?
        sp11_message_31 = message(linecount=2, text='/rhythm 160.690582 482.071747 642.762329 803.452881 964.143494 1285.524658 1446.21521 321.381165 482.071747 642.762329 803.452881 1124.834106 1285.524658 160.690582 321.381165 482.071747 803.452881 964.143494 160.690582 321.381165 642.762329 803.452881 160.690582 482.071747 642.762329 321.381165 482.071747')
        sp11_route_rhythm_55[0] = sp11_message_31[0]  # type = ?
        sp11_outlet_96 = outlet(comment='', index=1)
        sp11_number_86 = number(parameter_enable=0)
        sp11_p_pcsetrhy_81[0] = sp11_number_86[0]  # type = ?
        sp11_outlet_96[0] = sp11_number_86[0]  # type = ?
        sp11_receive_142 = receive(text='r cardinal')
        sp11_number_86[0] = sp11_receive_142[0]  # type = ?
        sp11_t_b_l_52 = newobj(text='t b l')
        sp11_message_31[0] = sp11_t_b_l_52[0]  # type = bang
        sp11_number_86[0] = sp11_t_b_l_52[0]  # type = bang
        sp11_message_31[1] = sp11_t_b_l_52[1]  # type = ?
        sp11_inlet_70 = inlet(comment='', index=1)
        sp11_t_b_l_52[0] = sp11_inlet_70[0]  # type = ?
        return sp11_outlet_96, sp11_outlet_98, sp11_outlet_99, sp11_outlet_100, sp11_outlet_101, sp11_outlet_102, sp11_outlet_103, sp11_outlet_104, sp11_outlet_105, sp11_outlet_106, sp11_outlet_107, sp11_outlet_108, sp11_outlet_109, sp11_outlet_110

    #----------------------------------------------------------------------
    def subpatcher_0(sp11_sp0_inlet_18, sp11_sp0_inlet_106):
        sp11_sp0_outlet_16 = outlet(comment='', index=8)
        sp11_sp0_flonum_17 = flonum(format=6, parameter_enable=0)
        sp11_sp0_outlet_16[0] = sp11_sp0_flonum_17[0]  # type = ?
        sp11_sp0_gswitch2_35 = gswitch2(parameter_enable=0)
        sp11_sp0_flonum_17[0] = sp11_sp0_gswitch2_35[0]  # type = ?
        sp11_sp0_toggle_36 = toggle(parameter_enable=0)
        sp11_sp0_gswitch2_35[0] = sp11_sp0_toggle_36[0]  # type = int
        sp11_sp0_outlet_8 = outlet(comment='', index=5)
        sp11_sp0_flonum_9 = flonum(format=6, parameter_enable=0)
        sp11_sp0_outlet_8[0] = sp11_sp0_flonum_9[0]  # type = ?
        sp11_sp0_gswitch2_41 = gswitch2(parameter_enable=0)
        sp11_sp0_flonum_9[0] = sp11_sp0_gswitch2_41[0]  # type = ?
        sp11_sp0_outlet_45 = outlet(comment='', index=12)
        sp11_sp0_flonum_46 = flonum(format=6, parameter_enable=0)
        sp11_sp0_outlet_45[0] = sp11_sp0_flonum_46[0]  # type = ?
        sp11_sp0_gswitch2_19 = gswitch2(int=1, parameter_enable=0)
        sp11_sp0_flonum_46[0] = sp11_sp0_gswitch2_19[0]  # type = ?
        sp11_sp0_toggle_20 = toggle(parameter_enable=0)
        sp11_sp0_gswitch2_19[0] = sp11_sp0_toggle_20[0]  # type = int
        sp11_sp0_outlet_14 = outlet(comment='', index=9)
        sp11_sp0_flonum_15 = flonum(format=6, parameter_enable=0)
        sp11_sp0_outlet_14[0] = sp11_sp0_flonum_15[0]  # type = ?
        sp11_sp0_gswitch2_43 = gswitch2(parameter_enable=0)
        sp11_sp0_flonum_15[0] = sp11_sp0_gswitch2_43[0]  # type = ?
        sp11_sp0_toggle_44 = toggle(parameter_enable=0)
        sp11_sp0_gswitch2_43[0] = sp11_sp0_toggle_44[0]  # type = int
        sp11_sp0_outlet_5 = outlet(comment='', index=3)
        sp11_sp0_flonum_6 = flonum(format=6, parameter_enable=0)
        sp11_sp0_outlet_5[0] = sp11_sp0_flonum_6[0]  # type = ?
        sp11_sp0_gswitch2_31 = gswitch2(parameter_enable=0)
        sp11_sp0_flonum_6[0] = sp11_sp0_gswitch2_31[0]  # type = ?
        sp11_sp0_outlet_4 = outlet(comment='', index=4)
        sp11_sp0_flonum_7 = flonum(format=6, parameter_enable=0)
        sp11_sp0_outlet_4[0] = sp11_sp0_flonum_7[0]  # type = ?
        sp11_sp0_gswitch2_33 = gswitch2(parameter_enable=0)
        sp11_sp0_flonum_7[0] = sp11_sp0_gswitch2_33[0]  # type = ?
        sp11_sp0_outlet_3 = outlet(comment='', index=2)
        sp11_sp0_flonum_24 = flonum(format=6, parameter_enable=0)
        sp11_sp0_outlet_3[0] = sp11_sp0_flonum_24[0]  # type = ?
        sp11_sp0_gswitch2_29 = gswitch2(parameter_enable=0)
        sp11_sp0_flonum_24[0] = sp11_sp0_gswitch2_29[0]  # type = ?
        sp11_sp0_outlet_10 = outlet(comment='', index=7)
        sp11_sp0_flonum_11 = flonum(format=6, parameter_enable=0)
        sp11_sp0_outlet_10[0] = sp11_sp0_flonum_11[0]  # type = ?
        sp11_sp0_gswitch2_37 = gswitch2(parameter_enable=0)
        sp11_sp0_flonum_11[0] = sp11_sp0_gswitch2_37[0]  # type = ?
        sp11_sp0_outlet_12 = outlet(comment='', index=6)
        sp11_sp0_flonum_13 = flonum(format=6, parameter_enable=0)
        sp11_sp0_outlet_12[0] = sp11_sp0_flonum_13[0]  # type = ?
        sp11_sp0_gswitch2_39 = gswitch2(parameter_enable=0)
        sp11_sp0_flonum_13[0] = sp11_sp0_gswitch2_39[0]  # type = ?
        sp11_sp0_outlet_47 = outlet(comment='', index=11)
        sp11_sp0_flonum_48 = flonum(format=6, parameter_enable=0)
        sp11_sp0_outlet_47[0] = sp11_sp0_flonum_48[0]  # type = ?
        sp11_sp0_gswitch2_23 = gswitch2(int=1, parameter_enable=0)
        sp11_sp0_flonum_48[0] = sp11_sp0_gswitch2_23[0]  # type = ?
        sp11_sp0_outlet_1 = outlet(comment='', index=1)
        sp11_sp0_flonum_25 = flonum(format=6, parameter_enable=0)
        sp11_sp0_outlet_1[0] = sp11_sp0_flonum_25[0]  # type = ?
        sp11_sp0_gswitch2_22 = gswitch2(parameter_enable=0)
        sp11_sp0_flonum_25[0] = sp11_sp0_gswitch2_22[0]  # type = ?
        sp11_sp0_outlet_49 = outlet(comment='', index=10)
        sp11_sp0_flonum_51 = flonum(format=6, parameter_enable=0)
        sp11_sp0_outlet_49[0] = sp11_sp0_flonum_51[0]  # type = ?
        sp11_sp0_gswitch2_27 = gswitch2(parameter_enable=0)
        sp11_sp0_flonum_51[0] = sp11_sp0_gswitch2_27[0]  # type = ?
        sp11_sp0_unpack_f_f_f_f_f_f_f_f_f_f_f_f_l_2 = newobj(text='unpack f f f f f f f f f f f f l')
        sp11_sp0_gswitch2_22[1] = sp11_sp0_unpack_f_f_f_f_f_f_f_f_f_f_f_f_l_2[0]  # type = float
        sp11_sp0_gswitch2_29[1] = sp11_sp0_unpack_f_f_f_f_f_f_f_f_f_f_f_f_l_2[1]  # type = float
        sp11_sp0_gswitch2_31[1] = sp11_sp0_unpack_f_f_f_f_f_f_f_f_f_f_f_f_l_2[2]  # type = float
        sp11_sp0_gswitch2_33[1] = sp11_sp0_unpack_f_f_f_f_f_f_f_f_f_f_f_f_l_2[3]  # type = float
        sp11_sp0_gswitch2_41[1] = sp11_sp0_unpack_f_f_f_f_f_f_f_f_f_f_f_f_l_2[4]  # type = float
        sp11_sp0_gswitch2_39[1] = sp11_sp0_unpack_f_f_f_f_f_f_f_f_f_f_f_f_l_2[5]  # type = float
        sp11_sp0_gswitch2_37[1] = sp11_sp0_unpack_f_f_f_f_f_f_f_f_f_f_f_f_l_2[6]  # type = float
        sp11_sp0_gswitch2_35[1] = sp11_sp0_unpack_f_f_f_f_f_f_f_f_f_f_f_f_l_2[7]  # type = float
        sp11_sp0_gswitch2_43[1] = sp11_sp0_unpack_f_f_f_f_f_f_f_f_f_f_f_f_l_2[8]  # type = float
        sp11_sp0_gswitch2_27[1] = sp11_sp0_unpack_f_f_f_f_f_f_f_f_f_f_f_f_l_2[9]  # type = float
        sp11_sp0_gswitch2_23[1] = sp11_sp0_unpack_f_f_f_f_f_f_f_f_f_f_f_f_l_2[10]  # type = float
        sp11_sp0_gswitch2_19[1] = sp11_sp0_unpack_f_f_f_f_f_f_f_f_f_f_f_f_l_2[11]  # type = float
        sp11_sp0_message_50 = message(linecount=4, text='160.690582 482.071747 642.762329 803.452881 964.143494 1285.524658 1446.21521 321.381165 482.071747 642.762329 803.452881 1124.834106 1285.524658 160.690582 321.381165 482.071747 803.452881 964.143494 160.690582 321.381165 642.762329 803.452881 160.690582 482.071747 642.762329 321.381165 482.071747')
        sp11_sp0_unpack_f_f_f_f_f_f_f_f_f_f_f_f_l_2[0] = sp11_sp0_message_50[0]  # type = ?
        sp11_sp0_inlet_106 = inlet(comment='', index=2)
        sp11_sp0_message_50[1] = sp11_sp0_inlet_106[0]  # type = ?
        sp11_sp0_toggle_34 = toggle(parameter_enable=0)
        sp11_sp0_gswitch2_33[0] = sp11_sp0_toggle_34[0]  # type = int
        sp11_sp0_toggle_21 = toggle(parameter_enable=0)
        sp11_sp0_gswitch2_22[0] = sp11_sp0_toggle_21[0]  # type = int
        sp11_sp0_toggle_32 = toggle(parameter_enable=0)
        sp11_sp0_gswitch2_31[0] = sp11_sp0_toggle_32[0]  # type = int
        sp11_sp0_toggle_40 = toggle(parameter_enable=0)
        sp11_sp0_gswitch2_39[0] = sp11_sp0_toggle_40[0]  # type = int
        sp11_sp0_button_72 = button()
        sp11_sp0_message_50[0] = sp11_sp0_button_72[0]  # type = bang
        sp11_sp0_toggle_28 = toggle(parameter_enable=0)
        sp11_sp0_gswitch2_27[0] = sp11_sp0_toggle_28[0]  # type = int
        sp11_sp0_toggle_26 = toggle(parameter_enable=0)
        sp11_sp0_gswitch2_23[0] = sp11_sp0_toggle_26[0]  # type = int
        sp11_sp0_toggle_38 = toggle(parameter_enable=0)
        sp11_sp0_gswitch2_37[0] = sp11_sp0_toggle_38[0]  # type = int
        sp11_sp0_toggle_30 = toggle(parameter_enable=0)
        sp11_sp0_gswitch2_29[0] = sp11_sp0_toggle_30[0]  # type = int
        sp11_sp0_toggle_42 = toggle(parameter_enable=0)
        sp11_sp0_gswitch2_41[0] = sp11_sp0_toggle_42[0]  # type = int
        sp11_sp0_unpack_f_f_f_f_f_f_f_f_f_f_f_f_l_60 = newobj(text='unpack f f f f f f f f f f f f l')
        sp11_sp0_toggle_21[0] = sp11_sp0_unpack_f_f_f_f_f_f_f_f_f_f_f_f_l_60[0]  # type = float
        sp11_sp0_button_72[0] = sp11_sp0_unpack_f_f_f_f_f_f_f_f_f_f_f_f_l_60[0]  # type = float
        sp11_sp0_toggle_30[0] = sp11_sp0_unpack_f_f_f_f_f_f_f_f_f_f_f_f_l_60[1]  # type = float
        sp11_sp0_toggle_32[0] = sp11_sp0_unpack_f_f_f_f_f_f_f_f_f_f_f_f_l_60[2]  # type = float
        sp11_sp0_toggle_34[0] = sp11_sp0_unpack_f_f_f_f_f_f_f_f_f_f_f_f_l_60[3]  # type = float
        sp11_sp0_toggle_42[0] = sp11_sp0_unpack_f_f_f_f_f_f_f_f_f_f_f_f_l_60[4]  # type = float
        sp11_sp0_toggle_40[0] = sp11_sp0_unpack_f_f_f_f_f_f_f_f_f_f_f_f_l_60[5]  # type = float
        sp11_sp0_toggle_38[0] = sp11_sp0_unpack_f_f_f_f_f_f_f_f_f_f_f_f_l_60[6]  # type = float
        sp11_sp0_toggle_36[0] = sp11_sp0_unpack_f_f_f_f_f_f_f_f_f_f_f_f_l_60[7]  # type = float
        sp11_sp0_toggle_44[0] = sp11_sp0_unpack_f_f_f_f_f_f_f_f_f_f_f_f_l_60[8]  # type = float
        sp11_sp0_toggle_28[0] = sp11_sp0_unpack_f_f_f_f_f_f_f_f_f_f_f_f_l_60[9]  # type = float
        sp11_sp0_toggle_26[0] = sp11_sp0_unpack_f_f_f_f_f_f_f_f_f_f_f_f_l_60[10]  # type = float
        sp11_sp0_toggle_20[0] = sp11_sp0_unpack_f_f_f_f_f_f_f_f_f_f_f_f_l_60[11]  # type = float
        sp11_sp0_message_69 = message(linecount=2, text='0 0 0 0 0 0 0 0 0 0 1 1 1')
        sp11_sp0_coll_classrhy_65 = newobj(text='coll ClassRhy')
        sp11_sp0_unpack_f_f_f_f_f_f_f_f_f_f_f_f_l_60[0] = sp11_sp0_coll_classrhy_65[0]  # type = ?
        sp11_sp0_message_69[1] = sp11_sp0_coll_classrhy_65[0]  # type = ?
        sp11_sp0_number_70 = number(parameter_enable=0)
        sp11_sp0_coll_classrhy_65[0] = sp11_sp0_number_70[0]  # type = ?
        sp11_sp0_inlet_18 = inlet(comment='', index=1)
        sp11_sp0_number_70[0] = sp11_sp0_inlet_18[0]  # type = ?
        sp11_sp0_message_67 = message(text='open')
        sp11_sp0_coll_classrhy_65[0] = sp11_sp0_message_67[0]  # type = ?
        return sp11_sp0_outlet_45, sp11_sp0_outlet_47, sp11_sp0_outlet_49, sp11_sp0_outlet_14, sp11_sp0_outlet_16, sp11_sp0_outlet_10, sp11_sp0_outlet_12, sp11_sp0_outlet_8, sp11_sp0_outlet_4, sp11_sp0_outlet_5, sp11_sp0_outlet_3, sp11_sp0_outlet_1

    #----------------------------------------------------------------------
    def subpatcher_12(sp12_inlet_2, sp12_inlet_1):
        sp12_message_52 = message(linecount=2, text='10 "482.0717 642.7623 160.6906 482.0717 642.7623 803.4529 964.1435 1285.5247 1446.2152 1446.0000"')
        sp12_outlet_92 = outlet(comment='', index=1)
        sp12_message_46 = message(linecount=2, text='10 "482.0717 642.7623 160.6906 482.0717 642.7623 803.4529 964.1435 1285.5247 1446.2152 1446.0000"')
        sp12_message_52[1] = sp12_message_46[0]  # type = ?
        sp12_outlet_92[0] = sp12_message_46[0]  # type = ?
        sp12_button_68 = button()
        sp12_message_46[0] = sp12_button_68[0]  # type = bang
        sp12_send_62 = send(text='s out_R')
        sp12_message_39 = message(text='prepend $1')
        sp12_message_46[0] = sp12_message_39[0]  # type = ?
        sp12_button_68[0] = sp12_message_39[0]  # type = ?
        sp12_int_75 = int(text='int')
        sp12_message_39[0] = sp12_int_75[0]  # type = int
        sp12_if_i1__0_then_bang_72 = newobj(text='if $i1 != 0 then bang')
        sp12_int_75[0] = sp12_if_i1__0_then_bang_72[0]  # type = ?
        sp12_tosymbol_44 = tosymbol(text='tosymbol')
        sp12_message_46[1] = sp12_tosymbol_44[0]  # type = ?
        sp12_message_8 = message(linecount=2, text='482.071747 642.762329 160.690582 482.071747 642.762329 803.452881 964.143494 1285.524658 1446.21521 1446.')
        sp12_tosymbol_44[0] = sp12_message_8[0]  # type = ?
        sp12_number_20 = number(parameter_enable=0)
        sp12_if_i1__0_then_bang_72[0] = sp12_number_20[0]  # type = ?
        sp12_int_75[1] = sp12_number_20[0]  # type = ?
        sp12_message_8[0] = sp12_number_20[0]  # type = ?
        sp12_jitspill_plane_0_listlength_2_offset_0_0_5 = newobj(text='jit.spill @plane 0 @listlength 2 @offset 0 0')
        sp12_message_8[1] = sp12_jitspill_plane_0_listlength_2_offset_0_0_5[0]  # type = ?
        sp12_message_17 = message(text='offset 0 $1')
        sp12_jitspill_plane_0_listlength_2_offset_0_0_5[0] = sp12_message_17[0]  # type = ?
        sp12_counter_0_12_3 = newobj(text='counter 0 12')
        sp12_message_17[0] = sp12_counter_0_12_3[0]  # type = int
        sp12_number_20[0] = sp12_counter_0_12_3[0]  # type = int
        sp12_button_145 = button()
        sp12_counter_0_12_3[0] = sp12_button_145[0]  # type = bang
        sp12_if_i1__i2_then_bang_141 = newobj(text='if $i1 <= $i2 then bang')
        sp12_button_145[0] = sp12_if_i1__i2_then_bang_141[0]  # type = ?
        sp12_send_62[0] = sp12_if_i1__i2_then_bang_141[0]  # type = ?
        sp12_outlet_95 = outlet(comment='', index=2)
        sp12___0_93 = newobj(text='== 0')
        sp12_outlet_95[0] = sp12___0_93[0]  # type = int
        sp12_number_140 = number(parameter_enable=0)
        sp12_if_i1__i2_then_bang_141[0] = sp12_number_140[0]  # type = ?
        sp12___0_93[0] = sp12_number_140[0]  # type = ?
        sp12_accum_1_136 = newobj(text='accum 1')
        sp12_number_140[0] = sp12_accum_1_136[0]  # type = int
        sp12_message_138 = message(text='1')
        sp12_accum_1_136[1] = sp12_message_138[0]  # type = ?
        sp12_button_56 = button()
        sp12_accum_1_136[0] = sp12_button_56[0]  # type = bang
        sp12_message_138[0] = sp12_button_56[0]  # type = bang
        sp12_inlet_2 = inlet(comment='', index=2)
        sp12_jitspill_plane_0_listlength_2_offset_0_0_5[0] = sp12_inlet_2[0]  # type = jit_matrix
        sp12_button_56[0] = sp12_inlet_2[0]  # type = jit_matrix
        sp12_message_135 = message(text='set 0')
        sp12_accum_1_136[0] = sp12_message_135[0]  # type = ?
        sp12_message_27 = message(text='0')
        sp12_counter_0_12_3[2] = sp12_message_27[0]  # type = ?
        sp12_message_71 = message(text='clear')
        sp12_outlet_92[0] = sp12_message_71[0]  # type = ?
        sp12_button_12 = button()
        sp12_message_27[0] = sp12_button_12[0]  # type = bang
        sp12_message_71[0] = sp12_button_12[0]  # type = bang
        sp12_message_14 = message(text='listlength $1')
        sp12_jitspill_plane_0_listlength_2_offset_0_0_5[0] = sp12_message_14[0]  # type = ?
        sp12_number_22 = number(parameter_enable=0)
        sp12_message_14[0] = sp12_number_22[0]  # type = ?
        sp12_t_b_i_4 = newobj(text='t b i')
        sp12_counter_0_12_3[0] = sp12_t_b_i_4[0]  # type = bang
        sp12_message_135[0] = sp12_t_b_i_4[1]  # type = int
        sp12_if_i1__i2_then_bang_141[1] = sp12_t_b_i_4[1]  # type = int
        sp12_number_22[0] = sp12_t_b_i_4[1]  # type = int
        sp12_counter_0_12_3[4] = sp12_t_b_i_4[1]  # type = int
        sp12___0_93[1] = sp12_t_b_i_4[1]  # type = int
        sp12_inlet_1 = inlet(comment='', index=1)
        sp12_button_12[0] = sp12_inlet_1[0]  # type = int
        sp12_t_b_i_4[0] = sp12_inlet_1[0]  # type = int
# (comment) c.
# (comment) d.
# (comment) e.
# (comment) a.
# (comment) b.
        return sp12_outlet_95, sp12_outlet_92

    #----------------------------------------------------------------------
    def subpatcher_13(sp13_inlet_42, sp13_inlet_34, sp13_inlet_35, sp13_inlet_37, sp13_inlet_46, sp13_inlet_11, sp13_inlet_1, sp13_inlet_3, sp13_inlet_5, sp13_inlet_8, sp13_inlet_50, sp13_inlet_51, sp13_inlet_58, sp13_inlet_60):
        sp13___1_114 = newobj(text='- 1')
        sp13_counter_0_11_113[4] = sp13___1_114[0]  # type = int
        sp13_if_i1__i2_then_f3_else_f4_7[1] = sp13___1_114[0]  # type = int
        sp13___1_36 = newobj(text='+ 1')
        sp13_counter_1_12_130[2] = sp13___1_36[0]  # type = int
        sp13___3_72 = newobj(text='+ 3')
        sp13___1_114[0] = sp13___3_72[0]  # type = int
        sp13_counter_1_12_130[4] = sp13___3_72[0]  # type = int
        sp13_number_14[0] = sp13___3_72[0]  # type = int
        sp13_send_205[0] = sp13___3_72[0]  # type = int
        sp13_uzi_0_53[1] = sp13___3_72[0]  # type = int
        sp13_outlet_69[0] = sp13___3_72[0]  # type = int
        sp13_sel_i1_80[1] = sp13___3_72[0]  # type = int
        sp13_message_86[0] = sp13___3_72[0]  # type = int
        sp13_accum_1_136 = newobj(text='accum 1')
        sp13_number_140[0] = sp13_accum_1_136[0]  # type = int
        sp13_button_10 = button()
        sp13_uzi_0_53[0] = sp13_button_10[0]  # type = bang
        sp13_button_74[0] = sp13_button_10[0]  # type = bang
        sp13_button_102 = button()
        sp13_message_98[0] = sp13_button_102[0]  # type = bang
        sp13_button_110 = button()
        sp13_counter_0_12_124[0] = sp13_button_110[0]  # type = bang
        sp13_button_112 = button()
        sp13_counter_0_11_113[0] = sp13_button_112[0]  # type = bang
        sp13_button_12 = button()
        sp13_accum_1_136[0] = sp13_button_12[0]  # type = bang
        sp13_message_138[0] = sp13_button_12[0]  # type = bang
        sp13_button_145 = button()
        sp13_button_10[0] = sp13_button_145[0]  # type = bang
        sp13_button_191 = button()
        sp13_message_199[0] = sp13_button_191[0]  # type = bang
        sp13_button_193 = button()
        sp13_message_201[0] = sp13_button_193[0]  # type = bang
        sp13_button_195 = button()
        sp13_message_203[0] = sp13_button_195[0]  # type = bang
        sp13_button_197 = button()
        sp13_message_206[0] = sp13_button_197[0]  # type = bang
        sp13_button_24 = button()
        sp13_message_135[0] = sp13_button_24[0]  # type = bang
        sp13_message_147[0] = sp13_button_24[0]  # type = bang
        sp13_message_16[0] = sp13_button_24[0]  # type = bang
        sp13_message_18[0] = sp13_button_24[0]  # type = bang
        sp13_button_27 = button()
        sp13_jitmatrix_1_float32_4_4_30[0] = sp13_button_27[0]  # type = bang
        sp13_button_45 = button()
        sp13_message_44[0] = sp13_button_45[0]  # type = bang
        sp13_button_52 = button()
        sp13_message_48[0] = sp13_button_52[0]  # type = bang
        sp13_button_70 = button()
        sp13_message_67[0] = sp13_button_70[0]  # type = bang
        sp13_button_74 = button()
        sp13_flonum_13[0] = sp13_button_74[0]  # type = bang
        sp13_flonum_19[0] = sp13_button_74[0]  # type = bang
        sp13_flonum_20[0] = sp13_button_74[0]  # type = bang
        sp13_flonum_21[0] = sp13_button_74[0]  # type = bang
        sp13_flonum_22[0] = sp13_button_74[0]  # type = bang
        sp13_flonum_23[0] = sp13_button_74[0]  # type = bang
        sp13_flonum_26[0] = sp13_button_74[0]  # type = bang
        sp13_flonum_32[0] = sp13_button_74[0]  # type = bang
        sp13_flonum_6[0] = sp13_button_74[0]  # type = bang
        sp13_flonum_9[0] = sp13_button_74[0]  # type = bang
        sp13_button_91 = button()
        sp13_message_87[0] = sp13_button_91[0]  # type = bang
        sp13_button_92 = button()
        sp13_message_88[0] = sp13_button_92[0]  # type = bang
        sp13_button_93 = button()
        sp13_message_89[0] = sp13_button_93[0]  # type = bang
        sp13_button_94 = button()
        sp13_message_90[0] = sp13_button_94[0]  # type = bang
# (comment) Permutación cirular Matriz de Ritmo
# (comment) Set/Get contents
# (comment) value
# (comment) y position
# (comment) Create a matrix
# (comment) x position
# (comment) Sums Max at the end when inversion start from 0 to n-1
# (comment) a.
# (comment) b.
# (comment) c.
# (comment) d.
# (comment) e.
# (comment) Matrix
# (comment) Cardinal Number PcSet
        sp13_counter_0_11_113 = newobj(text='counter 0 11')
        sp13_number_245[0] = sp13_counter_0_11_113[0]  # type = int
        sp13_delay_10_39[0] = sp13_counter_0_11_113[3]  # type = int
        sp13_counter_0_12_124 = newobj(text='counter 0 12')
        sp13_number_121[0] = sp13_counter_0_12_124[0]  # type = int
        sp13_counter_1_12_130 = newobj(text='counter 1 12')
        sp13_number_188[0] = sp13_counter_1_12_130[0]  # type = int
        sp13_delay_10_39 = newobj(text='delay 10')
        sp13_button_110[0] = sp13_delay_10_39[0]  # type = bang
        sp13_flonum_13 = flonum(format=6, parameter_enable=0)
        sp13_message_67[1] = sp13_flonum_13[0]  # type = ?
        sp13_flonum_19 = flonum(format=6, parameter_enable=0)
        sp13_message_48[1] = sp13_flonum_19[0]  # type = ?
        sp13_flonum_2 = flonum(format=6, parameter_enable=0)
        sp13_button_10[0] = sp13_flonum_2[0]  # type = ?
        sp13_message_199[1] = sp13_flonum_2[0]  # type = ?
        sp13_button_24[0] = sp13_flonum_2[0]  # type = ?
        sp13_flonum_20 = flonum(format=6, parameter_enable=0)
        sp13_message_87[1] = sp13_flonum_20[0]  # type = ?
        sp13_flonum_21 = flonum(format=6, parameter_enable=0)
        sp13_message_88[1] = sp13_flonum_21[0]  # type = ?
        sp13_flonum_22 = flonum(format=6, parameter_enable=0)
        sp13_message_89[1] = sp13_flonum_22[0]  # type = ?
        sp13_flonum_23 = flonum(format=6, parameter_enable=0)
        sp13_message_90[1] = sp13_flonum_23[0]  # type = ?
        sp13_flonum_243 = flonum(format=6, mousefilter=1, parameter_enable=0, triscale=0.9)
        sp13_if_i1__i2_then_f3_else_f4_7[3] = sp13_flonum_243[0]  # type = ?
        sp13_flonum_26 = flonum(format=6, parameter_enable=0)
        sp13_message_98[1] = sp13_flonum_26[0]  # type = ?
        sp13_flonum_32 = flonum(format=6, parameter_enable=0)
        sp13_message_44[1] = sp13_flonum_32[0]  # type = ?
        sp13_flonum_4 = flonum(format=6, parameter_enable=0)
        sp13_message_201[1] = sp13_flonum_4[0]  # type = ?
        sp13_flonum_40 = flonum(format=6, parameter_enable=0)
        sp13_if_i1__i2_then_f3_else_f4_7[2] = sp13_flonum_40[0]  # type = ?
        sp13_flonum_6 = flonum(format=6, parameter_enable=0)
        sp13_message_203[1] = sp13_flonum_6[0]  # type = ?
        sp13_flonum_9 = flonum(format=6, parameter_enable=0)
        sp13_message_206[1] = sp13_flonum_9[0]  # type = ?
        sp13_if_i1__0_then_bang_29 = newobj(text='if $i1 != 0 then bang')
        sp13_button_112[0] = sp13_if_i1__0_then_bang_29[0]  # type = ?
        sp13_if_i1__0_then_bang_54 = newobj(text='if $i1 != 0 then bang')
        sp13_button_12[0] = sp13_if_i1__0_then_bang_54[0]  # type = ?
        sp13_if_i1__i2_then_bang_141 = newobj(text='if $i1 <= $i2 then bang')
        sp13_button_145[0] = sp13_if_i1__i2_then_bang_141[0]  # type = ?
        sp13_if_i1__i2_then_f3_else_f4_7 = newobj(text='if $i1 == $i2 then $f3 else $f4')
        sp13_pack_0_0_0_25[0] = sp13_if_i1__i2_then_f3_else_f4_7[0]  # type = ?
        sp13_inlet_1 = inlet(comment='', index=6)
        sp13_flonum_23[0] = sp13_inlet_1[0]  # type = ?
        sp13_inlet_11 = inlet(comment='', index=10)
        sp13_flonum_26[0] = sp13_inlet_11[0]  # type = ?
        sp13_inlet_3 = inlet(comment='', index=7)
        sp13_flonum_22[0] = sp13_inlet_3[0]  # type = ?
        sp13_inlet_34 = inlet(comment='', index=13)
        sp13_flonum_13[0] = sp13_inlet_34[0]  # type = ?
        sp13_inlet_35 = inlet(comment='', index=11)
        sp13_flonum_32[0] = sp13_inlet_35[0]  # type = ?
        sp13_inlet_37 = inlet(comment='', index=12)
        sp13_flonum_19[0] = sp13_inlet_37[0]  # type = ?
        sp13_inlet_42 = inlet(comment='', index=14)
        sp13_flonum_40[0] = sp13_inlet_42[0]  # type = ?
        sp13_inlet_46 = inlet(comment='', index=1)
        sp13_number_17[0] = sp13_inlet_46[0]  # type = ?
        sp13_inlet_5 = inlet(comment='', index=8)
        sp13_flonum_21[0] = sp13_inlet_5[0]  # type = ?
        sp13_inlet_50 = inlet(comment='', index=2)
        sp13_flonum_2[0] = sp13_inlet_50[0]  # type = ?
        sp13_inlet_51 = inlet(comment='', index=3)
        sp13_flonum_4[0] = sp13_inlet_51[0]  # type = ?
        sp13_inlet_58 = inlet(comment='', index=4)
        sp13_flonum_6[0] = sp13_inlet_58[0]  # type = ?
        sp13_inlet_60 = inlet(comment='', index=5)
        sp13_flonum_9[0] = sp13_inlet_60[0]  # type = ?
        sp13_inlet_8 = inlet(comment='', index=9)
        sp13_flonum_20[0] = sp13_inlet_8[0]  # type = ?
        sp13_jit.cellblock_33 = jit.cellblock(colwidth=40, rowheight=15)
        sp13_jitmatrix_1_float32_4_4_30 = newobj(text='jit.matrix 1 float32 4 4')
        sp13_jit.cellblock_33[0] = sp13_jitmatrix_1_float32_4_4_30[0]  # type = jit_matrix
        sp13_outlet_68[0] = sp13_jitmatrix_1_float32_4_4_30[0]  # type = jit_matrix
        sp13_jitprint_240 = newobj(text='jit.print')
        sp13_message_135 = message(text='set 0')
        sp13_accum_1_136[0] = sp13_message_135[0]  # type = ?
        sp13_message_138 = message(text='1')
        sp13_accum_1_136[1] = sp13_message_138[0]  # type = ?
        sp13_message_147 = message(text='0')
        sp13_counter_0_11_113[3] = sp13_message_147[0]  # type = ?
        sp13_counter_0_12_124[3] = sp13_message_147[0]  # type = ?
        sp13_counter_1_12_130[3] = sp13_message_147[0]  # type = ?
        sp13_send_76[0] = sp13_message_147[0]  # type = ?
        sp13_message_16 = message(text='1')
        sp13_counter_1_12_130[2] = sp13_message_16[0]  # type = ?
        sp13_message_18 = message(text='clear')
        sp13_jitmatrix_1_float32_4_4_30[0] = sp13_message_18[0]  # type = ?
        sp13_message_199 = message(linecount=5, text='160.690582')
        sp13_flonum_243[0] = sp13_message_199[0]  # type = ?
        sp13_message_201 = message(linecount=4, text='482.071747')
        sp13_flonum_243[0] = sp13_message_201[0]  # type = ?
        sp13_message_203 = message(linecount=5, text='642.762329')
        sp13_flonum_243[0] = sp13_message_203[0]  # type = ?
        sp13_message_206 = message(linecount=4, text='803.452881')
        sp13_flonum_243[0] = sp13_message_206[0]  # type = ?
        sp13_message_246 = message(text='setcell $2 $3 val $1')
        sp13_jitmatrix_1_float32_4_4_30[0] = sp13_message_246[0]  # type = ?
        sp13_message_44 = message(linecount=10, text='642.762329')
        sp13_flonum_243[0] = sp13_message_44[0]  # type = ?
        sp13_message_48 = message(linecount=10, text='708.656494')
        sp13_flonum_243[0] = sp13_message_48[0]  # type = ?
        sp13_message_67 = message(linecount=2, text='0.')
        sp13_flonum_243[0] = sp13_message_67[0]  # type = ?
        sp13_message_86 = message(text='dim $1 $1')
        sp13_jitmatrix_1_float32_4_4_30[0] = sp13_message_86[0]  # type = ?
        sp13_message_87 = message(linecount=5, text='321.381165')
        sp13_flonum_243[0] = sp13_message_87[0]  # type = ?
        sp13_message_88 = message(linecount=4, text='1446.21521')
        sp13_flonum_243[0] = sp13_message_88[0]  # type = ?
        sp13_message_89 = message(linecount=4, text='1285.524658')
        sp13_flonum_243[0] = sp13_message_89[0]  # type = ?
        sp13_message_90 = message(linecount=10, text='964.143494')
        sp13_flonum_243[0] = sp13_message_90[0]  # type = ?
        sp13_message_98 = message(linecount=5, text='482.071747')
        sp13_flonum_243[0] = sp13_message_98[0]  # type = ?
        sp13_number_121 = number(parameter_enable=0)
        sp13_pack_0_0_0_25[2] = sp13_number_121[0]  # type = ?
        sp13___1_36[0] = sp13_number_121[0]  # type = ?
        sp13_if_i1__0_then_bang_54[0] = sp13_number_121[0]  # type = ?
        sp13_sel_i1_80[0] = sp13_number_121[0]  # type = ?
        sp13_number_14 = number(parameter_enable=0)
        sp13_counter_0_12_124[4] = sp13_number_14[0]  # type = ?
        sp13_if_i1__i2_then_bang_141[1] = sp13_number_14[0]  # type = ?
        sp13_number_140 = number(parameter_enable=0)
        sp13_if_i1__i2_then_bang_141[0] = sp13_number_140[0]  # type = ?
        sp13_number_17 = number(parameter_enable=0)
        sp13___3_72[0] = sp13_number_17[0]  # type = ?
        sp13_number_188 = number(parameter_enable=0)
        sp13_sel_1_2_3_4_5_6_7_8_9_10_11_12_189[0] = sp13_number_188[0]  # type = ?
        sp13_if_i1__0_then_bang_29[0] = sp13_number_188[0]  # type = ?
        sp13_number_245 = number(maximum=15, minimum=0, mousefilter=1, parameter_enable=0, triscale=0.9)
        sp13_pack_0_0_0_25[1] = sp13_number_245[0]  # type = ?
        sp13_if_i1__i2_then_f3_else_f4_7[0] = sp13_number_245[0]  # type = ?
        sp13_outlet_68 = outlet(comment='', index=2)
        sp13_outlet_69 = outlet(comment='', index=1)
        sp13_pack_0_0_0_25 = newobj(text='pack 0. 0 0')
        sp13_message_246[0] = sp13_pack_0_0_0_25[0]  # type = ?
        sp13_print_left_239 = newobj(text='print left')
        sp13_print_right_28 = newobj(text='print right')
        sp13_receive_97 = receive(text='r out_R')
        sp13_button_27[0] = sp13_receive_97[0]  # type = ?
        sp13_sel_1_2_3_4_5_6_7_8_9_10_11_12_189 = newobj(text='sel 1 2 3 4 5 6 7 8 9 10 11 12')
        sp13_button_191[0] = sp13_sel_1_2_3_4_5_6_7_8_9_10_11_12_189[0]  # type = bang
        sp13_button_193[0] = sp13_sel_1_2_3_4_5_6_7_8_9_10_11_12_189[1]  # type = bang
        sp13_button_195[0] = sp13_sel_1_2_3_4_5_6_7_8_9_10_11_12_189[2]  # type = bang
        sp13_button_197[0] = sp13_sel_1_2_3_4_5_6_7_8_9_10_11_12_189[3]  # type = bang
        sp13_button_94[0] = sp13_sel_1_2_3_4_5_6_7_8_9_10_11_12_189[4]  # type = bang
        sp13_button_93[0] = sp13_sel_1_2_3_4_5_6_7_8_9_10_11_12_189[5]  # type = bang
        sp13_button_92[0] = sp13_sel_1_2_3_4_5_6_7_8_9_10_11_12_189[6]  # type = bang
        sp13_button_91[0] = sp13_sel_1_2_3_4_5_6_7_8_9_10_11_12_189[7]  # type = bang
        sp13_button_102[0] = sp13_sel_1_2_3_4_5_6_7_8_9_10_11_12_189[8]  # type = bang
        sp13_button_45[0] = sp13_sel_1_2_3_4_5_6_7_8_9_10_11_12_189[9]  # type = bang
        sp13_button_52[0] = sp13_sel_1_2_3_4_5_6_7_8_9_10_11_12_189[10]  # type = bang
        sp13_button_70[0] = sp13_sel_1_2_3_4_5_6_7_8_9_10_11_12_189[11]  # type = bang
        sp13_sel_i1_80 = newobj(text='sel $i1')
        sp13_button_27[0] = sp13_sel_i1_80[0]  # type = bang
        sp13_send_205 = send(text='s RhyCardinal')
        sp13_send_76 = send(text='s initR')
        sp13_uzi_0_53 = newobj(text='Uzi 0')
        sp13_counter_1_12_130[0] = sp13_uzi_0_53[0]  # type = bang
        return sp13_outlet_69, sp13_outlet_68

    #----------------------------------------------------------------------
    def subpatcher_14(sp14_inlet_2, sp14_inlet_1):
        sp14_message_52 = message(text='7 "61 62 63 53 54 55 59"')
        sp14_outlet_92 = outlet(comment='', index=1)
        sp14_message_46 = message(text='7 "61 62 63 53 54 55 59"')
        sp14_message_52[1] = sp14_message_46[0]  # type = ?
        sp14_outlet_92[0] = sp14_message_46[0]  # type = ?
        sp14_button_68 = button()
        sp14_message_46[0] = sp14_button_68[0]  # type = bang
        sp14_send_62 = send(text='s out_I')
        sp14_message_39 = message(text='prepend $1')
        sp14_message_46[0] = sp14_message_39[0]  # type = ?
        sp14_button_68[0] = sp14_message_39[0]  # type = ?
        sp14_int_75 = int(text='int')
        sp14_message_39[0] = sp14_int_75[0]  # type = int
        sp14_if_i1__0_then_bang_72 = newobj(text='if $i1 != 0 then bang')
        sp14_int_75[0] = sp14_if_i1__0_then_bang_72[0]  # type = ?
        sp14_tosymbol_44 = tosymbol(text='tosymbol')
        sp14_message_46[1] = sp14_tosymbol_44[0]  # type = ?
        sp14_message_8 = message(linecount=2, text='61 62 63 53 54 55 59')
        sp14_tosymbol_44[0] = sp14_message_8[0]  # type = ?
        sp14_number_20 = number(parameter_enable=0)
        sp14_if_i1__0_then_bang_72[0] = sp14_number_20[0]  # type = ?
        sp14_int_75[1] = sp14_number_20[0]  # type = ?
        sp14_message_8[0] = sp14_number_20[0]  # type = ?
        sp14_jitspill_plane_1_listlength_2_offset_0_0_5 = newobj(text='jit.spill @plane 1 @listlength 2 @offset 0 0')
        sp14_message_8[1] = sp14_jitspill_plane_1_listlength_2_offset_0_0_5[0]  # type = ?
        sp14_message_17 = message(text='offset 0 $1')
        sp14_jitspill_plane_1_listlength_2_offset_0_0_5[0] = sp14_message_17[0]  # type = ?
        sp14_counter_0_9_3 = newobj(text='counter 0 9')
        sp14_message_17[0] = sp14_counter_0_9_3[0]  # type = int
        sp14_number_20[0] = sp14_counter_0_9_3[0]  # type = int
        sp14_button_145 = button()
        sp14_counter_0_9_3[0] = sp14_button_145[0]  # type = bang
        sp14_if_i1__i2_then_bang_141 = newobj(text='if $i1 <= $i2 then bang')
        sp14_button_145[0] = sp14_if_i1__i2_then_bang_141[0]  # type = ?
        sp14_send_62[0] = sp14_if_i1__i2_then_bang_141[0]  # type = ?
        sp14_outlet_95 = outlet(comment='', index=2)
        sp14___0_93 = newobj(text='== 0')
        sp14_outlet_95[0] = sp14___0_93[0]  # type = int
        sp14_number_140 = number(parameter_enable=0)
        sp14_if_i1__i2_then_bang_141[0] = sp14_number_140[0]  # type = ?
        sp14___0_93[0] = sp14_number_140[0]  # type = ?
        sp14_accum_1_136 = newobj(text='accum 1')
        sp14_number_140[0] = sp14_accum_1_136[0]  # type = int
        sp14_message_138 = message(text='1')
        sp14_accum_1_136[1] = sp14_message_138[0]  # type = ?
        sp14_button_56 = button()
        sp14_accum_1_136[0] = sp14_button_56[0]  # type = bang
        sp14_message_138[0] = sp14_button_56[0]  # type = bang
        sp14_inlet_2 = inlet(comment='', index=2)
        sp14_jitspill_plane_1_listlength_2_offset_0_0_5[0] = sp14_inlet_2[0]  # type = jit_matrix
        sp14_button_56[0] = sp14_inlet_2[0]  # type = jit_matrix
        sp14_message_135 = message(text='set 0')
        sp14_accum_1_136[0] = sp14_message_135[0]  # type = ?
        sp14_message_27 = message(text='0')
        sp14_counter_0_9_3[2] = sp14_message_27[0]  # type = ?
        sp14_message_71 = message(text='clear')
        sp14_outlet_92[0] = sp14_message_71[0]  # type = ?
        sp14_button_12 = button()
        sp14_message_27[0] = sp14_button_12[0]  # type = bang
        sp14_message_71[0] = sp14_button_12[0]  # type = bang
        sp14_message_14 = message(text='listlength $1')
        sp14_jitspill_plane_1_listlength_2_offset_0_0_5[0] = sp14_message_14[0]  # type = ?
        sp14_number_22 = number(parameter_enable=0)
        sp14_message_14[0] = sp14_number_22[0]  # type = ?
        sp14_t_b_i_4 = newobj(text='t b i')
        sp14_counter_0_9_3[0] = sp14_t_b_i_4[0]  # type = bang
        sp14_message_135[0] = sp14_t_b_i_4[1]  # type = int
        sp14_if_i1__i2_then_bang_141[1] = sp14_t_b_i_4[1]  # type = int
        sp14_number_22[0] = sp14_t_b_i_4[1]  # type = int
        sp14_counter_0_9_3[4] = sp14_t_b_i_4[1]  # type = int
        sp14___0_93[1] = sp14_t_b_i_4[1]  # type = int
        sp14_inlet_1 = inlet(comment='', index=1)
        sp14_button_12[0] = sp14_inlet_1[0]  # type = ?
        sp14_t_b_i_4[0] = sp14_inlet_1[0]  # type = ?
        return sp14_outlet_95, sp14_outlet_92

    #----------------------------------------------------------------------
    def subpatcher_15(sp15_inlet_2, sp15_inlet_1):
        sp15_message_52 = message(text='7 "59 63 64 65 67 68 69"')
        sp15_outlet_92 = outlet(comment='', index=1)
        sp15_message_46 = message(text='7 "59 63 64 65 67 68 69"')
        sp15_message_52[1] = sp15_message_46[0]  # type = ?
        sp15_outlet_92[0] = sp15_message_46[0]  # type = ?
        sp15_button_68 = button()
        sp15_message_46[0] = sp15_button_68[0]  # type = bang
        sp15_send_62 = send(text='s out_T')
        sp15_message_39 = message(text='prepend $1')
        sp15_message_46[0] = sp15_message_39[0]  # type = ?
        sp15_button_68[0] = sp15_message_39[0]  # type = ?
        sp15_int_75 = int(text='int')
        sp15_message_39[0] = sp15_int_75[0]  # type = int
        sp15_if_i1__0_then_bang_72 = newobj(text='if $i1 != 0 then bang')
        sp15_int_75[0] = sp15_if_i1__0_then_bang_72[0]  # type = ?
        sp15_tosymbol_44 = tosymbol(text='tosymbol')
        sp15_message_46[1] = sp15_tosymbol_44[0]  # type = ?
        sp15_message_8 = message(text='59 63 64 65 67 68 69')
        sp15_tosymbol_44[0] = sp15_message_8[0]  # type = ?
        sp15_number_20 = number(parameter_enable=0)
        sp15_if_i1__0_then_bang_72[0] = sp15_number_20[0]  # type = ?
        sp15_int_75[1] = sp15_number_20[0]  # type = ?
        sp15_message_8[0] = sp15_number_20[0]  # type = ?
        sp15_jitspill_plane_1_listlength_2_offset_0_0_5 = newobj(text='jit.spill @plane 1 @listlength 2 @offset 0 0')
        sp15_message_8[1] = sp15_jitspill_plane_1_listlength_2_offset_0_0_5[0]  # type = ?
        sp15_message_17 = message(text='offset 0 $1')
        sp15_jitspill_plane_1_listlength_2_offset_0_0_5[0] = sp15_message_17[0]  # type = ?
        sp15_counter_0_9_3 = newobj(text='counter 0 9')
        sp15_message_17[0] = sp15_counter_0_9_3[0]  # type = int
        sp15_number_20[0] = sp15_counter_0_9_3[0]  # type = int
        sp15_button_145 = button()
        sp15_counter_0_9_3[0] = sp15_button_145[0]  # type = bang
        sp15_if_i1__i2_then_bang_141 = newobj(text='if $i1 <= $i2 then bang')
        sp15_button_145[0] = sp15_if_i1__i2_then_bang_141[0]  # type = ?
        sp15_send_62[0] = sp15_if_i1__i2_then_bang_141[0]  # type = ?
        sp15_outlet_95 = outlet(comment='', index=2)
        sp15___0_93 = newobj(text='== 0')
        sp15_outlet_95[0] = sp15___0_93[0]  # type = int
        sp15_number_140 = number(parameter_enable=0)
        sp15_if_i1__i2_then_bang_141[0] = sp15_number_140[0]  # type = ?
        sp15___0_93[0] = sp15_number_140[0]  # type = ?
        sp15_accum_1_136 = newobj(text='accum 1')
        sp15_number_140[0] = sp15_accum_1_136[0]  # type = int
        sp15_message_138 = message(text='1')
        sp15_accum_1_136[1] = sp15_message_138[0]  # type = ?
        sp15_button_56 = button()
        sp15_accum_1_136[0] = sp15_button_56[0]  # type = bang
        sp15_message_138[0] = sp15_button_56[0]  # type = bang
        sp15_inlet_2 = inlet(comment='', index=2)
        sp15_jitspill_plane_1_listlength_2_offset_0_0_5[0] = sp15_inlet_2[0]  # type = jit_matrix
        sp15_button_56[0] = sp15_inlet_2[0]  # type = jit_matrix
        sp15_message_135 = message(text='set 0')
        sp15_accum_1_136[0] = sp15_message_135[0]  # type = ?
        sp15_message_27 = message(text='0')
        sp15_counter_0_9_3[2] = sp15_message_27[0]  # type = ?
        sp15_message_71 = message(text='clear')
        sp15_outlet_92[0] = sp15_message_71[0]  # type = ?
        sp15_button_12 = button()
        sp15_message_27[0] = sp15_button_12[0]  # type = bang
        sp15_message_71[0] = sp15_button_12[0]  # type = bang
        sp15_message_14 = message(text='listlength $1')
        sp15_jitspill_plane_1_listlength_2_offset_0_0_5[0] = sp15_message_14[0]  # type = ?
        sp15_number_22 = number(parameter_enable=0)
        sp15_message_14[0] = sp15_number_22[0]  # type = ?
        sp15_t_b_i_4 = newobj(text='t b i')
        sp15_counter_0_9_3[0] = sp15_t_b_i_4[0]  # type = bang
        sp15_message_135[0] = sp15_t_b_i_4[1]  # type = int
        sp15_if_i1__i2_then_bang_141[1] = sp15_t_b_i_4[1]  # type = int
        sp15_number_22[0] = sp15_t_b_i_4[1]  # type = int
        sp15_counter_0_9_3[4] = sp15_t_b_i_4[1]  # type = int
        sp15___0_93[1] = sp15_t_b_i_4[1]  # type = int
        sp15_inlet_1 = inlet(comment='', index=1)
        sp15_button_12[0] = sp15_inlet_1[0]  # type = ?
        sp15_t_b_i_4[0] = sp15_inlet_1[0]  # type = ?
        return sp15_outlet_95, sp15_outlet_92

    #----------------------------------------------------------------------
    def subpatcher_16(sp16_inlet_19, sp16_inlet_11, sp16_inlet_1, sp16_inlet_3, sp16_inlet_5, sp16_inlet_8, sp16_inlet_50, sp16_inlet_51, sp16_inlet_58, sp16_inlet_60):
        sp16___0_81 = newobj(text='== 0')
        sp16_button_84[0] = sp16___0_81[0]  # type = int
        sp16___1_114 = newobj(text='- 1')
        sp16_counter_1_8_113[4] = sp16___1_114[0]  # type = int
        sp16_uzi_0_53[1] = sp16___1_114[0]  # type = int
        sp16___1_80 = newobj(text='- 1')
        sp16_if_i1__i2_then_bang_141[1] = sp16___1_80[0]  # type = int
        sp16_accum_1_136 = newobj(text='accum 1')
        sp16_number_140[0] = sp16_accum_1_136[0]  # type = int
        sp16_button_112 = button()
        sp16_counter_1_8_113[0] = sp16_button_112[0]  # type = bang
        sp16_button_12 = button()
        sp16_accum_1_136[0] = sp16_button_12[0]  # type = bang
        sp16_message_138[0] = sp16_button_12[0]  # type = bang
        sp16_button_14 = button()
        sp16_uzi_0_53[0] = sp16_button_14[0]  # type = bang
        sp16_button_145 = button()
        sp16_button_52[0] = sp16_button_145[0]  # type = bang
        sp16_button_191 = button()
        sp16_message_199[0] = sp16_button_191[0]  # type = bang
        sp16_button_193 = button()
        sp16_message_201[0] = sp16_button_193[0]  # type = bang
        sp16_button_195 = button()
        sp16_message_203[0] = sp16_button_195[0]  # type = bang
        sp16_button_197 = button()
        sp16_message_206[0] = sp16_button_197[0]  # type = bang
        sp16_button_24 = button()
        sp16_message_135[0] = sp16_button_24[0]  # type = bang
        sp16_message_147[0] = sp16_button_24[0]  # type = bang
        sp16_message_16[0] = sp16_button_24[0]  # type = bang
        sp16_message_78[0] = sp16_button_24[0]  # type = bang
        sp16_button_30 = button()
        sp16_message_18[0] = sp16_button_30[0]  # type = bang
        sp16_button_31 = button()
        sp16_message_25[0] = sp16_button_31[0]  # type = bang
        sp16_button_32 = button()
        sp16_message_27[0] = sp16_button_32[0]  # type = bang
        sp16_button_33 = button()
        sp16_message_28[0] = sp16_button_33[0]  # type = bang
        sp16_button_38 = button()
        sp16_message_34[0] = sp16_button_38[0]  # type = bang
        sp16_button_39 = button()
        sp16_message_35[0] = sp16_button_39[0]  # type = bang
        sp16_button_40 = button()
        sp16_message_36[0] = sp16_button_40[0]  # type = bang
        sp16_button_41 = button()
        sp16_message_37[0] = sp16_button_41[0]  # type = bang
        sp16_button_52 = button()
        sp16_counter_1_9_130[0] = sp16_button_52[0]  # type = bang
        sp16_button_84 = button()
        sp16_message_63[0] = sp16_button_84[0]  # type = bang
        sp16_button_91 = button()
        sp16_message_87[0] = sp16_button_91[0]  # type = bang
        sp16_button_92 = button()
        sp16_message_88[0] = sp16_button_92[0]  # type = bang
        sp16_button_93 = button()
        sp16_message_89[0] = sp16_button_93[0]  # type = bang
        sp16_button_94 = button()
        sp16_message_90[0] = sp16_button_94[0]  # type = bang
        sp16_button_96 = button()
# (comment) a.
# (comment) b.
# (comment) c.
# (comment) d.
# (comment) e.
# (comment) f.
# (comment) output
        sp16_counter_1_8_113 = newobj(text='counter 1 8')
        sp16_number_48[0] = sp16_counter_1_8_113[0]  # type = int
        sp16_if_i1__0_then_bang_54[0] = sp16_counter_1_8_113[2]  # type = ?
        sp16_counter_1_9_130 = newobj(text='counter 1 9')
        sp16_number_188[0] = sp16_counter_1_9_130[0]  # type = int
        sp16_delay_5_69 = newobj(text='delay 5')
        sp16_button_12[0] = sp16_delay_5_69[0]  # type = bang
        sp16_expr_i1__f2_57 = newobj(text='expr ($i1 * $f2)')
        sp16_message_61[0] = sp16_expr_i1__f2_57[0]  # type = ?
        sp16_expr_i1__i2_43 = newobj(text='expr ($i1 - $i2)')
        sp16_if_i1__0_then_i1_45[0] = sp16_expr_i1__i2_43[0]  # type = ?
        sp16_flonum_10 = flonum(format=6, parameter_enable=0)
        sp16_expr_i1__f2_57[1] = sp16_flonum_10[0]  # type = ?
        sp16_if_i1__0_then_bang_29 = newobj(text='if $i1 != 0 then bang')
        sp16_button_14[0] = sp16_if_i1__0_then_bang_29[0]  # type = ?
        sp16_if_i1__0_then_bang_54 = newobj(text='if $i1 != 0 then bang')
        sp16_delay_5_69[0] = sp16_if_i1__0_then_bang_54[0]  # type = ?
        sp16_if_i1__0_then_i1_45 = newobj(text='if $i1 > 0 then $i1')
        sp16_expr_i1__f2_57[0] = sp16_if_i1__0_then_i1_45[0]  # type = ?
        sp16_message_70[0] = sp16_if_i1__0_then_i1_45[0]  # type = ?
        sp16_if_i1__i2_then_bang_141 = newobj(text='if $i1 < $i2 then bang')
        sp16_button_145[0] = sp16_if_i1__i2_then_bang_141[0]  # type = ?
        sp16_inlet_1 = inlet(comment='', index=5)
        sp16_int_15[1] = sp16_inlet_1[0]  # type = ?
        sp16_inlet_11 = inlet(comment='', index=9)
        sp16_int_77[1] = sp16_inlet_11[0]  # type = ?
        sp16_inlet_19 = inlet(comment='', index=10)
        sp16_t_b_f_95[0] = sp16_inlet_19[0]  # type = ?
        sp16_inlet_3 = inlet(comment='', index=6)
        sp16_int_79[1] = sp16_inlet_3[0]  # type = ?
        sp16_inlet_5 = inlet(comment='', index=7)
        sp16_int_72[1] = sp16_inlet_5[0]  # type = ?
        sp16_inlet_50 = inlet(comment='', index=1)
        sp16_int_49[1] = sp16_inlet_50[0]  # type = ?
        sp16_inlet_51 = inlet(comment='', index=2)
        sp16_int_47[1] = sp16_inlet_51[0]  # type = ?
        sp16_inlet_58 = inlet(comment='', index=3)
        sp16_int_83[1] = sp16_inlet_58[0]  # type = ?
        sp16_inlet_60 = inlet(comment='', index=4)
        sp16_int_44[1] = sp16_inlet_60[0]  # type = ?
        sp16_inlet_8 = inlet(comment='', index=8)
        sp16_int_13[1] = sp16_inlet_8[0]  # type = ?
        sp16_int_13 = int(text='int')
        sp16_number_20[0] = sp16_int_13[0]  # type = int
        sp16_int_15 = int(text='int')
        sp16_number_23[0] = sp16_int_15[0]  # type = int
        sp16_int_44 = int(text='int')
        sp16_number_9[0] = sp16_int_44[0]  # type = int
        sp16_int_47 = int(text='int')
        sp16_number_4[0] = sp16_int_47[0]  # type = int
        sp16_int_49 = int(text='int')
        sp16_number_2[0] = sp16_int_49[0]  # type = int
        sp16_int_72 = int(text='int')
        sp16_number_21[0] = sp16_int_72[0]  # type = int
        sp16_int_77 = int(text='int')
        sp16_number_26[0] = sp16_int_77[0]  # type = int
        sp16_int_79 = int(text='int')
        sp16_number_22[0] = sp16_int_79[0]  # type = int
        sp16_int_83 = int(text='int')
        sp16_number_6[0] = sp16_int_83[0]  # type = int
        sp16_int_97 = int(text='int')
        sp16_number_17[0] = sp16_int_97[0]  # type = int
        sp16_message_135 = message(text='set 0')
        sp16_accum_1_136[0] = sp16_message_135[0]  # type = ?
        sp16_message_138 = message(text='1')
        sp16_accum_1_136[1] = sp16_message_138[0]  # type = ?
        sp16_message_147 = message(text='0')
        sp16_counter_1_8_113[3] = sp16_message_147[0]  # type = ?
        sp16_counter_1_9_130[3] = sp16_message_147[0]  # type = ?
        sp16_message_16 = message(text='1')
        sp16_counter_1_9_130[2] = sp16_message_16[0]  # type = ?
        sp16_message_18 = message(text='0')
        sp16_expr_i1__i2_43[0] = sp16_message_18[0]  # type = ?
        sp16_message_199 = message(text='0')
        sp16_expr_i1__i2_43[1] = sp16_message_199[0]  # type = ?
        sp16_message_201 = message(text='1')
        sp16_expr_i1__i2_43[1] = sp16_message_201[0]  # type = ?
        sp16_message_203 = message(text='3')
        sp16_expr_i1__i2_43[1] = sp16_message_203[0]  # type = ?
        sp16_message_206 = message(text='4')
        sp16_expr_i1__i2_43[1] = sp16_message_206[0]  # type = ?
        sp16_message_25 = message(text='9')
        sp16_expr_i1__i2_43[0] = sp16_message_25[0]  # type = ?
        sp16_message_27 = message(text='8')
        sp16_expr_i1__i2_43[0] = sp16_message_27[0]  # type = ?
        sp16_message_28 = message(text='6')
        sp16_expr_i1__i2_43[0] = sp16_message_28[0]  # type = ?
        sp16_message_34 = message(text='5')
        sp16_expr_i1__i2_43[0] = sp16_message_34[0]  # type = ?
        sp16_message_35 = message(text='4')
        sp16_expr_i1__i2_43[0] = sp16_message_35[0]  # type = ?
        sp16_message_36 = message(text='3')
        sp16_expr_i1__i2_43[0] = sp16_message_36[0]  # type = ?
        sp16_message_37 = message(text='1')
        sp16_expr_i1__i2_43[0] = sp16_message_37[0]  # type = ?
        sp16_message_61 = message(text='append $1')
        sp16_message_63[0] = sp16_message_61[0]  # type = ?
        sp16_message_63 = message(linecount=3, text='/rhythm 160.690582 482.071747 642.762329 803.452881 964.143494 1285.524658 1446.21521 321.381165 482.071747 642.762329 803.452881 1124.834106 1285.524658 160.690582 321.381165 482.071747 803.452881 964.143494 160.690582 321.381165 642.762329 803.452881 160.690582 482.071747 642.762329 321.381165 482.071747')
        sp16_outlet_85[0] = sp16_message_63[0]  # type = ?
        sp16_message_70 = message(text='append $1')
        sp16_message_76[0] = sp16_message_70[0]  # type = ?
        sp16_message_76 = message(linecount=2, text='/rhythm 1 3 4 5 6 8 9 2 3 4 5 7 8 1 2 3 5 6 1 2 4 5 1 3 4 2 3')
        sp16_message_78 = message(text='/rhythm')
        sp16_message_63[1] = sp16_message_78[0]  # type = ?
        sp16_message_76[1] = sp16_message_78[0]  # type = ?
        sp16_message_87 = message(text='9')
        sp16_expr_i1__i2_43[1] = sp16_message_87[0]  # type = ?
        sp16_message_88 = message(text='8')
        sp16_expr_i1__i2_43[1] = sp16_message_88[0]  # type = ?
        sp16_message_89 = message(text='6')
        sp16_expr_i1__i2_43[1] = sp16_message_89[0]  # type = ?
        sp16_message_90 = message(text='5')
        sp16_expr_i1__i2_43[1] = sp16_message_90[0]  # type = ?
        sp16_number_140 = number(parameter_enable=0)
        sp16_if_i1__i2_then_bang_141[0] = sp16_number_140[0]  # type = ?
        sp16___0_81[0] = sp16_number_140[0]  # type = ?
        sp16_number_17 = number(parameter_enable=0)
        sp16___1_114[0] = sp16_number_17[0]  # type = ?
        sp16_counter_1_9_130[4] = sp16_number_17[0]  # type = ?
        sp16_button_24[0] = sp16_number_17[0]  # type = ?
        sp16_number_82[0] = sp16_number_17[0]  # type = ?
        sp16_number_188 = number(parameter_enable=0)
        sp16_sel_1_2_3_4_5_6_7_8_189[0] = sp16_number_188[0]  # type = ?
        sp16_if_i1__0_then_bang_29[0] = sp16_number_188[0]  # type = ?
        sp16_number_2 = number(parameter_enable=0)
        sp16_message_199[1] = sp16_number_2[0]  # type = ?
        sp16_button_52[0] = sp16_number_2[0]  # type = ?
        sp16_number_20 = number(parameter_enable=0)
        sp16_message_25[1] = sp16_number_20[0]  # type = ?
        sp16_message_87[1] = sp16_number_20[0]  # type = ?
        sp16_number_21 = number(parameter_enable=0)
        sp16_message_27[1] = sp16_number_21[0]  # type = ?
        sp16_message_88[1] = sp16_number_21[0]  # type = ?
        sp16_number_22 = number(parameter_enable=0)
        sp16_message_28[1] = sp16_number_22[0]  # type = ?
        sp16_message_89[1] = sp16_number_22[0]  # type = ?
        sp16_number_23 = number(parameter_enable=0)
        sp16_message_34[1] = sp16_number_23[0]  # type = ?
        sp16_message_90[1] = sp16_number_23[0]  # type = ?
        sp16_number_26 = number(parameter_enable=0)
        sp16_message_18[1] = sp16_number_26[0]  # type = ?
        sp16_number_4 = number(parameter_enable=0)
        sp16_message_201[1] = sp16_number_4[0]  # type = ?
        sp16_message_37[1] = sp16_number_4[0]  # type = ?
        sp16_number_48 = number(parameter_enable=0)
        sp16_sel_1_2_3_4_5_6_7_8_42[0] = sp16_number_48[0]  # type = ?
        sp16_number_6 = number(parameter_enable=0)
        sp16_message_203[1] = sp16_number_6[0]  # type = ?
        sp16_message_36[1] = sp16_number_6[0]  # type = ?
        sp16_number_82 = number(parameter_enable=0)
        sp16___1_80[0] = sp16_number_82[0]  # type = ?
        sp16___0_81[1] = sp16_number_82[0]  # type = ?
        sp16_number_9 = number(parameter_enable=0)
        sp16_message_206[1] = sp16_number_9[0]  # type = ?
        sp16_message_35[1] = sp16_number_9[0]  # type = ?
        sp16_outlet_85 = outlet(comment='', index=1)
        sp16_receive_142 = receive(text='r cardinal')
        sp16_int_97[1] = sp16_receive_142[0]  # type = ?
        sp16_sel_1_2_3_4_5_6_7_8_189 = newobj(text='sel 1 2 3 4 5 6 7 8')
        sp16_button_191[0] = sp16_sel_1_2_3_4_5_6_7_8_189[0]  # type = bang
        sp16_button_193[0] = sp16_sel_1_2_3_4_5_6_7_8_189[1]  # type = bang
        sp16_button_195[0] = sp16_sel_1_2_3_4_5_6_7_8_189[2]  # type = bang
        sp16_button_197[0] = sp16_sel_1_2_3_4_5_6_7_8_189[3]  # type = bang
        sp16_button_94[0] = sp16_sel_1_2_3_4_5_6_7_8_189[4]  # type = bang
        sp16_button_93[0] = sp16_sel_1_2_3_4_5_6_7_8_189[5]  # type = bang
        sp16_button_92[0] = sp16_sel_1_2_3_4_5_6_7_8_189[6]  # type = bang
        sp16_button_91[0] = sp16_sel_1_2_3_4_5_6_7_8_189[7]  # type = bang
        sp16_sel_1_2_3_4_5_6_7_8_42 = newobj(text='sel 1 2 3 4 5 6 7 8')
        sp16_button_41[0] = sp16_sel_1_2_3_4_5_6_7_8_42[0]  # type = bang
        sp16_button_40[0] = sp16_sel_1_2_3_4_5_6_7_8_42[1]  # type = bang
        sp16_button_39[0] = sp16_sel_1_2_3_4_5_6_7_8_42[2]  # type = bang
        sp16_button_38[0] = sp16_sel_1_2_3_4_5_6_7_8_42[3]  # type = bang
        sp16_button_33[0] = sp16_sel_1_2_3_4_5_6_7_8_42[4]  # type = bang
        sp16_button_32[0] = sp16_sel_1_2_3_4_5_6_7_8_42[5]  # type = bang
        sp16_button_31[0] = sp16_sel_1_2_3_4_5_6_7_8_42[6]  # type = bang
        sp16_button_30[0] = sp16_sel_1_2_3_4_5_6_7_8_42[7]  # type = bang
        sp16_t_b_f_95 = newobj(text='t b f')
        sp16_int_13[0] = sp16_t_b_f_95[0]  # type = bang
        sp16_int_15[0] = sp16_t_b_f_95[0]  # type = bang
        sp16_int_44[0] = sp16_t_b_f_95[0]  # type = bang
        sp16_int_47[0] = sp16_t_b_f_95[0]  # type = bang
        sp16_int_49[0] = sp16_t_b_f_95[0]  # type = bang
        sp16_int_72[0] = sp16_t_b_f_95[0]  # type = bang
        sp16_int_77[0] = sp16_t_b_f_95[0]  # type = bang
        sp16_int_79[0] = sp16_t_b_f_95[0]  # type = bang
        sp16_int_83[0] = sp16_t_b_f_95[0]  # type = bang
        sp16_button_96[0] = sp16_t_b_f_95[0]  # type = bang
        sp16_int_97[0] = sp16_t_b_f_95[0]  # type = bang
        sp16_flonum_10[0] = sp16_t_b_f_95[1]  # type = float
        sp16_uzi_0_53 = newobj(text='Uzi 0')
        sp16_button_112[0] = sp16_uzi_0_53[0]  # type = bang
        return sp16_outlet_85

    #----------------------------------------------------------------------
    def subpatcher_17(sp17_inlet_2, sp17_inlet_1):
        sp17_outlet_92 = outlet(comment='', index=1)
        sp17_message_46 = message(text='7 "44 48 49 50 52 53 54"')
        sp17_outlet_92[0] = sp17_message_46[0]  # type = ?
        sp17_button_68 = button()
        sp17_message_46[0] = sp17_button_68[0]  # type = bang
        sp17_send_62 = send(text='s out')
        sp17_message_39 = message(text='prepend $1')
        sp17_message_46[0] = sp17_message_39[0]  # type = ?
        sp17_button_68[0] = sp17_message_39[0]  # type = ?
        sp17_int_75 = int(text='int')
        sp17_message_39[0] = sp17_int_75[0]  # type = int
        sp17_if_i1__0_then_bang_72 = newobj(text='if $i1 != 0 then bang')
        sp17_int_75[0] = sp17_if_i1__0_then_bang_72[0]  # type = ?
        sp17_tosymbol_44 = tosymbol(text='tosymbol')
        sp17_message_46[1] = sp17_tosymbol_44[0]  # type = ?
        sp17_message_8 = message(text='44 48 49 50 52 53 54')
        sp17_tosymbol_44[0] = sp17_message_8[0]  # type = ?
        sp17_number_20 = number(parameter_enable=0)
        sp17_if_i1__0_then_bang_72[0] = sp17_number_20[0]  # type = ?
        sp17_int_75[1] = sp17_number_20[0]  # type = ?
        sp17_message_8[0] = sp17_number_20[0]  # type = ?
        sp17_jitspill_plane_1_listlength_2_offset_0_0_5 = newobj(text='jit.spill @plane 1 @listlength 2 @offset 0 0')
        sp17_message_8[1] = sp17_jitspill_plane_1_listlength_2_offset_0_0_5[0]  # type = ?
        sp17_message_17 = message(text='offset 0 $1')
        sp17_jitspill_plane_1_listlength_2_offset_0_0_5[0] = sp17_message_17[0]  # type = ?
        sp17_counter_0_9_3 = newobj(text='counter 0 9')
        sp17_message_17[0] = sp17_counter_0_9_3[0]  # type = int
        sp17_number_20[0] = sp17_counter_0_9_3[0]  # type = int
        sp17_button_145 = button()
        sp17_counter_0_9_3[0] = sp17_button_145[0]  # type = bang
        sp17_if_i1__i2_then_bang_141 = newobj(text='if $i1 <= $i2 then bang')
        sp17_button_145[0] = sp17_if_i1__i2_then_bang_141[0]  # type = ?
        sp17_send_62[0] = sp17_if_i1__i2_then_bang_141[0]  # type = ?
        sp17_outlet_95 = outlet(comment='', index=2)
        sp17___0_93 = newobj(text='== 0')
        sp17_outlet_95[0] = sp17___0_93[0]  # type = int
        sp17_number_140 = number(parameter_enable=0)
        sp17_if_i1__i2_then_bang_141[0] = sp17_number_140[0]  # type = ?
        sp17___0_93[0] = sp17_number_140[0]  # type = ?
        sp17_accum_1_136 = newobj(text='accum 1')
        sp17_number_140[0] = sp17_accum_1_136[0]  # type = int
        sp17_message_138 = message(text='1')
        sp17_accum_1_136[1] = sp17_message_138[0]  # type = ?
        sp17_button_56 = button()
        sp17_accum_1_136[0] = sp17_button_56[0]  # type = bang
        sp17_message_138[0] = sp17_button_56[0]  # type = bang
        sp17_inlet_2 = inlet(comment='', index=2)
        sp17_jitspill_plane_1_listlength_2_offset_0_0_5[0] = sp17_inlet_2[0]  # type = jit_matrix
        sp17_button_56[0] = sp17_inlet_2[0]  # type = jit_matrix
        sp17_message_135 = message(text='set 0')
        sp17_accum_1_136[0] = sp17_message_135[0]  # type = ?
        sp17_message_27 = message(text='0')
        sp17_counter_0_9_3[2] = sp17_message_27[0]  # type = ?
        sp17_message_71 = message(text='clear')
        sp17_outlet_92[0] = sp17_message_71[0]  # type = ?
        sp17_button_12 = button()
        sp17_message_27[0] = sp17_button_12[0]  # type = bang
        sp17_message_71[0] = sp17_button_12[0]  # type = bang
        sp17_message_14 = message(text='listlength $1')
        sp17_jitspill_plane_1_listlength_2_offset_0_0_5[0] = sp17_message_14[0]  # type = ?
        sp17_number_22 = number(parameter_enable=0)
        sp17_message_14[0] = sp17_number_22[0]  # type = ?
        sp17_t_b_i_4 = newobj(text='t b i')
        sp17_counter_0_9_3[0] = sp17_t_b_i_4[0]  # type = bang
        sp17_message_135[0] = sp17_t_b_i_4[1]  # type = int
        sp17_if_i1__i2_then_bang_141[1] = sp17_t_b_i_4[1]  # type = int
        sp17_number_22[0] = sp17_t_b_i_4[1]  # type = int
        sp17_counter_0_9_3[4] = sp17_t_b_i_4[1]  # type = int
        sp17___0_93[1] = sp17_t_b_i_4[1]  # type = int
        sp17_inlet_1 = inlet(comment='', index=1)
        sp17_button_12[0] = sp17_inlet_1[0]  # type = ?
        sp17_t_b_i_4[0] = sp17_inlet_1[0]  # type = ?
# (comment) c.
# (comment) b.
# (comment) d.
# (comment) e.
# (comment) a.
        return sp17_outlet_95, sp17_outlet_92

    #----------------------------------------------------------------------
    def subpatcher_18(sp18_inlet_19, sp18_inlet_11, sp18_inlet_1, sp18_inlet_3, sp18_inlet_5, sp18_inlet_8, sp18_inlet_50, sp18_inlet_51, sp18_inlet_58, sp18_inlet_60):
        sp18___0_100 = newobj(text='+ 0')
        sp18_number_37[0] = sp18___0_100[0]  # type = int
        sp18___0_15 = newobj(text='+ 0')
        sp18_number_200[0] = sp18___0_15[0]  # type = int
        sp18___0_32 = newobj(text='+ 0')
        sp18_number_202[0] = sp18___0_32[0]  # type = int
        sp18___0_34 = newobj(text='+ 0')
        sp18_number_204[0] = sp18___0_34[0]  # type = int
        sp18___0_35 = newobj(text='+ 0')
        sp18_number_205[0] = sp18___0_35[0]  # type = int
        sp18___0_38 = newobj(text='+ 0')
        sp18_p_limiteri_103[8] = sp18___0_38[0]  # type = int
        sp18___0_40 = newobj(text='+ 0')
        sp18_p_limiteri_103[7] = sp18___0_40[0]  # type = int
        sp18___0_41 = newobj(text='+ 0')
        sp18_p_limiteri_103[6] = sp18___0_41[0]  # type = int
        sp18___0_42 = newobj(text='+ 0')
        sp18_p_limiteri_103[5] = sp18___0_42[0]  # type = int
        sp18___0_43 = newobj(text='+ 0')
        sp18_p_limiteri_103[4] = sp18___0_43[0]  # type = int
        sp18___0_44 = newobj(text='+ 0')
        sp18_p_limiteri_103[3] = sp18___0_44[0]  # type = int
        sp18___0_45 = newobj(text='+ 0')
        sp18_p_limiteri_103[2] = sp18___0_45[0]  # type = int
        sp18___0_47 = newobj(text='+ 0')
        sp18_p_limiteri_103[1] = sp18___0_47[0]  # type = int
        sp18___0_48 = newobj(text='+ 0')
        sp18_p_limiteri_103[0] = sp18___0_48[0]  # type = int
        sp18___0_52 = newobj(text='+ 0')
        sp18_number_207[0] = sp18___0_52[0]  # type = int
        sp18___0_55 = newobj(text='+ 0')
        sp18_number_208[0] = sp18___0_55[0]  # type = int
        sp18___0_56 = newobj(text='+ 0')
        sp18_number_209[0] = sp18___0_56[0]  # type = int
        sp18___0_67 = newobj(text='+ 0')
        sp18_number_210[0] = sp18___0_67[0]  # type = int
        sp18___0_68 = newobj(text='+ 0')
        sp18_number_211[0] = sp18___0_68[0]  # type = int
        sp18___0_76 = newobj(text='+ 0')
        sp18___1_114 = newobj(text='- 1')
        sp18_counter_0_8_113[4] = sp18___1_114[0]  # type = int
        sp18___1_150 = newobj(text='+ 1')
        sp18_counter_1_9_130[2] = sp18___1_150[0]  # type = int
        sp18___1_73 = newobj(text='- 1')
        sp18_number_169[0] = sp18___1_73[0]  # type = int
        sp18_accum_1_136 = newobj(text='accum 1')
        sp18_number_140[0] = sp18_accum_1_136[0]  # type = int
        sp18_button_10 = button()
        sp18_uzi_0_53[0] = sp18_button_10[0]  # type = bang
        sp18_button_102 = button()
        sp18_message_98[0] = sp18_button_102[0]  # type = bang
        sp18_button_110 = button()
        sp18_counter_0_10_124[0] = sp18_button_110[0]  # type = bang
        sp18_counter_0_10_149[0] = sp18_button_110[0]  # type = bang
        sp18_button_112 = button()
        sp18_counter_0_8_113[0] = sp18_button_112[0]  # type = bang
        sp18_button_12 = button()
        sp18_accum_1_136[0] = sp18_button_12[0]  # type = bang
        sp18_message_138[0] = sp18_button_12[0]  # type = bang
        sp18_button_145 = button()
        sp18_button_10[0] = sp18_button_145[0]  # type = bang
        sp18_button_155 = button()
        sp18_message_153[0] = sp18_button_155[0]  # type = bang
        sp18_button_167 = button()
        sp18_toggle_157[0] = sp18_button_167[0]  # type = bang
        sp18_message_166[0] = sp18_button_167[0]  # type = bang
        sp18_button_171 = button()
        sp18_toggle_156[0] = sp18_button_171[0]  # type = bang
        sp18_message_170[0] = sp18_button_171[0]  # type = bang
        sp18_button_176 = button()
        sp18_toggle_158[0] = sp18_button_176[0]  # type = bang
        sp18_message_172[0] = sp18_button_176[0]  # type = bang
        sp18_button_177 = button()
        sp18_toggle_160[0] = sp18_button_177[0]  # type = bang
        sp18_message_173[0] = sp18_button_177[0]  # type = bang
        sp18_button_178 = button()
        sp18_toggle_159[0] = sp18_button_178[0]  # type = bang
        sp18_message_174[0] = sp18_button_178[0]  # type = bang
        sp18_button_179 = button()
        sp18_toggle_161[0] = sp18_button_179[0]  # type = bang
        sp18_message_175[0] = sp18_button_179[0]  # type = bang
        sp18_button_184 = button()
        sp18_toggle_162[0] = sp18_button_184[0]  # type = bang
        sp18_message_180[0] = sp18_button_184[0]  # type = bang
        sp18_button_185 = button()
        sp18_toggle_163[0] = sp18_button_185[0]  # type = bang
        sp18_message_181[0] = sp18_button_185[0]  # type = bang
        sp18_button_186 = button()
        sp18_toggle_164[0] = sp18_button_186[0]  # type = bang
        sp18_message_182[0] = sp18_button_186[0]  # type = bang
        sp18_button_187 = button()
        sp18_toggle_165[0] = sp18_button_187[0]  # type = bang
        sp18_message_183[0] = sp18_button_187[0]  # type = bang
        sp18_button_191 = button()
        sp18_message_199[0] = sp18_button_191[0]  # type = bang
        sp18_button_193 = button()
        sp18_message_201[0] = sp18_button_193[0]  # type = bang
        sp18_button_195 = button()
        sp18_message_203[0] = sp18_button_195[0]  # type = bang
        sp18_button_197 = button()
        sp18_message_206[0] = sp18_button_197[0]  # type = bang
        sp18_button_24 = button()
        sp18_message_135[0] = sp18_button_24[0]  # type = bang
        sp18_message_147[0] = sp18_button_24[0]  # type = bang
        sp18_message_16[0] = sp18_button_24[0]  # type = bang
        sp18_message_18[0] = sp18_button_24[0]  # type = bang
        sp18_button_27 = button()
        sp18_jitmatrix_1_char_4_4_30[0] = sp18_button_27[0]  # type = bang
        sp18_button_91 = button()
        sp18_message_87[0] = sp18_button_91[0]  # type = bang
        sp18_button_92 = button()
        sp18_message_88[0] = sp18_button_92[0]  # type = bang
        sp18_button_93 = button()
        sp18_message_89[0] = sp18_button_93[0]  # type = bang
        sp18_button_94 = button()
        sp18_message_90[0] = sp18_button_94[0]  # type = bang
        sp18_button_96 = button()
# (comment) a.
# (comment) b.
# (comment) c.
# (comment) d.
# (comment) e
# (comment) f.
# (comment) Sums 12 when ivversion start from 0 to n-1
# (comment) Set/Get contents
# (comment) value
# (comment) y position
# (comment) Create a matrix
# (comment) x position
# (comment) Permutación Circular Matriz Inversion-Transposicion
# (comment) output
        sp18_counter_0_10_124 = newobj(text='counter 0 10')
        sp18_number_121[0] = sp18_counter_0_10_124[0]  # type = int
        sp18_counter_0_10_149 = newobj(text='counter 0 10')
        sp18_number_151[0] = sp18_counter_0_10_149[0]  # type = int
        sp18_counter_0_8_113 = newobj(text='counter 0 8')
        sp18_number_245[0] = sp18_counter_0_8_113[0]  # type = int
        sp18_delay_10_39[0] = sp18_counter_0_8_113[3]  # type = int
        sp18_counter_1_9_130 = newobj(text='counter 1 9')
        sp18_number_188[0] = sp18_counter_1_9_130[0]  # type = int
        sp18_delay_10_39 = newobj(text='delay 10')
        sp18_button_110[0] = sp18_delay_10_39[0]  # type = bang
        sp18_expr_12i1_49 = newobj(text='expr (12-$i1)')
        sp18___0_38[0] = sp18_expr_12i1_49[0]  # type = ?
        sp18_expr_12i1_57 = newobj(text='expr (12-$i1)')
        sp18___0_40[0] = sp18_expr_12i1_57[0]  # type = ?
        sp18_expr_12i1_59 = newobj(text='expr (12-$i1)')
        sp18___0_41[0] = sp18_expr_12i1_59[0]  # type = ?
        sp18_expr_12i1_61 = newobj(text='expr (12-$i1)')
        sp18___0_42[0] = sp18_expr_12i1_61[0]  # type = ?
        sp18_expr_12i1_62 = newobj(text='expr (12-$i1)')
        sp18___0_43[0] = sp18_expr_12i1_62[0]  # type = ?
        sp18_expr_12i1_63 = newobj(text='expr (12-$i1)')
        sp18___0_44[0] = sp18_expr_12i1_63[0]  # type = ?
        sp18_expr_12i1_64 = newobj(text='expr (12-$i1)')
        sp18___0_45[0] = sp18_expr_12i1_64[0]  # type = ?
        sp18_expr_12i1_65 = newobj(text='expr (12-$i1)')
        sp18___0_47[0] = sp18_expr_12i1_65[0]  # type = ?
        sp18_expr_12i1_66 = newobj(text='expr (12-$i1)')
        sp18___0_48[0] = sp18_expr_12i1_66[0]  # type = ?
        sp18_if_i1__0_then_bang_29 = newobj(text='if $i1 != 0 then bang')
        sp18_button_112[0] = sp18_if_i1__0_then_bang_29[0]  # type = ?
        sp18_if_i1__0_then_bang_54 = newobj(text='if $i1 != 0 then bang')
        sp18_button_12[0] = sp18_if_i1__0_then_bang_54[0]  # type = ?
        sp18_if_i1__i2_then_bang_141 = newobj(text='if $i1 <= $i2 then bang')
        sp18_button_145[0] = sp18_if_i1__i2_then_bang_141[0]  # type = ?
        sp18_inlet_1 = inlet(comment='', index=5)
        sp18_int_81[1] = sp18_inlet_1[0]  # type = ?
        sp18_inlet_11 = inlet(comment='', index=9)
        sp18_int_77[1] = sp18_inlet_11[0]  # type = ?
        sp18_inlet_19 = inlet(comment='', index=10)
        sp18_t_b_i_95[0] = sp18_inlet_19[0]  # type = ?
        sp18_inlet_3 = inlet(comment='', index=6)
        sp18_int_79[1] = sp18_inlet_3[0]  # type = ?
        sp18_inlet_5 = inlet(comment='', index=7)
        sp18_int_72[1] = sp18_inlet_5[0]  # type = ?
        sp18_inlet_50 = inlet(comment='', index=1)
        sp18_int_85[1] = sp18_inlet_50[0]  # type = ?
        sp18_inlet_51 = inlet(comment='', index=2)
        sp18_int_84[1] = sp18_inlet_51[0]  # type = ?
        sp18_inlet_58 = inlet(comment='', index=3)
        sp18_int_83[1] = sp18_inlet_58[0]  # type = ?
        sp18_inlet_60 = inlet(comment='', index=4)
        sp18_int_82[1] = sp18_inlet_60[0]  # type = ?
        sp18_inlet_8 = inlet(comment='', index=8)
        sp18_int_78[1] = sp18_inlet_8[0]  # type = ?
        sp18_int_72 = int(text='int')
        sp18_number_21[0] = sp18_int_72[0]  # type = int
        sp18_int_77 = int(text='int')
        sp18_number_26[0] = sp18_int_77[0]  # type = int
        sp18_int_78 = int(text='int')
        sp18_number_20[0] = sp18_int_78[0]  # type = int
        sp18_int_79 = int(text='int')
        sp18_number_22[0] = sp18_int_79[0]  # type = int
        sp18_int_81 = int(text='int')
        sp18_number_23[0] = sp18_int_81[0]  # type = int
        sp18_int_82 = int(text='int')
        sp18_number_9[0] = sp18_int_82[0]  # type = int
        sp18_int_83 = int(text='int')
        sp18_number_6[0] = sp18_int_83[0]  # type = int
        sp18_int_84 = int(text='int')
        sp18_number_4[0] = sp18_int_84[0]  # type = int
        sp18_int_85 = int(text='int')
        sp18_number_2[0] = sp18_int_85[0]  # type = int
        sp18_int_97 = int(text='int')
        sp18_number_17[0] = sp18_int_97[0]  # type = int
        sp18_jit.cellblock_33 = jit.cellblock(cols=7, colwidth=25, rows=7)
        sp18_jitmatrix_1_char_4_4_30 = newobj(text='jit.matrix 1 char 4 4')
        sp18_jit.cellblock_33[0] = sp18_jitmatrix_1_char_4_4_30[0]  # type = jit_matrix
        sp18_outlet_71[0] = sp18_jitmatrix_1_char_4_4_30[0]  # type = jit_matrix
        sp18_jitprint_240 = newobj(text='jit.print')
        sp18_message_135 = message(text='set 0')
        sp18_accum_1_136[0] = sp18_message_135[0]  # type = ?
        sp18_message_138 = message(text='1')
        sp18_accum_1_136[1] = sp18_message_138[0]  # type = ?
        sp18_message_147 = message(text='0')
        sp18_counter_0_8_113[3] = sp18_message_147[0]  # type = ?
        sp18_counter_0_10_124[3] = sp18_message_147[0]  # type = ?
        sp18_counter_1_9_130[3] = sp18_message_147[0]  # type = ?
        sp18_button_155[0] = sp18_message_147[0]  # type = ?
        sp18_send_192[0] = sp18_message_147[0]  # type = ?
        sp18_message_153 = message(text='7')
        sp18_counter_0_10_149[3] = sp18_message_153[0]  # type = ?
        sp18_message_16 = message(text='1')
        sp18_counter_1_9_130[1] = sp18_message_16[0]  # type = ?
        sp18_counter_0_10_149[1] = sp18_message_16[0]  # type = ?
        sp18_message_166 = message(text='12')
        sp18_message_170 = message(text='12')
        sp18___0_15[1] = sp18_message_170[0]  # type = ?
        sp18___0_32[1] = sp18_message_170[0]  # type = ?
        sp18___0_34[1] = sp18_message_170[0]  # type = ?
        sp18___0_35[1] = sp18_message_170[0]  # type = ?
        sp18___0_52[1] = sp18_message_170[0]  # type = ?
        sp18___0_55[1] = sp18_message_170[0]  # type = ?
        sp18___0_56[1] = sp18_message_170[0]  # type = ?
        sp18___0_67[1] = sp18_message_170[0]  # type = ?
        sp18_message_172 = message(text='12')
        sp18___0_15[1] = sp18_message_172[0]  # type = ?
        sp18___0_32[1] = sp18_message_172[0]  # type = ?
        sp18___0_34[1] = sp18_message_172[0]  # type = ?
        sp18___0_35[1] = sp18_message_172[0]  # type = ?
        sp18___0_52[1] = sp18_message_172[0]  # type = ?
        sp18___0_55[1] = sp18_message_172[0]  # type = ?
        sp18___0_56[1] = sp18_message_172[0]  # type = ?
        sp18_message_173 = message(text='12')
        sp18___0_15[1] = sp18_message_173[0]  # type = ?
        sp18___0_32[1] = sp18_message_173[0]  # type = ?
        sp18___0_34[1] = sp18_message_173[0]  # type = ?
        sp18___0_35[1] = sp18_message_173[0]  # type = ?
        sp18___0_52[1] = sp18_message_173[0]  # type = ?
        sp18___0_55[1] = sp18_message_173[0]  # type = ?
        sp18_message_174 = message(text='12')
        sp18___0_15[1] = sp18_message_174[0]  # type = ?
        sp18___0_32[1] = sp18_message_174[0]  # type = ?
        sp18___0_34[1] = sp18_message_174[0]  # type = ?
        sp18___0_35[1] = sp18_message_174[0]  # type = ?
        sp18___0_52[1] = sp18_message_174[0]  # type = ?
        sp18_message_175 = message(text='12')
        sp18___0_15[1] = sp18_message_175[0]  # type = ?
        sp18___0_32[1] = sp18_message_175[0]  # type = ?
        sp18___0_34[1] = sp18_message_175[0]  # type = ?
        sp18___0_35[1] = sp18_message_175[0]  # type = ?
        sp18_message_18 = message(text='clear')
        sp18_jitmatrix_1_char_4_4_30[0] = sp18_message_18[0]  # type = ?
        sp18_message_180 = message(text='12')
        sp18___0_15[1] = sp18_message_180[0]  # type = ?
        sp18___0_32[1] = sp18_message_180[0]  # type = ?
        sp18___0_34[1] = sp18_message_180[0]  # type = ?
        sp18_message_181 = message(text='12')
        sp18___0_15[1] = sp18_message_181[0]  # type = ?
        sp18___0_32[1] = sp18_message_181[0]  # type = ?
        sp18_message_182 = message(text='12')
        sp18___0_15[1] = sp18_message_182[0]  # type = ?
        sp18_message_183 = message(text='0')
        sp18___0_15[1] = sp18_message_183[0]  # type = ?
        sp18___0_32[1] = sp18_message_183[0]  # type = ?
        sp18___0_34[1] = sp18_message_183[0]  # type = ?
        sp18___0_35[1] = sp18_message_183[0]  # type = ?
        sp18___0_52[1] = sp18_message_183[0]  # type = ?
        sp18___0_55[1] = sp18_message_183[0]  # type = ?
        sp18___0_56[1] = sp18_message_183[0]  # type = ?
        sp18___0_67[1] = sp18_message_183[0]  # type = ?
        sp18___0_68[1] = sp18_message_183[0]  # type = ?
        sp18_message_199 = message(text='0')
        sp18_expr_12i1_66[0] = sp18_message_199[0]  # type = ?
        sp18_message_201 = message(text='1')
        sp18_expr_12i1_65[0] = sp18_message_201[0]  # type = ?
        sp18_message_203 = message(text='2')
        sp18_expr_12i1_64[0] = sp18_message_203[0]  # type = ?
        sp18_message_206 = message(text='4')
        sp18_expr_12i1_63[0] = sp18_message_206[0]  # type = ?
        sp18_message_246 = message(text='setcell $2 $3 val $1')
        sp18_jitmatrix_1_char_4_4_30[0] = sp18_message_246[0]  # type = ?
        sp18_message_86 = message(text='dim $1 $1')
        sp18_jitmatrix_1_char_4_4_30[0] = sp18_message_86[0]  # type = ?
        sp18_message_87 = message(text='9')
        sp18_expr_12i1_57[0] = sp18_message_87[0]  # type = ?
        sp18_message_88 = message(text='8')
        sp18_expr_12i1_59[0] = sp18_message_88[0]  # type = ?
        sp18_message_89 = message(text='6')
        sp18_expr_12i1_61[0] = sp18_message_89[0]  # type = ?
        sp18_message_90 = message(text='5')
        sp18_expr_12i1_62[0] = sp18_message_90[0]  # type = ?
        sp18_message_98 = message(text='0')
        sp18_expr_12i1_49[0] = sp18_message_98[0]  # type = ?
        sp18_number_121 = number(parameter_enable=0)
        sp18_pack_0_0_0_25[2] = sp18_number_121[0]  # type = ?
        sp18_if_i1__0_then_bang_54[0] = sp18_number_121[0]  # type = ?
        sp18___1_73[0] = sp18_number_121[0]  # type = ?
        sp18_sel_i1_80[0] = sp18_number_121[0]  # type = ?
        sp18_number_14 = number(parameter_enable=0)
        sp18_counter_0_10_124[4] = sp18_number_14[0]  # type = ?
        sp18_if_i1__i2_then_bang_141[1] = sp18_number_14[0]  # type = ?
        sp18_counter_0_10_149[4] = sp18_number_14[0]  # type = ?
        sp18_message_153[1] = sp18_number_14[0]  # type = ?
        sp18_number_140 = number(parameter_enable=0)
        sp18_if_i1__i2_then_bang_141[0] = sp18_number_140[0]  # type = ?
        sp18_number_151 = number(parameter_enable=0)
        sp18___1_150[0] = sp18_number_151[0]  # type = ?
        sp18_number_169 = number(parameter_enable=0)
        sp18_sel_0_1_2_3_4_5_6_7_8_190[0] = sp18_number_169[0]  # type = ?
        sp18_number_17 = number(parameter_enable=0)
        sp18___1_114[0] = sp18_number_17[0]  # type = ?
        sp18_counter_1_9_130[4] = sp18_number_17[0]  # type = ?
        sp18_number_14[0] = sp18_number_17[0]  # type = ?
        sp18_button_24[0] = sp18_number_17[0]  # type = ?
        sp18_uzi_0_53[1] = sp18_number_17[0]  # type = ?
        sp18_outlet_70[0] = sp18_number_17[0]  # type = ?
        sp18_sel_i1_80[1] = sp18_number_17[0]  # type = ?
        sp18_message_86[0] = sp18_number_17[0]  # type = ?
        sp18_number_188 = number(parameter_enable=0)
        sp18_sel_1_2_3_4_5_6_7_8_9_189[0] = sp18_number_188[0]  # type = ?
        sp18_if_i1__0_then_bang_29[0] = sp18_number_188[0]  # type = ?
        sp18_number_2 = number(parameter_enable=0)
        sp18_button_10[0] = sp18_number_2[0]  # type = ?
        sp18_message_199[1] = sp18_number_2[0]  # type = ?
        sp18_number_20 = number(parameter_enable=0)
        sp18_message_87[1] = sp18_number_20[0]  # type = ?
        sp18_number_200 = number(parameter_enable=0)
        sp18_number_243[0] = sp18_number_200[0]  # type = ?
        sp18_number_202 = number(parameter_enable=0)
        sp18_number_243[0] = sp18_number_202[0]  # type = ?
        sp18_number_204 = number(parameter_enable=0)
        sp18_number_243[0] = sp18_number_204[0]  # type = ?
        sp18_number_205 = number(parameter_enable=0)
        sp18_number_243[0] = sp18_number_205[0]  # type = ?
        sp18_number_207 = number(parameter_enable=0)
        sp18_number_243[0] = sp18_number_207[0]  # type = ?
        sp18_number_208 = number(parameter_enable=0)
        sp18_number_243[0] = sp18_number_208[0]  # type = ?
        sp18_number_209 = number(parameter_enable=0)
        sp18_number_243[0] = sp18_number_209[0]  # type = ?
        sp18_number_21 = number(parameter_enable=0)
        sp18_message_88[1] = sp18_number_21[0]  # type = ?
        sp18_number_210 = number(parameter_enable=0)
        sp18_number_243[0] = sp18_number_210[0]  # type = ?
        sp18_number_211 = number(parameter_enable=0)
        sp18_number_243[0] = sp18_number_211[0]  # type = ?
        sp18_number_22 = number(parameter_enable=0)
        sp18_message_89[1] = sp18_number_22[0]  # type = ?
        sp18_number_23 = number(parameter_enable=0)
        sp18_message_90[1] = sp18_number_23[0]  # type = ?
        sp18_number_243 = number(mousefilter=1, parameter_enable=0, triscale=0.9)
        sp18_pack_0_0_0_25[0] = sp18_number_243[0]  # type = ?
        sp18_number_245 = number(maximum=15, minimum=0, mousefilter=1, parameter_enable=0, triscale=0.9)
        sp18_pack_0_0_0_25[1] = sp18_number_245[0]  # type = ?
        sp18_number_26 = number(parameter_enable=0)
        sp18_message_98[1] = sp18_number_26[0]  # type = ?
        sp18_number_36 = number(parameter_enable=0)
        sp18___0_100[1] = sp18_number_36[0]  # type = ?
        sp18_number_37 = number(parameter_enable=0)
        sp18___0_38[1] = sp18_number_37[0]  # type = ?
        sp18___0_40[1] = sp18_number_37[0]  # type = ?
        sp18___0_41[1] = sp18_number_37[0]  # type = ?
        sp18___0_42[1] = sp18_number_37[0]  # type = ?
        sp18___0_43[1] = sp18_number_37[0]  # type = ?
        sp18___0_44[1] = sp18_number_37[0]  # type = ?
        sp18___0_45[1] = sp18_number_37[0]  # type = ?
        sp18___0_47[1] = sp18_number_37[0]  # type = ?
        sp18___0_48[1] = sp18_number_37[0]  # type = ?
        sp18_number_4 = number(parameter_enable=0)
        sp18_message_201[1] = sp18_number_4[0]  # type = ?
        sp18_number_6 = number(parameter_enable=0)
        sp18_message_203[1] = sp18_number_6[0]  # type = ?
        sp18_number_74 = number(parameter_enable=0)
        sp18_number_9 = number(parameter_enable=0)
        sp18_message_206[1] = sp18_number_9[0]  # type = ?
        sp18_number_99 = number(parameter_enable=0)
        sp18___0_100[0] = sp18_number_99[0]  # type = ?
        sp18_outlet_70 = outlet(comment='', index=1)
        sp18_outlet_71 = outlet(comment='', index=2)
        sp18_p_limiteri_103 = _subpatcher(subpatcher_id=0, text='p LimiterI')
        sp18___0_68[0] = sp18_p_limiteri_103[0]  # type = int
        sp18___0_67[0] = sp18_p_limiteri_103[1]  # type = int
        sp18___0_56[0] = sp18_p_limiteri_103[2]  # type = int
        sp18___0_55[0] = sp18_p_limiteri_103[3]  # type = int
        sp18___0_52[0] = sp18_p_limiteri_103[4]  # type = int
        sp18___0_35[0] = sp18_p_limiteri_103[5]  # type = int
        sp18___0_34[0] = sp18_p_limiteri_103[6]  # type = int
        sp18___0_32[0] = sp18_p_limiteri_103[7]  # type = int
        sp18___0_15[0] = sp18_p_limiteri_103[8]  # type = int
        sp18_pack_0_0_0_25 = newobj(text='pack 0 0 0')
        sp18_message_246[0] = sp18_pack_0_0_0_25[0]  # type = ?
        sp18_print_left_239 = newobj(text='print left')
        sp18_print_right_28 = newobj(text='print right')
        sp18_receive_101 = receive(text='r PcRange')
        sp18_number_36[0] = sp18_receive_101[0]  # type = ?
        sp18_receive_142 = receive(text='r cardinal')
        sp18_int_97[1] = sp18_receive_142[0]  # type = ?
        sp18_receive_194 = receive(text='r init')
        sp18_number_169[0] = sp18_receive_194[0]  # type = ?
        sp18_receive_69 = receive(text='r out_I')
        sp18_button_27[0] = sp18_receive_69[0]  # type = ?
        sp18_receive_75 = receive(text='r PcRange')
        sp18___0_76[1] = sp18_receive_75[0]  # type = ?
        sp18_sel_0_1_2_3_4_5_6_7_8_190 = newobj(text='sel 0 1 2 3 4 5 6 7 8')
        sp18_button_187[0] = sp18_sel_0_1_2_3_4_5_6_7_8_190[0]  # type = bang
        sp18_button_186[0] = sp18_sel_0_1_2_3_4_5_6_7_8_190[1]  # type = bang
        sp18_button_185[0] = sp18_sel_0_1_2_3_4_5_6_7_8_190[2]  # type = bang
        sp18_button_184[0] = sp18_sel_0_1_2_3_4_5_6_7_8_190[3]  # type = bang
        sp18_button_179[0] = sp18_sel_0_1_2_3_4_5_6_7_8_190[4]  # type = bang
        sp18_button_178[0] = sp18_sel_0_1_2_3_4_5_6_7_8_190[5]  # type = bang
        sp18_button_177[0] = sp18_sel_0_1_2_3_4_5_6_7_8_190[6]  # type = bang
        sp18_button_176[0] = sp18_sel_0_1_2_3_4_5_6_7_8_190[7]  # type = bang
        sp18_button_171[0] = sp18_sel_0_1_2_3_4_5_6_7_8_190[8]  # type = bang
        sp18_button_167[0] = sp18_sel_0_1_2_3_4_5_6_7_8_190[9]  # type = ?
        sp18_sel_1_2_3_4_5_6_7_8_9_189 = newobj(text='sel 1 2 3 4 5 6 7 8 9')
        sp18_button_191[0] = sp18_sel_1_2_3_4_5_6_7_8_9_189[0]  # type = bang
        sp18_button_193[0] = sp18_sel_1_2_3_4_5_6_7_8_9_189[1]  # type = bang
        sp18_button_195[0] = sp18_sel_1_2_3_4_5_6_7_8_9_189[2]  # type = bang
        sp18_button_197[0] = sp18_sel_1_2_3_4_5_6_7_8_9_189[3]  # type = bang
        sp18_button_94[0] = sp18_sel_1_2_3_4_5_6_7_8_9_189[4]  # type = bang
        sp18_button_93[0] = sp18_sel_1_2_3_4_5_6_7_8_9_189[5]  # type = bang
        sp18_button_92[0] = sp18_sel_1_2_3_4_5_6_7_8_9_189[6]  # type = bang
        sp18_button_91[0] = sp18_sel_1_2_3_4_5_6_7_8_9_189[7]  # type = bang
        sp18_button_102[0] = sp18_sel_1_2_3_4_5_6_7_8_9_189[8]  # type = bang
        sp18_sel_i1_80 = newobj(text='sel $i1')
        sp18_button_27[0] = sp18_sel_i1_80[0]  # type = bang
        sp18_send_192 = send(text='s init')
        sp18_t_b_i_95 = newobj(text='t b i')
        sp18_int_72[0] = sp18_t_b_i_95[0]  # type = bang
        sp18_int_77[0] = sp18_t_b_i_95[0]  # type = bang
        sp18_int_78[0] = sp18_t_b_i_95[0]  # type = bang
        sp18_int_79[0] = sp18_t_b_i_95[0]  # type = bang
        sp18_int_81[0] = sp18_t_b_i_95[0]  # type = bang
        sp18_int_82[0] = sp18_t_b_i_95[0]  # type = bang
        sp18_int_83[0] = sp18_t_b_i_95[0]  # type = bang
        sp18_int_84[0] = sp18_t_b_i_95[0]  # type = bang
        sp18_int_85[0] = sp18_t_b_i_95[0]  # type = bang
        sp18_button_96[0] = sp18_t_b_i_95[0]  # type = bang
        sp18_int_97[0] = sp18_t_b_i_95[0]  # type = bang
        sp18_number_99[0] = sp18_t_b_i_95[1]  # type = int
        sp18_toggle_156 = toggle(parameter_enable=0)
        sp18_toggle_157 = toggle(parameter_enable=0)
        sp18_toggle_158 = toggle(parameter_enable=0)
        sp18_toggle_159 = toggle(parameter_enable=0)
        sp18_toggle_160 = toggle(parameter_enable=0)
        sp18_toggle_161 = toggle(parameter_enable=0)
        sp18_toggle_162 = toggle(parameter_enable=0)
        sp18_toggle_163 = toggle(parameter_enable=0)
        sp18_toggle_164 = toggle(parameter_enable=0)
        sp18_toggle_165 = toggle(parameter_enable=0)
        sp18_uzi_0_53 = newobj(text='Uzi 0')
        sp18_counter_1_9_130[0] = sp18_uzi_0_53[0]  # type = bang
        return sp18_outlet_71, sp18_outlet_70

    #----------------------------------------------------------------------
    def subpatcher_0(sp18_sp0_inlet_137, sp18_sp0_inlet_139, sp18_sp0_inlet_143, sp18_sp0_inlet_144, sp18_sp0_inlet_146, sp18_sp0_inlet_148, sp18_sp0_inlet_149, sp18_sp0_inlet_150, sp18_sp0_inlet_151):
        sp18_sp0_outlet_153 = outlet(comment='', index=2)
        sp18_sp0_p_limitt_126 = _subpatcher(subpatcher_id=7, text='p LimitT')
        sp18_sp0_outlet_153[0] = sp18_sp0_p_limitt_126[0]  # type = int
        sp18_sp0_inlet_139 = inlet(comment='', index=2)
        sp18_sp0_p_limitt_126[0] = sp18_sp0_inlet_139[0]  # type = int
        sp18_sp0_outlet_159 = outlet(comment='', index=8)
        sp18_sp0_p_limitt_133 = _subpatcher(subpatcher_id=1, text='p LimitT')
        sp18_sp0_outlet_159[0] = sp18_sp0_p_limitt_133[0]  # type = int
        sp18_sp0_outlet_154 = outlet(comment='', index=3)
        sp18_sp0_p_limitt_127 = _subpatcher(subpatcher_id=6, text='p LimitT')
        sp18_sp0_outlet_154[0] = sp18_sp0_p_limitt_127[0]  # type = int
        sp18_sp0_inlet_143 = inlet(comment='', index=3)
        sp18_sp0_p_limitt_127[0] = sp18_sp0_inlet_143[0]  # type = int
        sp18_sp0_outlet_155 = outlet(comment='', index=4)
        sp18_sp0_p_limitt_128 = _subpatcher(subpatcher_id=5, text='p LimitT')
        sp18_sp0_outlet_155[0] = sp18_sp0_p_limitt_128[0]  # type = int
        sp18_sp0_inlet_144 = inlet(comment='', index=4)
        sp18_sp0_p_limitt_128[0] = sp18_sp0_inlet_144[0]  # type = int
        sp18_sp0_outlet_160 = outlet(comment='', index=9)
        sp18_sp0_p_limitt_134 = _subpatcher(subpatcher_id=0, text='p LimitT')
        sp18_sp0_outlet_160[0] = sp18_sp0_p_limitt_134[0]  # type = int
        sp18_sp0_inlet_151 = inlet(comment='', index=9)
        sp18_sp0_p_limitt_134[0] = sp18_sp0_inlet_151[0]  # type = int
        sp18_sp0_outlet_158 = outlet(comment='', index=7)
        sp18_sp0_p_limitt_132 = _subpatcher(subpatcher_id=2, text='p LimitT')
        sp18_sp0_outlet_158[0] = sp18_sp0_p_limitt_132[0]  # type = int
        sp18_sp0_inlet_149 = inlet(comment='', index=7)
        sp18_sp0_p_limitt_132[0] = sp18_sp0_inlet_149[0]  # type = int
        sp18_sp0_outlet_157 = outlet(comment='', index=6)
        sp18_sp0_p_limitt_131 = _subpatcher(subpatcher_id=3, text='p LimitT')
        sp18_sp0_outlet_157[0] = sp18_sp0_p_limitt_131[0]  # type = int
        sp18_sp0_inlet_148 = inlet(comment='', index=6)
        sp18_sp0_p_limitt_131[0] = sp18_sp0_inlet_148[0]  # type = int
        sp18_sp0_inlet_150 = inlet(comment='', index=8)
        sp18_sp0_p_limitt_133[0] = sp18_sp0_inlet_150[0]  # type = int
        sp18_sp0_outlet_152 = outlet(comment='', index=1)
        sp18_sp0_p_limitt_125 = _subpatcher(subpatcher_id=8, text='p LimitT')
        sp18_sp0_outlet_152[0] = sp18_sp0_p_limitt_125[0]  # type = int
        sp18_sp0_outlet_156 = outlet(comment='', index=5)
        sp18_sp0_p_limitt_129 = _subpatcher(subpatcher_id=4, text='p LimitT')
        sp18_sp0_outlet_156[0] = sp18_sp0_p_limitt_129[0]  # type = int
        sp18_sp0_inlet_137 = inlet(comment='', index=1)
        sp18_sp0_p_limitt_125[0] = sp18_sp0_inlet_137[0]  # type = int
        sp18_sp0_inlet_146 = inlet(comment='', index=5)
        sp18_sp0_p_limitt_129[0] = sp18_sp0_inlet_146[0]  # type = int
        return sp18_sp0_outlet_152, sp18_sp0_outlet_153, sp18_sp0_outlet_154, sp18_sp0_outlet_155, sp18_sp0_outlet_156, sp18_sp0_outlet_157, sp18_sp0_outlet_158, sp18_sp0_outlet_159, sp18_sp0_outlet_160

    #----------------------------------------------------------------------
    def subpatcher_0(sp18_sp0_sp0_inlet_122):
        sp18_sp0_sp0_outlet_123 = outlet(comment='', index=1)
        sp18_sp0_sp0___0_120 = newobj(text='- 0')
        sp18_sp0_sp0_outlet_123[0] = sp18_sp0_sp0___0_120[0]  # type = int
        sp18_sp0_sp0_number_107 = number(parameter_enable=0)
        sp18_sp0_sp0___0_120[1] = sp18_sp0_sp0_number_107[0]  # type = ?
        sp18_sp0_sp0_if_i1__111_then_24_else_0_104 = newobj(text='if $i1 > 111 then 24 else 0')
        sp18_sp0_sp0_number_107[0] = sp18_sp0_sp0_if_i1__111_then_24_else_0_104[0]  # type = ?
        sp18_sp0_sp0_inlet_122 = inlet(comment='', index=1)
        sp18_sp0_sp0_if_i1__111_then_24_else_0_104[0] = sp18_sp0_sp0_inlet_122[0]  # type = int
        sp18_sp0_sp0___0_120[0] = sp18_sp0_sp0_inlet_122[0]  # type = int
        return sp18_sp0_sp0_outlet_123

    #----------------------------------------------------------------------
    def subpatcher_1(sp18_sp0_sp1_inlet_122):
        sp18_sp0_sp1_outlet_123 = outlet(comment='', index=1)
        sp18_sp0_sp1___0_120 = newobj(text='- 0')
        sp18_sp0_sp1_outlet_123[0] = sp18_sp0_sp1___0_120[0]  # type = int
        sp18_sp0_sp1_number_107 = number(parameter_enable=0)
        sp18_sp0_sp1___0_120[1] = sp18_sp0_sp1_number_107[0]  # type = ?
        sp18_sp0_sp1_if_i1__110_then_24_else_0_104 = newobj(text='if $i1 > 110 then 24 else 0')
        sp18_sp0_sp1_number_107[0] = sp18_sp0_sp1_if_i1__110_then_24_else_0_104[0]  # type = ?
        sp18_sp0_sp1_inlet_122 = inlet(comment='', index=1)
        sp18_sp0_sp1_if_i1__110_then_24_else_0_104[0] = sp18_sp0_sp1_inlet_122[0]  # type = int
        sp18_sp0_sp1___0_120[0] = sp18_sp0_sp1_inlet_122[0]  # type = int
        return sp18_sp0_sp1_outlet_123

    #----------------------------------------------------------------------
    def subpatcher_2(sp18_sp0_sp2_inlet_122):
        sp18_sp0_sp2_outlet_123 = outlet(comment='', index=1)
        sp18_sp0_sp2___0_120 = newobj(text='- 0')
        sp18_sp0_sp2_outlet_123[0] = sp18_sp0_sp2___0_120[0]  # type = int
        sp18_sp0_sp2_number_107 = number(parameter_enable=0)
        sp18_sp0_sp2___0_120[1] = sp18_sp0_sp2_number_107[0]  # type = ?
        sp18_sp0_sp2_if_i1__109_then_24_else_0_104 = newobj(text='if $i1 > 109 then 24 else 0')
        sp18_sp0_sp2_number_107[0] = sp18_sp0_sp2_if_i1__109_then_24_else_0_104[0]  # type = ?
        sp18_sp0_sp2_inlet_122 = inlet(comment='', index=1)
        sp18_sp0_sp2_if_i1__109_then_24_else_0_104[0] = sp18_sp0_sp2_inlet_122[0]  # type = int
        sp18_sp0_sp2___0_120[0] = sp18_sp0_sp2_inlet_122[0]  # type = int
        return sp18_sp0_sp2_outlet_123

    #----------------------------------------------------------------------
    def subpatcher_3(sp18_sp0_sp3_inlet_122):
        sp18_sp0_sp3_outlet_123 = outlet(comment='', index=1)
        sp18_sp0_sp3___0_120 = newobj(text='- 0')
        sp18_sp0_sp3_outlet_123[0] = sp18_sp0_sp3___0_120[0]  # type = int
        sp18_sp0_sp3_number_107 = number(parameter_enable=0)
        sp18_sp0_sp3___0_120[1] = sp18_sp0_sp3_number_107[0]  # type = ?
        sp18_sp0_sp3_if_i1__108_then_24_else_0_104 = newobj(text='if $i1 > 108 then 24 else 0')
        sp18_sp0_sp3_number_107[0] = sp18_sp0_sp3_if_i1__108_then_24_else_0_104[0]  # type = ?
        sp18_sp0_sp3_inlet_122 = inlet(comment='', index=1)
        sp18_sp0_sp3_if_i1__108_then_24_else_0_104[0] = sp18_sp0_sp3_inlet_122[0]  # type = int
        sp18_sp0_sp3___0_120[0] = sp18_sp0_sp3_inlet_122[0]  # type = int
        return sp18_sp0_sp3_outlet_123

    #----------------------------------------------------------------------
    def subpatcher_4(sp18_sp0_sp4_inlet_122):
        sp18_sp0_sp4_outlet_123 = outlet(comment='', index=1)
        sp18_sp0_sp4___0_120 = newobj(text='- 0')
        sp18_sp0_sp4_outlet_123[0] = sp18_sp0_sp4___0_120[0]  # type = int
        sp18_sp0_sp4_number_107 = number(parameter_enable=0)
        sp18_sp0_sp4___0_120[1] = sp18_sp0_sp4_number_107[0]  # type = ?
        sp18_sp0_sp4_if_i1__107_then_24_else_0_104 = newobj(text='if $i1 > 107 then 24 else 0')
        sp18_sp0_sp4_number_107[0] = sp18_sp0_sp4_if_i1__107_then_24_else_0_104[0]  # type = ?
        sp18_sp0_sp4_inlet_122 = inlet(comment='', index=1)
        sp18_sp0_sp4_if_i1__107_then_24_else_0_104[0] = sp18_sp0_sp4_inlet_122[0]  # type = int
        sp18_sp0_sp4___0_120[0] = sp18_sp0_sp4_inlet_122[0]  # type = int
        return sp18_sp0_sp4_outlet_123

    #----------------------------------------------------------------------
    def subpatcher_5(sp18_sp0_sp5_inlet_122):
        sp18_sp0_sp5_outlet_123 = outlet(comment='', index=1)
        sp18_sp0_sp5___0_120 = newobj(text='- 0')
        sp18_sp0_sp5_outlet_123[0] = sp18_sp0_sp5___0_120[0]  # type = int
        sp18_sp0_sp5_number_107 = number(parameter_enable=0)
        sp18_sp0_sp5___0_120[1] = sp18_sp0_sp5_number_107[0]  # type = ?
        sp18_sp0_sp5_if_i1__106_then_24_else_0_104 = newobj(text='if $i1 > 106 then 24 else 0')
        sp18_sp0_sp5_number_107[0] = sp18_sp0_sp5_if_i1__106_then_24_else_0_104[0]  # type = ?
        sp18_sp0_sp5_inlet_122 = inlet(comment='', index=1)
        sp18_sp0_sp5_if_i1__106_then_24_else_0_104[0] = sp18_sp0_sp5_inlet_122[0]  # type = int
        sp18_sp0_sp5___0_120[0] = sp18_sp0_sp5_inlet_122[0]  # type = int
        return sp18_sp0_sp5_outlet_123

    #----------------------------------------------------------------------
    def subpatcher_6(sp18_sp0_sp6_inlet_122):
        sp18_sp0_sp6_outlet_123 = outlet(comment='', index=1)
        sp18_sp0_sp6___0_120 = newobj(text='- 0')
        sp18_sp0_sp6_outlet_123[0] = sp18_sp0_sp6___0_120[0]  # type = int
        sp18_sp0_sp6_number_107 = number(parameter_enable=0)
        sp18_sp0_sp6___0_120[1] = sp18_sp0_sp6_number_107[0]  # type = ?
        sp18_sp0_sp6_if_i1__105_then_24_else_0_104 = newobj(text='if $i1 > 105 then 24 else 0')
        sp18_sp0_sp6_number_107[0] = sp18_sp0_sp6_if_i1__105_then_24_else_0_104[0]  # type = ?
        sp18_sp0_sp6_inlet_122 = inlet(comment='', index=1)
        sp18_sp0_sp6_if_i1__105_then_24_else_0_104[0] = sp18_sp0_sp6_inlet_122[0]  # type = int
        sp18_sp0_sp6___0_120[0] = sp18_sp0_sp6_inlet_122[0]  # type = int
        return sp18_sp0_sp6_outlet_123

    #----------------------------------------------------------------------
    def subpatcher_7(sp18_sp0_sp7_inlet_122):
        sp18_sp0_sp7_outlet_123 = outlet(comment='', index=1)
        sp18_sp0_sp7___0_120 = newobj(text='- 0')
        sp18_sp0_sp7_outlet_123[0] = sp18_sp0_sp7___0_120[0]  # type = int
        sp18_sp0_sp7_number_107 = number(parameter_enable=0)
        sp18_sp0_sp7___0_120[1] = sp18_sp0_sp7_number_107[0]  # type = ?
        sp18_sp0_sp7_if_i1__104_then_24_else_0_104 = newobj(text='if $i1 > 104 then 24 else 0')
        sp18_sp0_sp7_number_107[0] = sp18_sp0_sp7_if_i1__104_then_24_else_0_104[0]  # type = ?
        sp18_sp0_sp7_inlet_122 = inlet(comment='', index=1)
        sp18_sp0_sp7_if_i1__104_then_24_else_0_104[0] = sp18_sp0_sp7_inlet_122[0]  # type = int
        sp18_sp0_sp7___0_120[0] = sp18_sp0_sp7_inlet_122[0]  # type = int
        return sp18_sp0_sp7_outlet_123

    #----------------------------------------------------------------------
    def subpatcher_8(sp18_sp0_sp8_inlet_122):
        sp18_sp0_sp8_outlet_123 = outlet(comment='', index=1)
        sp18_sp0_sp8___0_120 = newobj(text='- 0')
        sp18_sp0_sp8_outlet_123[0] = sp18_sp0_sp8___0_120[0]  # type = int
        sp18_sp0_sp8_number_107 = number(parameter_enable=0)
        sp18_sp0_sp8___0_120[1] = sp18_sp0_sp8_number_107[0]  # type = ?
        sp18_sp0_sp8_if_i1__103_then_24_else_0_104 = newobj(text='if $i1 > 103 then 24 else 0')
        sp18_sp0_sp8_number_107[0] = sp18_sp0_sp8_if_i1__103_then_24_else_0_104[0]  # type = ?
        sp18_sp0_sp8_inlet_122 = inlet(comment='', index=1)
        sp18_sp0_sp8_if_i1__103_then_24_else_0_104[0] = sp18_sp0_sp8_inlet_122[0]  # type = int
        sp18_sp0_sp8___0_120[0] = sp18_sp0_sp8_inlet_122[0]  # type = int
        return sp18_sp0_sp8_outlet_123

    #----------------------------------------------------------------------
    def subpatcher_19(sp19_inlet_19, sp19_inlet_11, sp19_inlet_1, sp19_inlet_3, sp19_inlet_5, sp19_inlet_8, sp19_inlet_50, sp19_inlet_51, sp19_inlet_58, sp19_inlet_60):
        sp19___0_38 = newobj(text='+ 0')
        sp19_p_limitert_161[8] = sp19___0_38[0]  # type = int
        sp19___0_40 = newobj(text='+ 0')
        sp19_p_limitert_161[7] = sp19___0_40[0]  # type = int
        sp19___0_41 = newobj(text='+ 0')
        sp19_p_limitert_161[6] = sp19___0_41[0]  # type = int
        sp19___0_42 = newobj(text='+ 0')
        sp19_p_limitert_161[5] = sp19___0_42[0]  # type = int
        sp19___0_43 = newobj(text='+ 0')
        sp19_p_limitert_161[4] = sp19___0_43[0]  # type = int
        sp19___0_44 = newobj(text='+ 0')
        sp19_p_limitert_161[3] = sp19___0_44[0]  # type = int
        sp19___0_45 = newobj(text='+ 0')
        sp19_p_limitert_161[2] = sp19___0_45[0]  # type = int
        sp19___0_47 = newobj(text='+ 0')
        sp19_p_limitert_161[1] = sp19___0_47[0]  # type = int
        sp19___0_48 = newobj(text='+ 0')
        sp19_p_limitert_161[0] = sp19___0_48[0]  # type = int
        sp19___0_56 = newobj(text='+ 0')
        sp19_number_243[0] = sp19___0_56[0]  # type = int
        sp19___0_57 = newobj(text='+ 0')
        sp19_number_243[0] = sp19___0_57[0]  # type = int
        sp19___0_59 = newobj(text='+ 0')
        sp19_number_243[0] = sp19___0_59[0]  # type = int
        sp19___0_61 = newobj(text='+ 0')
        sp19_number_243[0] = sp19___0_61[0]  # type = int
        sp19___0_62 = newobj(text='+ 0')
        sp19_number_243[0] = sp19___0_62[0]  # type = int
        sp19___0_63 = newobj(text='+ 0')
        sp19_number_243[0] = sp19___0_63[0]  # type = int
        sp19___0_64 = newobj(text='+ 0')
        sp19_number_243[0] = sp19___0_64[0]  # type = int
        sp19___0_65 = newobj(text='+ 0')
        sp19_number_243[0] = sp19___0_65[0]  # type = int
        sp19___0_66 = newobj(text='+ 0')
        sp19_number_243[0] = sp19___0_66[0]  # type = int
        sp19___0_83 = newobj(text='+ 0')
        sp19_number_37[0] = sp19___0_83[0]  # type = int
        sp19___1_114 = newobj(text='- 1')
        sp19_counter_0_8_113[4] = sp19___1_114[0]  # type = int
        sp19___1_36 = newobj(text='+ 1')
        sp19_counter_1_9_130[2] = sp19___1_36[0]  # type = int
        sp19_accum_1_136 = newobj(text='accum 1')
        sp19_number_140[0] = sp19_accum_1_136[0]  # type = int
        sp19_button_10 = button()
        sp19_uzi_0_53[0] = sp19_button_10[0]  # type = bang
        sp19_button_102 = button()
        sp19_message_98[0] = sp19_button_102[0]  # type = bang
        sp19_button_110 = button()
        sp19_counter_0_10_124[0] = sp19_button_110[0]  # type = bang
        sp19_button_112 = button()
        sp19_counter_0_8_113[0] = sp19_button_112[0]  # type = bang
        sp19_button_12 = button()
        sp19_accum_1_136[0] = sp19_button_12[0]  # type = bang
        sp19_message_138[0] = sp19_button_12[0]  # type = bang
        sp19_button_145 = button()
        sp19_button_10[0] = sp19_button_145[0]  # type = bang
        sp19_button_191 = button()
        sp19_message_199[0] = sp19_button_191[0]  # type = bang
        sp19_button_193 = button()
        sp19_message_201[0] = sp19_button_193[0]  # type = bang
        sp19_button_195 = button()
        sp19_message_203[0] = sp19_button_195[0]  # type = bang
        sp19_button_197 = button()
        sp19_message_206[0] = sp19_button_197[0]  # type = bang
        sp19_button_24 = button()
        sp19_message_135[0] = sp19_button_24[0]  # type = bang
        sp19_message_147[0] = sp19_button_24[0]  # type = bang
        sp19_message_16[0] = sp19_button_24[0]  # type = bang
        sp19_message_18[0] = sp19_button_24[0]  # type = bang
        sp19_button_27 = button()
        sp19_jitmatrix_1_char_4_4_30[0] = sp19_button_27[0]  # type = bang
        sp19_button_52 = button()
        sp19_button_91 = button()
        sp19_message_87[0] = sp19_button_91[0]  # type = bang
        sp19_button_92 = button()
        sp19_message_88[0] = sp19_button_92[0]  # type = bang
        sp19_button_93 = button()
        sp19_message_89[0] = sp19_button_93[0]  # type = bang
        sp19_button_94 = button()
        sp19_message_90[0] = sp19_button_94[0]  # type = bang
# (comment) Set/Get contents
# (comment) value
# (comment) y position
# (comment) Create a matrix
# (comment) x position
# (comment) f.
# (comment) Permutacion Circular Matriz Transpuesta
# (comment) Sums 12 when ivversion start from 0 to n-1
# (comment) output
# (comment) a.
# (comment) b.
# (comment) c.
# (comment) d.
# (comment) e.
        sp19_counter_0_10_124 = newobj(text='counter 0 10')
        sp19_number_121[0] = sp19_counter_0_10_124[0]  # type = int
        sp19_counter_0_8_113 = newobj(text='counter 0 8')
        sp19_number_245[0] = sp19_counter_0_8_113[0]  # type = int
        sp19_delay_10_39[0] = sp19_counter_0_8_113[3]  # type = int
        sp19_counter_1_9_130 = newobj(text='counter 1 9')
        sp19_number_188[0] = sp19_counter_1_9_130[0]  # type = int
        sp19_delay_10_39 = newobj(text='delay 10')
        sp19_button_110[0] = sp19_delay_10_39[0]  # type = bang
        sp19_if_i1__0_then_bang_29 = newobj(text='if $i1 != 0 then bang')
        sp19_button_112[0] = sp19_if_i1__0_then_bang_29[0]  # type = ?
        sp19_if_i1__0_then_bang_54 = newobj(text='if $i1 != 0 then bang')
        sp19_button_12[0] = sp19_if_i1__0_then_bang_54[0]  # type = ?
        sp19_if_i1__i2_then_bang_141 = newobj(text='if $i1 <= $i2 then bang')
        sp19_button_145[0] = sp19_if_i1__i2_then_bang_141[0]  # type = ?
        sp19_inlet_1 = inlet(comment='', index=5)
        sp19_int_70[1] = sp19_inlet_1[0]  # type = ?
        sp19_inlet_11 = inlet(comment='', index=9)
        sp19_int_74[1] = sp19_inlet_11[0]  # type = ?
        sp19_inlet_19 = inlet(comment='', index=10)
        sp19_t_b_i_55[0] = sp19_inlet_19[0]  # type = ?
        sp19_inlet_3 = inlet(comment='', index=6)
        sp19_int_73[1] = sp19_inlet_3[0]  # type = ?
        sp19_inlet_5 = inlet(comment='', index=7)
        sp19_int_72[1] = sp19_inlet_5[0]  # type = ?
        sp19_inlet_50 = inlet(comment='', index=1)
        sp19_int_67[1] = sp19_inlet_50[0]  # type = ?
        sp19_inlet_51 = inlet(comment='', index=2)
        sp19_int_69[1] = sp19_inlet_51[0]  # type = ?
        sp19_inlet_58 = inlet(comment='', index=3)
        sp19_int_68[1] = sp19_inlet_58[0]  # type = ?
        sp19_inlet_60 = inlet(comment='', index=4)
        sp19_int_71[1] = sp19_inlet_60[0]  # type = ?
        sp19_inlet_8 = inlet(comment='', index=8)
        sp19_int_75[1] = sp19_inlet_8[0]  # type = ?
        sp19_int_15 = int(text='int')
        sp19_number_17[0] = sp19_int_15[0]  # type = int
        sp19_int_67 = int(text='int')
        sp19_number_2[0] = sp19_int_67[0]  # type = int
        sp19_int_68 = int(text='int')
        sp19_number_6[0] = sp19_int_68[0]  # type = int
        sp19_int_69 = int(text='int')
        sp19_number_4[0] = sp19_int_69[0]  # type = int
        sp19_int_70 = int(text='int')
        sp19_number_23[0] = sp19_int_70[0]  # type = int
        sp19_int_71 = int(text='int')
        sp19_number_9[0] = sp19_int_71[0]  # type = int
        sp19_int_72 = int(text='int')
        sp19_number_21[0] = sp19_int_72[0]  # type = int
        sp19_int_73 = int(text='int')
        sp19_number_22[0] = sp19_int_73[0]  # type = int
        sp19_int_74 = int(text='int')
        sp19_number_26[0] = sp19_int_74[0]  # type = int
        sp19_int_75 = int(text='int')
        sp19_number_20[0] = sp19_int_75[0]  # type = int
        sp19_jit.cellblock_33 = jit.cellblock(cols=7, colwidth=25, rows=7)
        sp19_jitmatrix_1_char_4_4_30 = newobj(text='jit.matrix 1 char 4 4')
        sp19_jit.cellblock_33[0] = sp19_jitmatrix_1_char_4_4_30[0]  # type = jit_matrix
        sp19_outlet_34[0] = sp19_jitmatrix_1_char_4_4_30[0]  # type = jit_matrix
        sp19_jitprint_240 = newobj(text='jit.print')
        sp19_message_135 = message(text='set 0')
        sp19_accum_1_136[0] = sp19_message_135[0]  # type = ?
        sp19_message_138 = message(text='1')
        sp19_accum_1_136[1] = sp19_message_138[0]  # type = ?
        sp19_message_147 = message(text='0')
        sp19_counter_0_8_113[3] = sp19_message_147[0]  # type = ?
        sp19_counter_0_10_124[3] = sp19_message_147[0]  # type = ?
        sp19_counter_1_9_130[3] = sp19_message_147[0]  # type = ?
        sp19_message_16 = message(text='1')
        sp19_counter_1_9_130[2] = sp19_message_16[0]  # type = ?
        sp19_message_18 = message(text='clear')
        sp19_jitmatrix_1_char_4_4_30[0] = sp19_message_18[0]  # type = ?
        sp19_message_199 = message(text='0')
        sp19___0_48[0] = sp19_message_199[0]  # type = ?
        sp19_message_201 = message(text='1')
        sp19___0_47[0] = sp19_message_201[0]  # type = ?
        sp19_message_203 = message(text='2')
        sp19___0_45[0] = sp19_message_203[0]  # type = ?
        sp19_message_206 = message(text='4')
        sp19___0_44[0] = sp19_message_206[0]  # type = ?
        sp19_message_246 = message(text='setcell $2 $3 val $1')
        sp19_jitmatrix_1_char_4_4_30[0] = sp19_message_246[0]  # type = ?
        sp19_message_86 = message(text='dim $1 $1')
        sp19_jitmatrix_1_char_4_4_30[0] = sp19_message_86[0]  # type = ?
        sp19_message_87 = message(text='9')
        sp19___0_40[0] = sp19_message_87[0]  # type = ?
        sp19_message_88 = message(text='8')
        sp19___0_41[0] = sp19_message_88[0]  # type = ?
        sp19_message_89 = message(text='6')
        sp19___0_42[0] = sp19_message_89[0]  # type = ?
        sp19_message_90 = message(text='5')
        sp19___0_43[0] = sp19_message_90[0]  # type = ?
        sp19_message_98 = message(text='0')
        sp19___0_38[0] = sp19_message_98[0]  # type = ?
        sp19_number_121 = number(parameter_enable=0)
        sp19_pack_0_0_0_25[2] = sp19_number_121[0]  # type = ?
        sp19___1_36[0] = sp19_number_121[0]  # type = ?
        sp19_if_i1__0_then_bang_54[0] = sp19_number_121[0]  # type = ?
        sp19_sel_i1_80[0] = sp19_number_121[0]  # type = ?
        sp19_p_sum12_96[0] = sp19_number_121[0]  # type = ?
        sp19_number_14 = number(parameter_enable=0)
        sp19_counter_0_10_124[4] = sp19_number_14[0]  # type = ?
        sp19_if_i1__i2_then_bang_141[1] = sp19_number_14[0]  # type = ?
        sp19_number_140 = number(parameter_enable=0)
        sp19_if_i1__i2_then_bang_141[0] = sp19_number_140[0]  # type = ?
        sp19_number_17 = number(parameter_enable=0)
        sp19___1_114[0] = sp19_number_17[0]  # type = ?
        sp19_counter_1_9_130[4] = sp19_number_17[0]  # type = ?
        sp19_number_14[0] = sp19_number_17[0]  # type = ?
        sp19_button_24[0] = sp19_number_17[0]  # type = ?
        sp19_outlet_32[0] = sp19_number_17[0]  # type = ?
        sp19_uzi_0_53[1] = sp19_number_17[0]  # type = ?
        sp19_sel_i1_80[1] = sp19_number_17[0]  # type = ?
        sp19_message_86[0] = sp19_number_17[0]  # type = ?
        sp19_number_188 = number(parameter_enable=0)
        sp19_sel_1_2_3_4_5_6_7_8_9_189[0] = sp19_number_188[0]  # type = ?
        sp19_if_i1__0_then_bang_29[0] = sp19_number_188[0]  # type = ?
        sp19_number_2 = number(parameter_enable=0)
        sp19_button_10[0] = sp19_number_2[0]  # type = ?
        sp19_message_199[1] = sp19_number_2[0]  # type = ?
        sp19_number_20 = number(parameter_enable=0)
        sp19_message_87[1] = sp19_number_20[0]  # type = ?
        sp19_number_21 = number(parameter_enable=0)
        sp19_message_88[1] = sp19_number_21[0]  # type = ?
        sp19_number_22 = number(parameter_enable=0)
        sp19_message_89[1] = sp19_number_22[0]  # type = ?
        sp19_number_23 = number(parameter_enable=0)
        sp19_message_90[1] = sp19_number_23[0]  # type = ?
        sp19_number_243 = number(mousefilter=1, parameter_enable=0, triscale=0.9)
        sp19_pack_0_0_0_25[0] = sp19_number_243[0]  # type = ?
        sp19_number_245 = number(maximum=15, minimum=0, mousefilter=1, parameter_enable=0, triscale=0.9)
        sp19_pack_0_0_0_25[1] = sp19_number_245[0]  # type = ?
        sp19_number_26 = number(parameter_enable=0)
        sp19_message_98[1] = sp19_number_26[0]  # type = ?
        sp19_number_37 = number(parameter_enable=0)
        sp19___0_38[1] = sp19_number_37[0]  # type = ?
        sp19___0_40[1] = sp19_number_37[0]  # type = ?
        sp19___0_41[1] = sp19_number_37[0]  # type = ?
        sp19___0_42[1] = sp19_number_37[0]  # type = ?
        sp19___0_43[1] = sp19_number_37[0]  # type = ?
        sp19___0_44[1] = sp19_number_37[0]  # type = ?
        sp19___0_45[1] = sp19_number_37[0]  # type = ?
        sp19___0_47[1] = sp19_number_37[0]  # type = ?
        sp19___0_48[1] = sp19_number_37[0]  # type = ?
        sp19_number_4 = number(parameter_enable=0)
        sp19_message_201[1] = sp19_number_4[0]  # type = ?
        sp19_number_6 = number(parameter_enable=0)
        sp19_message_203[1] = sp19_number_6[0]  # type = ?
        sp19_number_84 = number(parameter_enable=0)
        sp19___0_83[0] = sp19_number_84[0]  # type = ?
        sp19_number_85 = number(parameter_enable=0)
        sp19___0_83[1] = sp19_number_85[0]  # type = ?
        sp19_number_9 = number(parameter_enable=0)
        sp19_message_206[1] = sp19_number_9[0]  # type = ?
        sp19_outlet_32 = outlet(comment='', index=1)
        sp19_outlet_34 = outlet(comment='', index=2)
        sp19_p_limitert_161 = _subpatcher(subpatcher_id=0, text='p LimiterT')
        sp19___0_56[0] = sp19_p_limitert_161[0]  # type = ?
        sp19___0_57[0] = sp19_p_limitert_161[1]  # type = int
        sp19___0_59[0] = sp19_p_limitert_161[2]  # type = int
        sp19___0_61[0] = sp19_p_limitert_161[3]  # type = int
        sp19___0_62[0] = sp19_p_limitert_161[4]  # type = int
        sp19___0_63[0] = sp19_p_limitert_161[5]  # type = int
        sp19___0_64[0] = sp19_p_limitert_161[6]  # type = int
        sp19___0_65[0] = sp19_p_limitert_161[7]  # type = int
        sp19___0_66[0] = sp19_p_limitert_161[8]  # type = int
        sp19_p_sum12_96 = _subpatcher(subpatcher_id=1, text='p Sum12')
        sp19___0_56[1] = sp19_p_sum12_96[0]  # type = ?
        sp19___0_57[1] = sp19_p_sum12_96[0]  # type = ?
        sp19___0_59[1] = sp19_p_sum12_96[0]  # type = ?
        sp19___0_61[1] = sp19_p_sum12_96[0]  # type = ?
        sp19___0_62[1] = sp19_p_sum12_96[0]  # type = ?
        sp19___0_63[1] = sp19_p_sum12_96[0]  # type = ?
        sp19___0_64[1] = sp19_p_sum12_96[0]  # type = ?
        sp19___0_65[1] = sp19_p_sum12_96[0]  # type = ?
        sp19___0_66[1] = sp19_p_sum12_96[0]  # type = ?
        sp19___0_56[1] = sp19_p_sum12_96[1]  # type = ?
        sp19___0_56[1] = sp19_p_sum12_96[2]  # type = ?
        sp19___0_57[1] = sp19_p_sum12_96[2]  # type = ?
        sp19___0_56[1] = sp19_p_sum12_96[3]  # type = ?
        sp19___0_57[1] = sp19_p_sum12_96[3]  # type = ?
        sp19___0_59[1] = sp19_p_sum12_96[3]  # type = ?
        sp19___0_56[1] = sp19_p_sum12_96[4]  # type = ?
        sp19___0_57[1] = sp19_p_sum12_96[4]  # type = ?
        sp19___0_59[1] = sp19_p_sum12_96[4]  # type = ?
        sp19___0_61[1] = sp19_p_sum12_96[4]  # type = ?
        sp19___0_56[1] = sp19_p_sum12_96[5]  # type = ?
        sp19___0_57[1] = sp19_p_sum12_96[5]  # type = ?
        sp19___0_59[1] = sp19_p_sum12_96[5]  # type = ?
        sp19___0_61[1] = sp19_p_sum12_96[5]  # type = ?
        sp19___0_62[1] = sp19_p_sum12_96[5]  # type = ?
        sp19___0_56[1] = sp19_p_sum12_96[6]  # type = ?
        sp19___0_57[1] = sp19_p_sum12_96[6]  # type = ?
        sp19___0_59[1] = sp19_p_sum12_96[6]  # type = ?
        sp19___0_61[1] = sp19_p_sum12_96[6]  # type = ?
        sp19___0_62[1] = sp19_p_sum12_96[6]  # type = ?
        sp19___0_63[1] = sp19_p_sum12_96[6]  # type = ?
        sp19___0_56[1] = sp19_p_sum12_96[7]  # type = ?
        sp19___0_57[1] = sp19_p_sum12_96[7]  # type = ?
        sp19___0_59[1] = sp19_p_sum12_96[7]  # type = ?
        sp19___0_61[1] = sp19_p_sum12_96[7]  # type = ?
        sp19___0_62[1] = sp19_p_sum12_96[7]  # type = ?
        sp19___0_63[1] = sp19_p_sum12_96[7]  # type = ?
        sp19___0_64[1] = sp19_p_sum12_96[7]  # type = ?
        sp19___0_56[1] = sp19_p_sum12_96[8]  # type = ?
        sp19___0_57[1] = sp19_p_sum12_96[8]  # type = ?
        sp19___0_59[1] = sp19_p_sum12_96[8]  # type = ?
        sp19___0_61[1] = sp19_p_sum12_96[8]  # type = ?
        sp19___0_62[1] = sp19_p_sum12_96[8]  # type = ?
        sp19___0_63[1] = sp19_p_sum12_96[8]  # type = ?
        sp19___0_64[1] = sp19_p_sum12_96[8]  # type = ?
        sp19___0_65[1] = sp19_p_sum12_96[8]  # type = ?
        sp19_pack_0_0_0_25 = newobj(text='pack 0 0 0')
        sp19_message_246[0] = sp19_pack_0_0_0_25[0]  # type = ?
        sp19_print_left_239 = newobj(text='print left')
        sp19_print_right_28 = newobj(text='print right')
        sp19_receive_142 = receive(text='r cardinal')
        sp19_int_15[1] = sp19_receive_142[0]  # type = ?
        sp19_receive_76 = receive(text='r PcRange')
        sp19_number_85[0] = sp19_receive_76[0]  # type = ?
        sp19_receive_97 = receive(text='r out_T')
        sp19_button_27[0] = sp19_receive_97[0]  # type = ?
        sp19_sel_1_2_3_4_5_6_7_8_9_189 = newobj(text='sel 1 2 3 4 5 6 7 8 9')
        sp19_button_191[0] = sp19_sel_1_2_3_4_5_6_7_8_9_189[0]  # type = bang
        sp19_button_193[0] = sp19_sel_1_2_3_4_5_6_7_8_9_189[1]  # type = bang
        sp19_button_195[0] = sp19_sel_1_2_3_4_5_6_7_8_9_189[2]  # type = bang
        sp19_button_197[0] = sp19_sel_1_2_3_4_5_6_7_8_9_189[3]  # type = bang
        sp19_button_94[0] = sp19_sel_1_2_3_4_5_6_7_8_9_189[4]  # type = bang
        sp19_button_93[0] = sp19_sel_1_2_3_4_5_6_7_8_9_189[5]  # type = bang
        sp19_button_92[0] = sp19_sel_1_2_3_4_5_6_7_8_9_189[6]  # type = bang
        sp19_button_91[0] = sp19_sel_1_2_3_4_5_6_7_8_9_189[7]  # type = bang
        sp19_button_102[0] = sp19_sel_1_2_3_4_5_6_7_8_9_189[8]  # type = bang
        sp19_sel_i1_80 = newobj(text='sel $i1')
        sp19_button_27[0] = sp19_sel_i1_80[0]  # type = bang
        sp19_slider_119 = slider(parameter_enable=0)
        sp19_t_b_i_55 = newobj(text='t b i')
        sp19_int_15[0] = sp19_t_b_i_55[0]  # type = bang
        sp19_button_52[0] = sp19_t_b_i_55[0]  # type = bang
        sp19_int_67[0] = sp19_t_b_i_55[0]  # type = bang
        sp19_int_68[0] = sp19_t_b_i_55[0]  # type = bang
        sp19_int_69[0] = sp19_t_b_i_55[0]  # type = bang
        sp19_int_70[0] = sp19_t_b_i_55[0]  # type = bang
        sp19_int_71[0] = sp19_t_b_i_55[0]  # type = bang
        sp19_int_72[0] = sp19_t_b_i_55[0]  # type = bang
        sp19_int_73[0] = sp19_t_b_i_55[0]  # type = bang
        sp19_int_74[0] = sp19_t_b_i_55[0]  # type = bang
        sp19_int_75[0] = sp19_t_b_i_55[0]  # type = bang
        sp19_number_84[0] = sp19_t_b_i_55[1]  # type = int
        sp19_uzi_0_53 = newobj(text='Uzi 0')
        sp19_counter_1_9_130[0] = sp19_uzi_0_53[0]  # type = bang
        return sp19_outlet_34, sp19_outlet_32

    #----------------------------------------------------------------------
    def subpatcher_0(sp19_sp0_inlet_137, sp19_sp0_inlet_139, sp19_sp0_inlet_143, sp19_sp0_inlet_144, sp19_sp0_inlet_146, sp19_sp0_inlet_148, sp19_sp0_inlet_149, sp19_sp0_inlet_150, sp19_sp0_inlet_151):
        sp19_sp0_outlet_153 = outlet(comment='', index=2)
        sp19_sp0_p_limitt_126 = _subpatcher(subpatcher_id=7, text='p LimitT')
        sp19_sp0_outlet_153[0] = sp19_sp0_p_limitt_126[0]  # type = int
        sp19_sp0_inlet_139 = inlet(comment='', index=2)
        sp19_sp0_p_limitt_126[0] = sp19_sp0_inlet_139[0]  # type = int
        sp19_sp0_outlet_159 = outlet(comment='', index=8)
        sp19_sp0_p_limitt_133 = _subpatcher(subpatcher_id=1, text='p LimitT')
        sp19_sp0_outlet_159[0] = sp19_sp0_p_limitt_133[0]  # type = int
        sp19_sp0_outlet_154 = outlet(comment='', index=3)
        sp19_sp0_p_limitt_127 = _subpatcher(subpatcher_id=6, text='p LimitT')
        sp19_sp0_outlet_154[0] = sp19_sp0_p_limitt_127[0]  # type = int
        sp19_sp0_inlet_143 = inlet(comment='', index=3)
        sp19_sp0_p_limitt_127[0] = sp19_sp0_inlet_143[0]  # type = int
        sp19_sp0_outlet_155 = outlet(comment='', index=4)
        sp19_sp0_p_limitt_128 = _subpatcher(subpatcher_id=5, text='p LimitT')
        sp19_sp0_outlet_155[0] = sp19_sp0_p_limitt_128[0]  # type = int
        sp19_sp0_inlet_144 = inlet(comment='', index=4)
        sp19_sp0_p_limitt_128[0] = sp19_sp0_inlet_144[0]  # type = int
        sp19_sp0_outlet_160 = outlet(comment='', index=9)
        sp19_sp0_p_limitt_134 = _subpatcher(subpatcher_id=0, text='p LimitT')
        sp19_sp0_outlet_160[0] = sp19_sp0_p_limitt_134[0]  # type = int
        sp19_sp0_inlet_151 = inlet(comment='', index=9)
        sp19_sp0_p_limitt_134[0] = sp19_sp0_inlet_151[0]  # type = int
        sp19_sp0_outlet_158 = outlet(comment='', index=7)
        sp19_sp0_p_limitt_132 = _subpatcher(subpatcher_id=2, text='p LimitT')
        sp19_sp0_outlet_158[0] = sp19_sp0_p_limitt_132[0]  # type = int
        sp19_sp0_inlet_149 = inlet(comment='', index=7)
        sp19_sp0_p_limitt_132[0] = sp19_sp0_inlet_149[0]  # type = int
        sp19_sp0_outlet_157 = outlet(comment='', index=6)
        sp19_sp0_p_limitt_131 = _subpatcher(subpatcher_id=3, text='p LimitT')
        sp19_sp0_outlet_157[0] = sp19_sp0_p_limitt_131[0]  # type = int
        sp19_sp0_inlet_148 = inlet(comment='', index=6)
        sp19_sp0_p_limitt_131[0] = sp19_sp0_inlet_148[0]  # type = int
        sp19_sp0_inlet_150 = inlet(comment='', index=8)
        sp19_sp0_p_limitt_133[0] = sp19_sp0_inlet_150[0]  # type = int
        sp19_sp0_outlet_152 = outlet(comment='', index=1)
        sp19_sp0_p_limitt_125 = _subpatcher(subpatcher_id=8, text='p LimitT')
        sp19_sp0_outlet_152[0] = sp19_sp0_p_limitt_125[0]  # type = ?
        sp19_sp0_outlet_156 = outlet(comment='', index=5)
        sp19_sp0_p_limitt_129 = _subpatcher(subpatcher_id=4, text='p LimitT')
        sp19_sp0_outlet_156[0] = sp19_sp0_p_limitt_129[0]  # type = int
        sp19_sp0_inlet_137 = inlet(comment='', index=1)
        sp19_sp0_p_limitt_125[0] = sp19_sp0_inlet_137[0]  # type = int
        sp19_sp0_inlet_146 = inlet(comment='', index=5)
        sp19_sp0_p_limitt_129[0] = sp19_sp0_inlet_146[0]  # type = int
        return sp19_sp0_outlet_152, sp19_sp0_outlet_153, sp19_sp0_outlet_154, sp19_sp0_outlet_155, sp19_sp0_outlet_156, sp19_sp0_outlet_157, sp19_sp0_outlet_158, sp19_sp0_outlet_159, sp19_sp0_outlet_160

    #----------------------------------------------------------------------
    def subpatcher_0(sp19_sp0_sp0_inlet_122):
        sp19_sp0_sp0_outlet_123 = outlet(comment='', index=1)
        sp19_sp0_sp0___0_120 = newobj(text='- 0')
        sp19_sp0_sp0_outlet_123[0] = sp19_sp0_sp0___0_120[0]  # type = int
        sp19_sp0_sp0_number_107 = number(parameter_enable=0)
        sp19_sp0_sp0___0_120[1] = sp19_sp0_sp0_number_107[0]  # type = ?
        sp19_sp0_sp0_if_i1__111_then_24_else_0_104 = newobj(text='if $i1 > 111 then 24 else 0')
        sp19_sp0_sp0_number_107[0] = sp19_sp0_sp0_if_i1__111_then_24_else_0_104[0]  # type = ?
        sp19_sp0_sp0_inlet_122 = inlet(comment='', index=1)
        sp19_sp0_sp0_if_i1__111_then_24_else_0_104[0] = sp19_sp0_sp0_inlet_122[0]  # type = int
        sp19_sp0_sp0___0_120[0] = sp19_sp0_sp0_inlet_122[0]  # type = int
        return sp19_sp0_sp0_outlet_123

    #----------------------------------------------------------------------
    def subpatcher_1(sp19_sp0_sp1_inlet_122):
        sp19_sp0_sp1_outlet_123 = outlet(comment='', index=1)
        sp19_sp0_sp1___0_120 = newobj(text='- 0')
        sp19_sp0_sp1_outlet_123[0] = sp19_sp0_sp1___0_120[0]  # type = int
        sp19_sp0_sp1_number_107 = number(parameter_enable=0)
        sp19_sp0_sp1___0_120[1] = sp19_sp0_sp1_number_107[0]  # type = ?
        sp19_sp0_sp1_if_i1__110_then_24_else_0_104 = newobj(text='if $i1 > 110 then 24 else 0')
        sp19_sp0_sp1_number_107[0] = sp19_sp0_sp1_if_i1__110_then_24_else_0_104[0]  # type = ?
        sp19_sp0_sp1_inlet_122 = inlet(comment='', index=1)
        sp19_sp0_sp1_if_i1__110_then_24_else_0_104[0] = sp19_sp0_sp1_inlet_122[0]  # type = int
        sp19_sp0_sp1___0_120[0] = sp19_sp0_sp1_inlet_122[0]  # type = int
        return sp19_sp0_sp1_outlet_123

    #----------------------------------------------------------------------
    def subpatcher_2(sp19_sp0_sp2_inlet_122):
        sp19_sp0_sp2_outlet_123 = outlet(comment='', index=1)
        sp19_sp0_sp2___0_120 = newobj(text='- 0')
        sp19_sp0_sp2_outlet_123[0] = sp19_sp0_sp2___0_120[0]  # type = int
        sp19_sp0_sp2_number_107 = number(parameter_enable=0)
        sp19_sp0_sp2___0_120[1] = sp19_sp0_sp2_number_107[0]  # type = ?
        sp19_sp0_sp2_if_i1__109_then_24_else_0_104 = newobj(text='if $i1 > 109 then 24 else 0')
        sp19_sp0_sp2_number_107[0] = sp19_sp0_sp2_if_i1__109_then_24_else_0_104[0]  # type = ?
        sp19_sp0_sp2_inlet_122 = inlet(comment='', index=1)
        sp19_sp0_sp2_if_i1__109_then_24_else_0_104[0] = sp19_sp0_sp2_inlet_122[0]  # type = int
        sp19_sp0_sp2___0_120[0] = sp19_sp0_sp2_inlet_122[0]  # type = int
        return sp19_sp0_sp2_outlet_123

    #----------------------------------------------------------------------
    def subpatcher_3(sp19_sp0_sp3_inlet_122):
        sp19_sp0_sp3_outlet_123 = outlet(comment='', index=1)
        sp19_sp0_sp3___0_120 = newobj(text='- 0')
        sp19_sp0_sp3_outlet_123[0] = sp19_sp0_sp3___0_120[0]  # type = int
        sp19_sp0_sp3_number_107 = number(parameter_enable=0)
        sp19_sp0_sp3___0_120[1] = sp19_sp0_sp3_number_107[0]  # type = ?
        sp19_sp0_sp3_if_i1__108_then_24_else_0_104 = newobj(text='if $i1 > 108 then 24 else 0')
        sp19_sp0_sp3_number_107[0] = sp19_sp0_sp3_if_i1__108_then_24_else_0_104[0]  # type = ?
        sp19_sp0_sp3_inlet_122 = inlet(comment='', index=1)
        sp19_sp0_sp3_if_i1__108_then_24_else_0_104[0] = sp19_sp0_sp3_inlet_122[0]  # type = int
        sp19_sp0_sp3___0_120[0] = sp19_sp0_sp3_inlet_122[0]  # type = int
        return sp19_sp0_sp3_outlet_123

    #----------------------------------------------------------------------
    def subpatcher_4(sp19_sp0_sp4_inlet_122):
        sp19_sp0_sp4_outlet_123 = outlet(comment='', index=1)
        sp19_sp0_sp4___0_120 = newobj(text='- 0')
        sp19_sp0_sp4_outlet_123[0] = sp19_sp0_sp4___0_120[0]  # type = int
        sp19_sp0_sp4_number_107 = number(parameter_enable=0)
        sp19_sp0_sp4___0_120[1] = sp19_sp0_sp4_number_107[0]  # type = ?
        sp19_sp0_sp4_if_i1__107_then_24_else_0_104 = newobj(text='if $i1 > 107 then 24 else 0')
        sp19_sp0_sp4_number_107[0] = sp19_sp0_sp4_if_i1__107_then_24_else_0_104[0]  # type = ?
        sp19_sp0_sp4_inlet_122 = inlet(comment='', index=1)
        sp19_sp0_sp4_if_i1__107_then_24_else_0_104[0] = sp19_sp0_sp4_inlet_122[0]  # type = int
        sp19_sp0_sp4___0_120[0] = sp19_sp0_sp4_inlet_122[0]  # type = int
        return sp19_sp0_sp4_outlet_123

    #----------------------------------------------------------------------
    def subpatcher_5(sp19_sp0_sp5_inlet_122):
        sp19_sp0_sp5_outlet_123 = outlet(comment='', index=1)
        sp19_sp0_sp5___0_120 = newobj(text='- 0')
        sp19_sp0_sp5_outlet_123[0] = sp19_sp0_sp5___0_120[0]  # type = int
        sp19_sp0_sp5_number_107 = number(parameter_enable=0)
        sp19_sp0_sp5___0_120[1] = sp19_sp0_sp5_number_107[0]  # type = ?
        sp19_sp0_sp5_if_i1__106_then_24_else_0_104 = newobj(text='if $i1 > 106 then 24 else 0')
        sp19_sp0_sp5_number_107[0] = sp19_sp0_sp5_if_i1__106_then_24_else_0_104[0]  # type = ?
        sp19_sp0_sp5_inlet_122 = inlet(comment='', index=1)
        sp19_sp0_sp5_if_i1__106_then_24_else_0_104[0] = sp19_sp0_sp5_inlet_122[0]  # type = int
        sp19_sp0_sp5___0_120[0] = sp19_sp0_sp5_inlet_122[0]  # type = int
        return sp19_sp0_sp5_outlet_123

    #----------------------------------------------------------------------
    def subpatcher_6(sp19_sp0_sp6_inlet_122):
        sp19_sp0_sp6_outlet_123 = outlet(comment='', index=1)
        sp19_sp0_sp6___0_120 = newobj(text='- 0')
        sp19_sp0_sp6_outlet_123[0] = sp19_sp0_sp6___0_120[0]  # type = int
        sp19_sp0_sp6_number_107 = number(parameter_enable=0)
        sp19_sp0_sp6___0_120[1] = sp19_sp0_sp6_number_107[0]  # type = ?
        sp19_sp0_sp6_if_i1__105_then_24_else_0_104 = newobj(text='if $i1 > 105 then 24 else 0')
        sp19_sp0_sp6_number_107[0] = sp19_sp0_sp6_if_i1__105_then_24_else_0_104[0]  # type = ?
        sp19_sp0_sp6_inlet_122 = inlet(comment='', index=1)
        sp19_sp0_sp6_if_i1__105_then_24_else_0_104[0] = sp19_sp0_sp6_inlet_122[0]  # type = int
        sp19_sp0_sp6___0_120[0] = sp19_sp0_sp6_inlet_122[0]  # type = int
        return sp19_sp0_sp6_outlet_123

    #----------------------------------------------------------------------
    def subpatcher_7(sp19_sp0_sp7_inlet_122):
        sp19_sp0_sp7_outlet_123 = outlet(comment='', index=1)
        sp19_sp0_sp7___0_120 = newobj(text='- 0')
        sp19_sp0_sp7_outlet_123[0] = sp19_sp0_sp7___0_120[0]  # type = int
        sp19_sp0_sp7_number_107 = number(parameter_enable=0)
        sp19_sp0_sp7___0_120[1] = sp19_sp0_sp7_number_107[0]  # type = ?
        sp19_sp0_sp7_if_i1__104_then_24_else_0_104 = newobj(text='if $i1 > 104 then 24 else 0')
        sp19_sp0_sp7_number_107[0] = sp19_sp0_sp7_if_i1__104_then_24_else_0_104[0]  # type = ?
        sp19_sp0_sp7_inlet_122 = inlet(comment='', index=1)
        sp19_sp0_sp7_if_i1__104_then_24_else_0_104[0] = sp19_sp0_sp7_inlet_122[0]  # type = int
        sp19_sp0_sp7___0_120[0] = sp19_sp0_sp7_inlet_122[0]  # type = int
        return sp19_sp0_sp7_outlet_123

    #----------------------------------------------------------------------
    def subpatcher_8(sp19_sp0_sp8_inlet_122):
        sp19_sp0_sp8_outlet_123 = outlet(comment='', index=1)
        sp19_sp0_sp8_number_6 = number(parameter_enable=0)
        sp19_sp0_sp8_outlet_123[0] = sp19_sp0_sp8_number_6[0]  # type = ?
        sp19_sp0_sp8___0_120 = newobj(text='- 0')
        sp19_sp0_sp8_number_6[0] = sp19_sp0_sp8___0_120[0]  # type = int
        sp19_sp0_sp8_number_107 = number(parameter_enable=0)
        sp19_sp0_sp8___0_120[1] = sp19_sp0_sp8_number_107[0]  # type = ?
        sp19_sp0_sp8_if_i1__103_then_24_else_0_104 = newobj(text='if $i1 > 103 then 24 else 0')
        sp19_sp0_sp8_number_107[0] = sp19_sp0_sp8_if_i1__103_then_24_else_0_104[0]  # type = ?
        sp19_sp0_sp8_inlet_122 = inlet(comment='', index=1)
        sp19_sp0_sp8_if_i1__103_then_24_else_0_104[0] = sp19_sp0_sp8_inlet_122[0]  # type = int
        sp19_sp0_sp8___0_120[0] = sp19_sp0_sp8_inlet_122[0]  # type = int
        return sp19_sp0_sp8_outlet_123

    #----------------------------------------------------------------------
    def subpatcher_1(sp19_sp1_inlet_74):
        sp19_sp1_outlet_82 = outlet(comment='', index=5)
        sp19_sp1_message_35 = message(text='12')
        sp19_sp1_outlet_82[0] = sp19_sp1_message_35[0]  # type = ?
        sp19_sp1_button_41 = button()
        sp19_sp1_message_35[0] = sp19_sp1_button_41[0]  # type = bang
        sp19_sp1_outlet_83 = outlet(comment='', index=6)
        sp19_sp1_message_34 = message(text='12')
        sp19_sp1_outlet_83[0] = sp19_sp1_message_34[0]  # type = ?
        sp19_sp1_outlet_76 = outlet(comment='', index=1)
        sp19_sp1_message_45 = message(text='0')
        sp19_sp1_outlet_76[0] = sp19_sp1_message_45[0]  # type = ?
        sp19_sp1_outlet_77 = outlet(comment='', index=2)
        sp19_sp1_message_44 = message(text='12')
        sp19_sp1_outlet_77[0] = sp19_sp1_message_44[0]  # type = ?
        sp19_sp1_outlet_84 = outlet(comment='', index=7)
        sp19_sp1_message_32 = message(text='12')
        sp19_sp1_outlet_84[0] = sp19_sp1_message_32[0]  # type = ?
        sp19_sp1_outlet_95 = outlet(comment='', index=9)
        sp19_sp1_message_13 = message(text='12')
        sp19_sp1_outlet_95[0] = sp19_sp1_message_13[0]  # type = ?
        sp19_sp1_button_15 = button()
        sp19_sp1_message_13[0] = sp19_sp1_button_15[0]  # type = bang
        sp19_sp1_button_38 = button()
        sp19_sp1_message_32[0] = sp19_sp1_button_38[0]  # type = bang
        sp19_sp1_outlet_81 = outlet(comment='', index=4)
        sp19_sp1_message_42 = message(text='12')
        sp19_sp1_outlet_81[0] = sp19_sp1_message_42[0]  # type = ?
        sp19_sp1_button_47 = button()
        sp19_sp1_message_42[0] = sp19_sp1_button_47[0]  # type = bang
        sp19_sp1_button_52 = button()
        sp19_sp1_message_45[0] = sp19_sp1_button_52[0]  # type = bang
        sp19_sp1_outlet_78 = outlet(comment='', index=3)
        sp19_sp1_message_43 = message(text='12')
        sp19_sp1_outlet_78[0] = sp19_sp1_message_43[0]  # type = ?
        sp19_sp1_button_48 = button()
        sp19_sp1_message_43[0] = sp19_sp1_button_48[0]  # type = bang
        sp19_sp1_button_40 = button()
        sp19_sp1_message_34[0] = sp19_sp1_button_40[0]  # type = bang
        sp19_sp1_outlet_85 = outlet(comment='', index=8)
        sp19_sp1_message_19 = message(text='12')
        sp19_sp1_outlet_85[0] = sp19_sp1_message_19[0]  # type = ?
        sp19_sp1_button_37 = button()
        sp19_sp1_message_19[0] = sp19_sp1_button_37[0]  # type = bang
        sp19_sp1_button_49 = button()
        sp19_sp1_message_44[0] = sp19_sp1_button_49[0]  # type = bang
        sp19_sp1_sel_0_1_2_3_4_5_6_7_8_55 = newobj(text='sel 0 1 2 3 4 5 6 7 8')
        sp19_sp1_button_52[0] = sp19_sp1_sel_0_1_2_3_4_5_6_7_8_55[0]  # type = bang
        sp19_sp1_button_49[0] = sp19_sp1_sel_0_1_2_3_4_5_6_7_8_55[1]  # type = bang
        sp19_sp1_button_48[0] = sp19_sp1_sel_0_1_2_3_4_5_6_7_8_55[2]  # type = bang
        sp19_sp1_button_47[0] = sp19_sp1_sel_0_1_2_3_4_5_6_7_8_55[3]  # type = bang
        sp19_sp1_button_41[0] = sp19_sp1_sel_0_1_2_3_4_5_6_7_8_55[4]  # type = bang
        sp19_sp1_button_40[0] = sp19_sp1_sel_0_1_2_3_4_5_6_7_8_55[5]  # type = bang
        sp19_sp1_button_38[0] = sp19_sp1_sel_0_1_2_3_4_5_6_7_8_55[6]  # type = bang
        sp19_sp1_button_37[0] = sp19_sp1_sel_0_1_2_3_4_5_6_7_8_55[7]  # type = bang
        sp19_sp1_button_15[0] = sp19_sp1_sel_0_1_2_3_4_5_6_7_8_55[8]  # type = bang
        sp19_sp1_number_70 = number(parameter_enable=0)
        sp19_sp1_sel_0_1_2_3_4_5_6_7_8_55[0] = sp19_sp1_number_70[0]  # type = ?
        sp19_sp1___1_73 = newobj(text='- 1')
        sp19_sp1_number_70[0] = sp19_sp1___1_73[0]  # type = int
        sp19_sp1_inlet_74 = inlet(comment='', index=1)
        sp19_sp1___1_73[0] = sp19_sp1_inlet_74[0]  # type = ?
        return sp19_sp1_outlet_76, sp19_sp1_outlet_77, sp19_sp1_outlet_78, sp19_sp1_outlet_81, sp19_sp1_outlet_82, sp19_sp1_outlet_83, sp19_sp1_outlet_84, sp19_sp1_outlet_85, sp19_sp1_outlet_95

    #----------------------------------------------------------------------
    def subpatcher_20(sp20_inlet_11, sp20_inlet_1, sp20_inlet_3, sp20_inlet_5, sp20_inlet_8, sp20_inlet_50, sp20_inlet_51, sp20_inlet_58, sp20_inlet_60):
        sp20___0_170 = newobj(text='+ 0')
        sp20_pack_0_0_0_25[0] = sp20___0_170[0]  # type = int
        sp20___1_114 = newobj(text='- 1')
        sp20_counter_0_8_113[4] = sp20___1_114[0]  # type = int
        sp20___1_36 = newobj(text='+ 1')
        sp20_counter_1_9_130[2] = sp20___1_36[0]  # type = int
        sp20_accum_1_136 = newobj(text='accum 1')
        sp20_number_140[0] = sp20_accum_1_136[0]  # type = int
        sp20_button_10 = button()
        sp20_uzi_0_53[0] = sp20_button_10[0]  # type = bang
        sp20_button_110 = button()
        sp20_counter_0_10_124[0] = sp20_button_110[0]  # type = bang
        sp20_button_112 = button()
        sp20_counter_0_8_113[0] = sp20_button_112[0]  # type = bang
        sp20_button_12 = button()
        sp20_accum_1_136[0] = sp20_button_12[0]  # type = bang
        sp20_message_138[0] = sp20_button_12[0]  # type = bang
        sp20_button_145 = button()
        sp20_button_10[0] = sp20_button_145[0]  # type = bang
        sp20_button_24 = button()
        sp20_message_135[0] = sp20_button_24[0]  # type = bang
        sp20_message_147[0] = sp20_button_24[0]  # type = bang
        sp20_message_16[0] = sp20_button_24[0]  # type = bang
        sp20_message_18[0] = sp20_button_24[0]  # type = bang
        sp20_button_27 = button()
        sp20_jitmatrix_1_char_4_4_30[0] = sp20_button_27[0]  # type = bang
# (comment) Permutación cirular Matriz Forma Prima 
# (comment) 3.1.a.
# (comment) Set/Get contents
# (comment) value
# (comment) y position
# (comment) Create a matrix
# (comment) x position
# (comment) 3.1.b.
# (comment) 3.1.c.
# (comment) 3.1.e.
# (comment) 3.1.d.
# (comment) output
# (comment) Matrix
# (comment) Cardinal Number PcSet
        sp20_counter_0_10_124 = newobj(text='counter 0 10')
        sp20_number_121[0] = sp20_counter_0_10_124[0]  # type = int
        sp20_counter_0_8_113 = newobj(text='counter 0 8')
        sp20_number_245[0] = sp20_counter_0_8_113[0]  # type = int
        sp20_delay_10_39[0] = sp20_counter_0_8_113[3]  # type = int
        sp20_counter_1_9_130 = newobj(text='counter 1 9')
        sp20_number_188[0] = sp20_counter_1_9_130[0]  # type = int
        sp20_delay_10_39 = newobj(text='delay 10')
        sp20_button_110[0] = sp20_delay_10_39[0]  # type = bang
        sp20_if_i1__0_then_bang_29 = newobj(text='if $i1 != 0 then bang')
        sp20_button_112[0] = sp20_if_i1__0_then_bang_29[0]  # type = ?
        sp20_if_i1__0_then_bang_54 = newobj(text='if $i1 != 0 then bang')
        sp20_button_12[0] = sp20_if_i1__0_then_bang_54[0]  # type = ?
        sp20_if_i1__i2_then_bang_141 = newobj(text='if $i1 <= $i2 then bang')
        sp20_button_145[0] = sp20_if_i1__i2_then_bang_141[0]  # type = ?
        sp20_inlet_1 = inlet(comment='', index=5)
        sp20_number_23[0] = sp20_inlet_1[0]  # type = ?
        sp20_inlet_11 = inlet(comment='', index=9)
        sp20_number_26[0] = sp20_inlet_11[0]  # type = ?
        sp20_inlet_3 = inlet(comment='', index=6)
        sp20_number_22[0] = sp20_inlet_3[0]  # type = ?
        sp20_inlet_5 = inlet(comment='', index=7)
        sp20_number_21[0] = sp20_inlet_5[0]  # type = ?
        sp20_inlet_50 = inlet(comment='', index=1)
        sp20_number_2[0] = sp20_inlet_50[0]  # type = ?
        sp20_inlet_51 = inlet(comment='', index=2)
        sp20_number_4[0] = sp20_inlet_51[0]  # type = ?
        sp20_inlet_58 = inlet(comment='', index=3)
        sp20_number_6[0] = sp20_inlet_58[0]  # type = ?
        sp20_inlet_60 = inlet(comment='', index=4)
        sp20_number_9[0] = sp20_inlet_60[0]  # type = ?
        sp20_inlet_8 = inlet(comment='', index=8)
        sp20_number_20[0] = sp20_inlet_8[0]  # type = ?
        sp20_jit.cellblock_33 = jit.cellblock(cols=7, colwidth=25, rows=7)
        sp20_jitmatrix_1_char_4_4_30 = newobj(text='jit.matrix 1 char 4 4')
        sp20_jit.cellblock_33[0] = sp20_jitmatrix_1_char_4_4_30[0]  # type = jit_matrix
        sp20_outlet_68[0] = sp20_jitmatrix_1_char_4_4_30[0]  # type = jit_matrix
        sp20_jitprint_240 = newobj(text='jit.print')
        sp20_message_135 = message(text='set 0')
        sp20_accum_1_136[0] = sp20_message_135[0]  # type = ?
        sp20_message_138 = message(text='1')
        sp20_accum_1_136[1] = sp20_message_138[0]  # type = ?
        sp20_message_147 = message(text='0')
        sp20_counter_0_8_113[3] = sp20_message_147[0]  # type = ?
        sp20_counter_0_10_124[3] = sp20_message_147[0]  # type = ?
        sp20_counter_1_9_130[3] = sp20_message_147[0]  # type = ?
        sp20_message_16 = message(text='1')
        sp20_counter_1_9_130[2] = sp20_message_16[0]  # type = ?
        sp20_message_18 = message(text='clear')
        sp20_jitmatrix_1_char_4_4_30[0] = sp20_message_18[0]  # type = ?
        sp20_message_246 = message(text='setcell $2 $3 val $1')
        sp20_jitmatrix_1_char_4_4_30[0] = sp20_message_246[0]  # type = ?
        sp20_message_86 = message(text='dim $1 $1')
        sp20_jitmatrix_1_char_4_4_30[0] = sp20_message_86[0]  # type = ?
        sp20_number_121 = number(parameter_enable=0)
        sp20_pack_0_0_0_25[2] = sp20_number_121[0]  # type = ?
        sp20___1_36[0] = sp20_number_121[0]  # type = ?
        sp20_if_i1__0_then_bang_54[0] = sp20_number_121[0]  # type = ?
        sp20_p_matrixvalues_77[10] = sp20_number_121[0]  # type = ?
        sp20_sel_i1_80[0] = sp20_number_121[0]  # type = ?
        sp20_number_14 = number(parameter_enable=0)
        sp20_counter_0_10_124[4] = sp20_number_14[0]  # type = ?
        sp20_if_i1__i2_then_bang_141[1] = sp20_number_14[0]  # type = ?
        sp20_number_140 = number(parameter_enable=0)
        sp20_if_i1__i2_then_bang_141[0] = sp20_number_140[0]  # type = ?
        sp20_number_17 = number(parameter_enable=0)
        sp20___1_114[0] = sp20_number_17[0]  # type = ?
        sp20_counter_1_9_130[4] = sp20_number_17[0]  # type = ?
        sp20_number_14[0] = sp20_number_17[0]  # type = ?
        sp20_button_24[0] = sp20_number_17[0]  # type = ?
        sp20_uzi_0_53[1] = sp20_number_17[0]  # type = ?
        sp20_outlet_69[0] = sp20_number_17[0]  # type = ?
        sp20_sel_i1_80[1] = sp20_number_17[0]  # type = ?
        sp20_message_86[0] = sp20_number_17[0]  # type = ?
        sp20_number_188 = number(parameter_enable=0)
        sp20_if_i1__0_then_bang_29[0] = sp20_number_188[0]  # type = ?
        sp20_p_matrixvalues_77[0] = sp20_number_188[0]  # type = ?
        sp20_number_2 = number(parameter_enable=0)
        sp20_button_10[0] = sp20_number_2[0]  # type = ?
        sp20_p_matrixvalues_77[1] = sp20_number_2[0]  # type = ?
        sp20_number_20 = number(parameter_enable=0)
        sp20_p_matrixvalues_77[8] = sp20_number_20[0]  # type = ?
        sp20_number_21 = number(parameter_enable=0)
        sp20_p_matrixvalues_77[7] = sp20_number_21[0]  # type = ?
        sp20_number_22 = number(parameter_enable=0)
        sp20_p_matrixvalues_77[6] = sp20_number_22[0]  # type = ?
        sp20_number_23 = number(parameter_enable=0)
        sp20_p_matrixvalues_77[5] = sp20_number_23[0]  # type = ?
        sp20_number_243 = number(mousefilter=1, parameter_enable=0, triscale=0.9)
        sp20___0_170[0] = sp20_number_243[0]  # type = ?
        sp20_number_245 = number(maximum=15, minimum=0, mousefilter=1, parameter_enable=0, triscale=0.9)
        sp20_pack_0_0_0_25[1] = sp20_number_245[0]  # type = ?
        sp20_number_26 = number(parameter_enable=0)
        sp20_p_matrixvalues_77[9] = sp20_number_26[0]  # type = ?
        sp20_number_4 = number(parameter_enable=0)
        sp20_p_matrixvalues_77[2] = sp20_number_4[0]  # type = ?
        sp20_number_6 = number(parameter_enable=0)
        sp20_p_matrixvalues_77[3] = sp20_number_6[0]  # type = ?
        sp20_number_9 = number(parameter_enable=0)
        sp20_p_matrixvalues_77[4] = sp20_number_9[0]  # type = ?
        sp20_outlet_68 = outlet(comment='', index=2)
        sp20_outlet_69 = outlet(comment='', index=1)
        sp20_p_matrixvalues_77 = _subpatcher(subpatcher_id=0, text='p MatrixValues')
        sp20_number_243[0] = sp20_p_matrixvalues_77[0]  # type = int
        sp20_pack_0_0_0_25 = newobj(text='pack 0 0 0')
        sp20_message_246[0] = sp20_pack_0_0_0_25[0]  # type = ?
        sp20_print_left_239 = newobj(text='print left')
        sp20_print_right_28 = newobj(text='print right')
        sp20_receive_142 = receive(text='r cardinal')
        sp20_number_17[0] = sp20_receive_142[0]  # type = ?
        sp20_receive_185 = receive(text='r PcRange')
        sp20___0_170[1] = sp20_receive_185[0]  # type = ?
        sp20_receive_97 = receive(text='r out')
        sp20_button_27[0] = sp20_receive_97[0]  # type = ?
        sp20_sel_i1_80 = newobj(text='sel $i1')
        sp20_button_27[0] = sp20_sel_i1_80[0]  # type = bang
        sp20_uzi_0_53 = newobj(text='Uzi 0')
        sp20_counter_1_9_130[0] = sp20_uzi_0_53[0]  # type = bang
        return sp20_outlet_69, sp20_outlet_68

    #----------------------------------------------------------------------
    def subpatcher_0(sp20_sp0_inlet_46, sp20_sp0_inlet_47, sp20_sp0_inlet_48, sp20_sp0_inlet_52, sp20_sp0_inlet_55, sp20_sp0_inlet_67, sp20_sp0_inlet_70, sp20_sp0_inlet_71, sp20_sp0_inlet_72, sp20_sp0_inlet_73, sp20_sp0_inlet_74):
        sp20_sp0_outlet_76 = outlet(comment='', index=1)
        sp20_sp0___0_59 = newobj(text='+ 0')
        sp20_sp0_outlet_76[0] = sp20_sp0___0_59[0]  # type = int
        sp20_sp0_message_203 = message(text='2')
        sp20_sp0___0_59[0] = sp20_sp0_message_203[0]  # type = ?
        sp20_sp0_button_195 = button()
        sp20_sp0_message_203[0] = sp20_sp0_button_195[0]  # type = bang
        sp20_sp0___0_63 = newobj(text='+ 0')
        sp20_sp0_outlet_76[0] = sp20_sp0___0_63[0]  # type = int
        sp20_sp0_message_89 = message(text='6')
        sp20_sp0___0_63[0] = sp20_sp0_message_89[0]  # type = ?
        sp20_sp0_button_93 = button()
        sp20_sp0_message_89[0] = sp20_sp0_button_93[0]  # type = bang
        sp20_sp0___0_66 = newobj(text='+ 0')
        sp20_sp0_outlet_76[0] = sp20_sp0___0_66[0]  # type = int
        sp20_sp0_message_98 = message(text='9')
        sp20_sp0___0_66[0] = sp20_sp0_message_98[0]  # type = ?
        sp20_sp0_button_102 = button()
        sp20_sp0_message_98[0] = sp20_sp0_button_102[0]  # type = bang
        sp20_sp0___0_61 = newobj(text='+ 0')
        sp20_sp0_outlet_76[0] = sp20_sp0___0_61[0]  # type = int
        sp20_sp0_message_206 = message(text='4')
        sp20_sp0___0_61[0] = sp20_sp0_message_206[0]  # type = ?
        sp20_sp0_button_197 = button()
        sp20_sp0_message_206[0] = sp20_sp0_button_197[0]  # type = bang
        sp20_sp0___0_57 = newobj(text='+ 0')
        sp20_sp0_outlet_76[0] = sp20_sp0___0_57[0]  # type = int
        sp20_sp0_message_201 = message(text='1')
        sp20_sp0___0_57[0] = sp20_sp0_message_201[0]  # type = ?
        sp20_sp0_button_193 = button()
        sp20_sp0_message_201[0] = sp20_sp0_button_193[0]  # type = bang
        sp20_sp0___0_62 = newobj(text='+ 0')
        sp20_sp0_outlet_76[0] = sp20_sp0___0_62[0]  # type = int
        sp20_sp0_message_90 = message(text='5')
        sp20_sp0___0_62[0] = sp20_sp0_message_90[0]  # type = ?
        sp20_sp0_button_94 = button()
        sp20_sp0_message_90[0] = sp20_sp0_button_94[0]  # type = bang
        sp20_sp0___0_65 = newobj(text='+ 0')
        sp20_sp0_outlet_76[0] = sp20_sp0___0_65[0]  # type = int
        sp20_sp0_message_87 = message(text='9')
        sp20_sp0___0_65[0] = sp20_sp0_message_87[0]  # type = ?
        sp20_sp0_button_91 = button()
        sp20_sp0_message_87[0] = sp20_sp0_button_91[0]  # type = bang
        sp20_sp0___0_64 = newobj(text='+ 0')
        sp20_sp0_outlet_76[0] = sp20_sp0___0_64[0]  # type = int
        sp20_sp0_message_88 = message(text='8')
        sp20_sp0___0_64[0] = sp20_sp0_message_88[0]  # type = ?
        sp20_sp0_button_92 = button()
        sp20_sp0_message_88[0] = sp20_sp0_button_92[0]  # type = bang
        sp20_sp0___0_56 = newobj(text='+ 0')
        sp20_sp0_outlet_76[0] = sp20_sp0___0_56[0]  # type = int
        sp20_sp0_message_199 = message(text='0')
        sp20_sp0___0_56[0] = sp20_sp0_message_199[0]  # type = ?
        sp20_sp0_button_191 = button()
        sp20_sp0_message_199[0] = sp20_sp0_button_191[0]  # type = bang
        sp20_sp0_sel_1_2_3_4_5_6_7_8_9_189 = newobj(text='sel 1 2 3 4 5 6 7 8 9')
        sp20_sp0_button_191[0] = sp20_sp0_sel_1_2_3_4_5_6_7_8_9_189[0]  # type = bang
        sp20_sp0_button_193[0] = sp20_sp0_sel_1_2_3_4_5_6_7_8_9_189[1]  # type = bang
        sp20_sp0_button_195[0] = sp20_sp0_sel_1_2_3_4_5_6_7_8_9_189[2]  # type = bang
        sp20_sp0_button_197[0] = sp20_sp0_sel_1_2_3_4_5_6_7_8_9_189[3]  # type = bang
        sp20_sp0_button_94[0] = sp20_sp0_sel_1_2_3_4_5_6_7_8_9_189[4]  # type = bang
        sp20_sp0_button_93[0] = sp20_sp0_sel_1_2_3_4_5_6_7_8_9_189[5]  # type = bang
        sp20_sp0_button_92[0] = sp20_sp0_sel_1_2_3_4_5_6_7_8_9_189[6]  # type = bang
        sp20_sp0_button_91[0] = sp20_sp0_sel_1_2_3_4_5_6_7_8_9_189[7]  # type = bang
        sp20_sp0_button_102[0] = sp20_sp0_sel_1_2_3_4_5_6_7_8_9_189[8]  # type = bang
        sp20_sp0_p_sum12_96 = _subpatcher(subpatcher_id=0, text='p Sum12')
        sp20_sp0___0_56[1] = sp20_sp0_p_sum12_96[0]  # type = ?
        sp20_sp0___0_57[1] = sp20_sp0_p_sum12_96[0]  # type = ?
        sp20_sp0___0_59[1] = sp20_sp0_p_sum12_96[0]  # type = ?
        sp20_sp0___0_61[1] = sp20_sp0_p_sum12_96[0]  # type = ?
        sp20_sp0___0_62[1] = sp20_sp0_p_sum12_96[0]  # type = ?
        sp20_sp0___0_63[1] = sp20_sp0_p_sum12_96[0]  # type = ?
        sp20_sp0___0_64[1] = sp20_sp0_p_sum12_96[0]  # type = ?
        sp20_sp0___0_65[1] = sp20_sp0_p_sum12_96[0]  # type = ?
        sp20_sp0___0_66[1] = sp20_sp0_p_sum12_96[0]  # type = ?
        sp20_sp0___0_56[1] = sp20_sp0_p_sum12_96[1]  # type = ?
        sp20_sp0___0_56[1] = sp20_sp0_p_sum12_96[2]  # type = ?
        sp20_sp0___0_57[1] = sp20_sp0_p_sum12_96[2]  # type = ?
        sp20_sp0___0_56[1] = sp20_sp0_p_sum12_96[3]  # type = ?
        sp20_sp0___0_57[1] = sp20_sp0_p_sum12_96[3]  # type = ?
        sp20_sp0___0_59[1] = sp20_sp0_p_sum12_96[3]  # type = ?
        sp20_sp0___0_56[1] = sp20_sp0_p_sum12_96[4]  # type = ?
        sp20_sp0___0_57[1] = sp20_sp0_p_sum12_96[4]  # type = ?
        sp20_sp0___0_59[1] = sp20_sp0_p_sum12_96[4]  # type = ?
        sp20_sp0___0_61[1] = sp20_sp0_p_sum12_96[4]  # type = ?
        sp20_sp0___0_56[1] = sp20_sp0_p_sum12_96[5]  # type = ?
        sp20_sp0___0_57[1] = sp20_sp0_p_sum12_96[5]  # type = ?
        sp20_sp0___0_59[1] = sp20_sp0_p_sum12_96[5]  # type = ?
        sp20_sp0___0_61[1] = sp20_sp0_p_sum12_96[5]  # type = ?
        sp20_sp0___0_62[1] = sp20_sp0_p_sum12_96[5]  # type = ?
        sp20_sp0___0_56[1] = sp20_sp0_p_sum12_96[6]  # type = ?
        sp20_sp0___0_57[1] = sp20_sp0_p_sum12_96[6]  # type = ?
        sp20_sp0___0_59[1] = sp20_sp0_p_sum12_96[6]  # type = ?
        sp20_sp0___0_61[1] = sp20_sp0_p_sum12_96[6]  # type = ?
        sp20_sp0___0_62[1] = sp20_sp0_p_sum12_96[6]  # type = ?
        sp20_sp0___0_63[1] = sp20_sp0_p_sum12_96[6]  # type = ?
        sp20_sp0___0_56[1] = sp20_sp0_p_sum12_96[7]  # type = ?
        sp20_sp0___0_57[1] = sp20_sp0_p_sum12_96[7]  # type = ?
        sp20_sp0___0_59[1] = sp20_sp0_p_sum12_96[7]  # type = ?
        sp20_sp0___0_61[1] = sp20_sp0_p_sum12_96[7]  # type = ?
        sp20_sp0___0_62[1] = sp20_sp0_p_sum12_96[7]  # type = ?
        sp20_sp0___0_63[1] = sp20_sp0_p_sum12_96[7]  # type = ?
        sp20_sp0___0_64[1] = sp20_sp0_p_sum12_96[7]  # type = ?
        sp20_sp0___0_56[1] = sp20_sp0_p_sum12_96[8]  # type = ?
        sp20_sp0___0_57[1] = sp20_sp0_p_sum12_96[8]  # type = ?
        sp20_sp0___0_59[1] = sp20_sp0_p_sum12_96[8]  # type = ?
        sp20_sp0___0_61[1] = sp20_sp0_p_sum12_96[8]  # type = ?
        sp20_sp0___0_62[1] = sp20_sp0_p_sum12_96[8]  # type = ?
        sp20_sp0___0_63[1] = sp20_sp0_p_sum12_96[8]  # type = ?
        sp20_sp0___0_64[1] = sp20_sp0_p_sum12_96[8]  # type = ?
        sp20_sp0___0_65[1] = sp20_sp0_p_sum12_96[8]  # type = ?
        sp20_sp0_inlet_74 = inlet(comment='', index=11)
        sp20_sp0_p_sum12_96[0] = sp20_sp0_inlet_74[0]  # type = ?
        sp20_sp0_inlet_48 = inlet(comment='', index=3)
        sp20_sp0_message_201[1] = sp20_sp0_inlet_48[0]  # type = ?
        sp20_sp0_inlet_55 = inlet(comment='', index=5)
        sp20_sp0_message_206[1] = sp20_sp0_inlet_55[0]  # type = ?
        sp20_sp0_inlet_46 = inlet(comment='', index=1)
        sp20_sp0_sel_1_2_3_4_5_6_7_8_9_189[0] = sp20_sp0_inlet_46[0]  # type = ?
        sp20_sp0_inlet_47 = inlet(comment='', index=2)
        sp20_sp0_message_199[1] = sp20_sp0_inlet_47[0]  # type = ?
        sp20_sp0_inlet_52 = inlet(comment='', index=4)
        sp20_sp0_message_203[1] = sp20_sp0_inlet_52[0]  # type = ?
        sp20_sp0_inlet_73 = inlet(comment='', index=10)
        sp20_sp0_message_98[1] = sp20_sp0_inlet_73[0]  # type = ?
        sp20_sp0_inlet_71 = inlet(comment='', index=8)
        sp20_sp0_message_88[1] = sp20_sp0_inlet_71[0]  # type = ?
        sp20_sp0_inlet_67 = inlet(comment='', index=6)
        sp20_sp0_message_90[1] = sp20_sp0_inlet_67[0]  # type = ?
        sp20_sp0_inlet_72 = inlet(comment='', index=9)
        sp20_sp0_message_87[1] = sp20_sp0_inlet_72[0]  # type = ?
        sp20_sp0_inlet_70 = inlet(comment='', index=7)
        sp20_sp0_message_89[1] = sp20_sp0_inlet_70[0]  # type = ?
# (comment) Sums 12 when ivversion start from 0 to n-1
        return sp20_sp0_outlet_76

    #----------------------------------------------------------------------
    def subpatcher_0(sp20_sp0_sp0_inlet_74):
        sp20_sp0_sp0_outlet_82 = outlet(comment='', index=5)
        sp20_sp0_sp0_message_35 = message(text='12')
        sp20_sp0_sp0_outlet_82[0] = sp20_sp0_sp0_message_35[0]  # type = ?
        sp20_sp0_sp0_button_41 = button()
        sp20_sp0_sp0_message_35[0] = sp20_sp0_sp0_button_41[0]  # type = bang
        sp20_sp0_sp0_outlet_83 = outlet(comment='', index=6)
        sp20_sp0_sp0_message_34 = message(text='12')
        sp20_sp0_sp0_outlet_83[0] = sp20_sp0_sp0_message_34[0]  # type = ?
        sp20_sp0_sp0_outlet_76 = outlet(comment='', index=1)
        sp20_sp0_sp0_message_45 = message(text='0')
        sp20_sp0_sp0_outlet_76[0] = sp20_sp0_sp0_message_45[0]  # type = ?
        sp20_sp0_sp0_outlet_77 = outlet(comment='', index=2)
        sp20_sp0_sp0_message_44 = message(text='12')
        sp20_sp0_sp0_outlet_77[0] = sp20_sp0_sp0_message_44[0]  # type = ?
        sp20_sp0_sp0_outlet_84 = outlet(comment='', index=7)
        sp20_sp0_sp0_message_32 = message(text='12')
        sp20_sp0_sp0_outlet_84[0] = sp20_sp0_sp0_message_32[0]  # type = ?
        sp20_sp0_sp0_outlet_95 = outlet(comment='', index=9)
        sp20_sp0_sp0_message_13 = message(text='12')
        sp20_sp0_sp0_outlet_95[0] = sp20_sp0_sp0_message_13[0]  # type = ?
        sp20_sp0_sp0_button_15 = button()
        sp20_sp0_sp0_message_13[0] = sp20_sp0_sp0_button_15[0]  # type = bang
        sp20_sp0_sp0_button_38 = button()
        sp20_sp0_sp0_message_32[0] = sp20_sp0_sp0_button_38[0]  # type = bang
        sp20_sp0_sp0_outlet_81 = outlet(comment='', index=4)
        sp20_sp0_sp0_message_42 = message(text='12')
        sp20_sp0_sp0_outlet_81[0] = sp20_sp0_sp0_message_42[0]  # type = ?
        sp20_sp0_sp0_button_47 = button()
        sp20_sp0_sp0_message_42[0] = sp20_sp0_sp0_button_47[0]  # type = bang
        sp20_sp0_sp0_button_52 = button()
        sp20_sp0_sp0_message_45[0] = sp20_sp0_sp0_button_52[0]  # type = bang
        sp20_sp0_sp0_outlet_78 = outlet(comment='', index=3)
        sp20_sp0_sp0_message_43 = message(text='12')
        sp20_sp0_sp0_outlet_78[0] = sp20_sp0_sp0_message_43[0]  # type = ?
        sp20_sp0_sp0_button_48 = button()
        sp20_sp0_sp0_message_43[0] = sp20_sp0_sp0_button_48[0]  # type = bang
        sp20_sp0_sp0_button_40 = button()
        sp20_sp0_sp0_message_34[0] = sp20_sp0_sp0_button_40[0]  # type = bang
        sp20_sp0_sp0_outlet_85 = outlet(comment='', index=8)
        sp20_sp0_sp0_message_19 = message(text='12')
        sp20_sp0_sp0_outlet_85[0] = sp20_sp0_sp0_message_19[0]  # type = ?
        sp20_sp0_sp0_button_37 = button()
        sp20_sp0_sp0_message_19[0] = sp20_sp0_sp0_button_37[0]  # type = bang
        sp20_sp0_sp0_button_49 = button()
        sp20_sp0_sp0_message_44[0] = sp20_sp0_sp0_button_49[0]  # type = bang
        sp20_sp0_sp0_sel_0_1_2_3_4_5_6_7_8_55 = newobj(text='sel 0 1 2 3 4 5 6 7 8')
        sp20_sp0_sp0_button_52[0] = sp20_sp0_sp0_sel_0_1_2_3_4_5_6_7_8_55[0]  # type = bang
        sp20_sp0_sp0_button_49[0] = sp20_sp0_sp0_sel_0_1_2_3_4_5_6_7_8_55[1]  # type = bang
        sp20_sp0_sp0_button_48[0] = sp20_sp0_sp0_sel_0_1_2_3_4_5_6_7_8_55[2]  # type = bang
        sp20_sp0_sp0_button_47[0] = sp20_sp0_sp0_sel_0_1_2_3_4_5_6_7_8_55[3]  # type = bang
        sp20_sp0_sp0_button_41[0] = sp20_sp0_sp0_sel_0_1_2_3_4_5_6_7_8_55[4]  # type = bang
        sp20_sp0_sp0_button_40[0] = sp20_sp0_sp0_sel_0_1_2_3_4_5_6_7_8_55[5]  # type = bang
        sp20_sp0_sp0_button_38[0] = sp20_sp0_sp0_sel_0_1_2_3_4_5_6_7_8_55[6]  # type = bang
        sp20_sp0_sp0_button_37[0] = sp20_sp0_sp0_sel_0_1_2_3_4_5_6_7_8_55[7]  # type = bang
        sp20_sp0_sp0_button_15[0] = sp20_sp0_sp0_sel_0_1_2_3_4_5_6_7_8_55[8]  # type = bang
        sp20_sp0_sp0_number_70 = number(parameter_enable=0)
        sp20_sp0_sp0_sel_0_1_2_3_4_5_6_7_8_55[0] = sp20_sp0_sp0_number_70[0]  # type = ?
        sp20_sp0_sp0___1_73 = newobj(text='- 1')
        sp20_sp0_sp0_number_70[0] = sp20_sp0_sp0___1_73[0]  # type = int
        sp20_sp0_sp0_inlet_74 = inlet(comment='', index=1)
        sp20_sp0_sp0___1_73[0] = sp20_sp0_sp0_inlet_74[0]  # type = ?
        return sp20_sp0_sp0_outlet_76, sp20_sp0_sp0_outlet_77, sp20_sp0_sp0_outlet_78, sp20_sp0_sp0_outlet_81, sp20_sp0_sp0_outlet_82, sp20_sp0_sp0_outlet_83, sp20_sp0_sp0_outlet_84, sp20_sp0_sp0_outlet_85, sp20_sp0_sp0_outlet_95

    #----------------------------------------------------------------------
    def subpatcher_21(sp21_inlet_18, sp21_inlet_106):
        sp21_outlet_16 = outlet(comment='', index=8)
        sp21_number_17 = number(parameter_enable=0)
        sp21_outlet_16[0] = sp21_number_17[0]  # type = ?
        sp21_gswitch2_35 = gswitch2(int=1, parameter_enable=0)
        sp21_number_17[0] = sp21_gswitch2_35[0]  # type = ?
        sp21_toggle_36 = toggle(parameter_enable=0)
        sp21_gswitch2_35[0] = sp21_toggle_36[0]  # type = int
        sp21_outlet_8 = outlet(comment='', index=5)
        sp21_number_9 = number(parameter_enable=0)
        sp21_outlet_8[0] = sp21_number_9[0]  # type = ?
        sp21_gswitch2_41 = gswitch2(parameter_enable=0)
        sp21_number_9[0] = sp21_gswitch2_41[0]  # type = ?
        sp21_outlet_14 = outlet(comment='', index=9)
        sp21_number_15 = number(parameter_enable=0)
        sp21_outlet_14[0] = sp21_number_15[0]  # type = ?
        sp21_gswitch2_43 = gswitch2(int=1, parameter_enable=0)
        sp21_number_15[0] = sp21_gswitch2_43[0]  # type = ?
        sp21_toggle_44 = toggle(parameter_enable=0)
        sp21_gswitch2_43[0] = sp21_toggle_44[0]  # type = int
        sp21_outlet_5 = outlet(comment='', index=3)
        sp21_number_6 = number(parameter_enable=0)
        sp21_outlet_5[0] = sp21_number_6[0]  # type = ?
        sp21_gswitch2_31 = gswitch2(parameter_enable=0)
        sp21_number_6[0] = sp21_gswitch2_31[0]  # type = ?
        sp21_outlet_4 = outlet(comment='', index=4)
        sp21_number_7 = number(parameter_enable=0)
        sp21_outlet_4[0] = sp21_number_7[0]  # type = ?
        sp21_gswitch2_33 = gswitch2(parameter_enable=0)
        sp21_number_7[0] = sp21_gswitch2_33[0]  # type = ?
        sp21_outlet_3 = outlet(comment='', index=2)
        sp21_number_24 = number(parameter_enable=0)
        sp21_outlet_3[0] = sp21_number_24[0]  # type = ?
        sp21_gswitch2_29 = gswitch2(parameter_enable=0)
        sp21_number_24[0] = sp21_gswitch2_29[0]  # type = ?
        sp21_outlet_10 = outlet(comment='', index=7)
        sp21_number_11 = number(parameter_enable=0)
        sp21_outlet_10[0] = sp21_number_11[0]  # type = ?
        sp21_gswitch2_37 = gswitch2(parameter_enable=0)
        sp21_number_11[0] = sp21_gswitch2_37[0]  # type = ?
        sp21_outlet_12 = outlet(comment='', index=6)
        sp21_number_13 = number(parameter_enable=0)
        sp21_outlet_12[0] = sp21_number_13[0]  # type = ?
        sp21_gswitch2_39 = gswitch2(parameter_enable=0)
        sp21_number_13[0] = sp21_gswitch2_39[0]  # type = ?
        sp21_outlet_1 = outlet(comment='', index=1)
        sp21_number_25 = number(parameter_enable=0)
        sp21_outlet_1[0] = sp21_number_25[0]  # type = ?
        sp21_gswitch2_22 = gswitch2(parameter_enable=0)
        sp21_number_25[0] = sp21_gswitch2_22[0]  # type = ?
        sp21_unpack_i_i_i_i_i_i_i_i_i_2 = newobj(text='unpack i i i i i i i i i')
        sp21_gswitch2_22[1] = sp21_unpack_i_i_i_i_i_i_i_i_i_2[0]  # type = int
        sp21_gswitch2_29[1] = sp21_unpack_i_i_i_i_i_i_i_i_i_2[1]  # type = int
        sp21_gswitch2_31[1] = sp21_unpack_i_i_i_i_i_i_i_i_i_2[2]  # type = int
        sp21_gswitch2_33[1] = sp21_unpack_i_i_i_i_i_i_i_i_i_2[3]  # type = int
        sp21_gswitch2_41[1] = sp21_unpack_i_i_i_i_i_i_i_i_i_2[4]  # type = int
        sp21_gswitch2_39[1] = sp21_unpack_i_i_i_i_i_i_i_i_i_2[5]  # type = int
        sp21_gswitch2_37[1] = sp21_unpack_i_i_i_i_i_i_i_i_i_2[6]  # type = int
        sp21_gswitch2_35[1] = sp21_unpack_i_i_i_i_i_i_i_i_i_2[7]  # type = int
        sp21_gswitch2_43[1] = sp21_unpack_i_i_i_i_i_i_i_i_i_2[8]  # type = int
        sp21_message_50 = message(text='0 1 2 4 5 6 8')
        sp21_unpack_i_i_i_i_i_i_i_i_i_2[0] = sp21_message_50[0]  # type = ?
        sp21_inlet_106 = inlet(comment='', index=2)
        sp21_message_50[1] = sp21_inlet_106[0]  # type = ?
        sp21_toggle_34 = toggle(parameter_enable=0)
        sp21_gswitch2_33[0] = sp21_toggle_34[0]  # type = int
        sp21_toggle_21 = toggle(parameter_enable=0)
        sp21_gswitch2_22[0] = sp21_toggle_21[0]  # type = int
        sp21_toggle_32 = toggle(parameter_enable=0)
        sp21_gswitch2_31[0] = sp21_toggle_32[0]  # type = int
        sp21_toggle_40 = toggle(parameter_enable=0)
        sp21_gswitch2_39[0] = sp21_toggle_40[0]  # type = int
        sp21_button_72 = button()
        sp21_message_50[0] = sp21_button_72[0]  # type = bang
        sp21_toggle_38 = toggle(parameter_enable=0)
        sp21_gswitch2_37[0] = sp21_toggle_38[0]  # type = int
        sp21_toggle_30 = toggle(parameter_enable=0)
        sp21_gswitch2_29[0] = sp21_toggle_30[0]  # type = int
        sp21_toggle_42 = toggle(parameter_enable=0)
        sp21_gswitch2_41[0] = sp21_toggle_42[0]  # type = int
        sp21_unpack_i_i_i_i_i_i_i_i_i_60 = newobj(text='unpack i i i i i i i i i')
        sp21_toggle_21[0] = sp21_unpack_i_i_i_i_i_i_i_i_i_60[0]  # type = int
        sp21_button_72[0] = sp21_unpack_i_i_i_i_i_i_i_i_i_60[0]  # type = int
        sp21_toggle_30[0] = sp21_unpack_i_i_i_i_i_i_i_i_i_60[1]  # type = int
        sp21_toggle_32[0] = sp21_unpack_i_i_i_i_i_i_i_i_i_60[2]  # type = int
        sp21_toggle_34[0] = sp21_unpack_i_i_i_i_i_i_i_i_i_60[3]  # type = int
        sp21_toggle_42[0] = sp21_unpack_i_i_i_i_i_i_i_i_i_60[4]  # type = int
        sp21_toggle_40[0] = sp21_unpack_i_i_i_i_i_i_i_i_i_60[5]  # type = int
        sp21_toggle_38[0] = sp21_unpack_i_i_i_i_i_i_i_i_i_60[6]  # type = int
        sp21_toggle_36[0] = sp21_unpack_i_i_i_i_i_i_i_i_i_60[7]  # type = int
        sp21_toggle_44[0] = sp21_unpack_i_i_i_i_i_i_i_i_i_60[8]  # type = int
        sp21_message_69 = message(text='0 0 0 0 0 0 0 1 1')
        sp21_coll_class_65 = newobj(text='coll Class')
        sp21_unpack_i_i_i_i_i_i_i_i_i_60[0] = sp21_coll_class_65[0]  # type = ?
        sp21_message_69[1] = sp21_coll_class_65[0]  # type = ?
        sp21_number_70 = number(parameter_enable=0)
        sp21_coll_class_65[0] = sp21_number_70[0]  # type = ?
        sp21_inlet_18 = inlet(comment='', index=1)
        sp21_number_70[0] = sp21_inlet_18[0]  # type = ?
        sp21_message_67 = message(text='open')
        sp21_coll_class_65[0] = sp21_message_67[0]  # type = ?
        return sp21_outlet_14, sp21_outlet_16, sp21_outlet_10, sp21_outlet_12, sp21_outlet_8, sp21_outlet_4, sp21_outlet_5, sp21_outlet_3, sp21_outlet_1
