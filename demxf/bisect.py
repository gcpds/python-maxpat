outlet_4 = outlet(comment='trigg_data<mean')
toggle_78 = toggle(parameter_enable=0)
outlet_4[0] = toggle_78[0]  # type = int
zl_change_76 = newobj(text='zl change')
toggle_78[0] = zl_change_76[0]  # type = ?
__1_75 = newobj(text='< #1')
zl_change_76[0] = __1_75[0]  # type = int
outlet_5 = outlet(comment='trigg_data>=mean')
toggle_80 = toggle(parameter_enable=0)
outlet_5[0] = toggle_80[0]  # type = int
zl_change_52 = newobj(text='zl change')
toggle_80[0] = zl_change_52[0]  # type = ?
__2_62 = newobj(text='>= #2')
zl_change_52[0] = __2_62[0]  # type = int
flonum_8 = flonum(format=6, parameter_enable=0)
__2_62[0] = flonum_8[0]  # type = ?
__1_75[0] = flonum_8[0]  # type = ?
inlet_3 = inlet(comment='datain')
flonum_8[0] = inlet_3[0]  # type = ?
flonum_10 = flonum(format=6, parameter_enable=0)
__2_62[1] = flonum_10[0]  # type = ?
__1_75[1] = flonum_10[0]  # type = ?
inlet_24 = inlet(comment='nth+1_mean')
flonum_10[0] = inlet_24[0]  # type = ?
# (comment) < y >=, comparan los valores debajo y sobre el promedio, el valor de 0.5 es un valor inicial para incialr los calulos, de allí en adelante la comparación se realiza sobre el valor de la media recibida.
# (comment) Dato < Media
# (comment) Dato > Media
# (comment) in1=valor recibido de interfaz, in2= valor promedio calculado
