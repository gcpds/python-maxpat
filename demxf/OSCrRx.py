send_24 = send(text='s HighGamma')
outlet_13 = outlet(comment='')
flonum_176 = flonum(format=6, parameter_enable=0)
flonum_176[0] = outlet_13[0]  # type = ?
bpatcher_15 = _subpatcher(embed=1, lockeddragscroll=0, subpatcher_id=1, viewvisibility=1)
bpatcher_15[0] = flonum_176[0]  # type = ?
bpatcher_15[1] = send_24[0]  # type = ?
gswitch_191 = gswitch(int=1, parameter_enable=0)
gswitch_191[0] = bpatcher_15[0]  # type = ?
message_193 = message()
message_193[0] = gswitch_191[2]  # type = ?
send_22 = send(text='s HighBeta')
outlet_11 = outlet(comment='')
flonum_168 = flonum(format=6, parameter_enable=0)
flonum_168[0] = outlet_11[0]  # type = ?
bpatcher_5 = _subpatcher(embed=1, lockeddragscroll=0, subpatcher_id=3, viewvisibility=1)
bpatcher_5[0] = flonum_168[0]  # type = ?
bpatcher_5[1] = send_22[0]  # type = ?
send_18 = send(text='s Theta')
outlet_7 = outlet(comment='')
flonum_141 = flonum(format=6, parameter_enable=0)
flonum_141[0] = outlet_7[0]  # type = ?
bpatcher_1 = _subpatcher(embed=1, lockeddragscroll=0, subpatcher_id=7, viewvisibility=1)
bpatcher_1[0] = flonum_141[0]  # type = ?
bpatcher_1[1] = send_18[0]  # type = ?
gswitch_161 = gswitch(int=1, parameter_enable=0)
gswitch_161[0] = bpatcher_1[0]  # type = ?
message_164 = message()
message_164[0] = gswitch_161[2]  # type = ?
number_163 = number(parameter_enable=0)
number_163[0] = gswitch_161[1]  # type = ?
fromsymbol_165 = fromsymbol(text='fromsymbol')
fromsymbol_165[0] = number_163[0]  # type = ?
fromsymbol_165[0] = message_164[1]  # type = ?
outlet_6 = outlet(comment='')
flonum_123 = flonum(format=6, parameter_enable=0)
flonum_123[0] = outlet_6[0]  # type = ?
send_16 = send(text='s Delta')
bpatcher_140 = _subpatcher(embed=1, lockeddragscroll=0, subpatcher_id=8, viewvisibility=1)
bpatcher_140[0] = flonum_123[0]  # type = ?
bpatcher_140[1] = send_16[0]  # type = ?
gswitch_160 = gswitch(int=1, parameter_enable=0)
gswitch_160[0] = bpatcher_140[0]  # type = ?
message_118 = message()
message_118[0] = gswitch_160[2]  # type = ?
send_21 = send(text='s LowBeta')
outlet_9 = outlet(comment='')
flonum_110 = flonum(format=6, parameter_enable=0)
flonum_110[0] = outlet_9[0]  # type = ?
bpatcher_4 = _subpatcher(embed=1, lockeddragscroll=0, subpatcher_id=4, viewvisibility=1)
bpatcher_4[0] = flonum_110[0]  # type = ?
bpatcher_4[1] = send_21[0]  # type = ?
gswitch_179 = gswitch(int=1, parameter_enable=0)
gswitch_179[0] = bpatcher_4[0]  # type = ?
message_181 = message()
message_181[0] = gswitch_179[2]  # type = ?
number_180 = number(parameter_enable=0)
number_180[0] = gswitch_179[1]  # type = ?
fromsymbol_182 = fromsymbol(text='fromsymbol')
fromsymbol_182[0] = number_180[0]  # type = ?
fromsymbol_182[0] = message_181[1]  # type = ?
number_130 = number(parameter_enable=0)
number_130[0] = gswitch_160[1]  # type = ?
fromsymbol_85 = fromsymbol(text='fromsymbol')
fromsymbol_85[0] = message_118[1]  # type = ?
fromsymbol_85[0] = number_130[0]  # type = ?
outlet_10 = outlet(comment='')
flonum_115 = flonum(format=6, parameter_enable=0)
flonum_115[0] = outlet_10[0]  # type = ?
send_20 = send(text='s HighAlpha')
bpatcher_3 = _subpatcher(embed=1, lockeddragscroll=0, subpatcher_id=5, viewvisibility=1)
bpatcher_3[0] = flonum_115[0]  # type = ?
bpatcher_3[1] = send_20[0]  # type = ?
gswitch_173 = gswitch(int=1, parameter_enable=0)
gswitch_173[0] = bpatcher_3[0]  # type = ?
number_175 = number(parameter_enable=0)
number_175[0] = gswitch_173[1]  # type = ?
message_177 = message()
message_177[0] = gswitch_173[2]  # type = ?
fromsymbol_178 = fromsymbol(text='fromsymbol')
fromsymbol_178[0] = number_175[0]  # type = ?
fromsymbol_178[0] = message_177[1]  # type = ?
number_192 = number(parameter_enable=0)
number_192[0] = gswitch_191[1]  # type = ?
fromsymbol_194 = fromsymbol(text='fromsymbol')
fromsymbol_194[0] = number_192[0]  # type = ?
fromsymbol_194[0] = message_193[1]  # type = ?
gswitch_183 = gswitch(int=1, parameter_enable=0)
gswitch_183[0] = bpatcher_5[0]  # type = ?
number_184 = number(parameter_enable=0)
number_184[0] = gswitch_183[1]  # type = ?
message_185 = message()
message_185[0] = gswitch_183[2]  # type = ?
fromsymbol_186 = fromsymbol(text='fromsymbol')
fromsymbol_186[0] = number_184[0]  # type = ?
fromsymbol_186[0] = message_185[1]  # type = ?
send_23 = send(text='s LowGamma')
outlet_12 = outlet(comment='')
flonum_106 = flonum(format=6, parameter_enable=0)
flonum_106[0] = outlet_12[0]  # type = ?
bpatcher_14 = _subpatcher(embed=1, lockeddragscroll=0, subpatcher_id=2, viewvisibility=1)
bpatcher_14[0] = flonum_106[0]  # type = ?
bpatcher_14[1] = send_23[0]  # type = ?
gswitch_187 = gswitch(int=1, parameter_enable=0)
gswitch_187[0] = bpatcher_14[0]  # type = ?
message_189 = message()
message_189[0] = gswitch_187[2]  # type = ?
number_188 = number(parameter_enable=0)
number_188[0] = gswitch_187[1]  # type = ?
fromsymbol_190 = fromsymbol(text='fromsymbol')
fromsymbol_190[0] = number_188[0]  # type = ?
fromsymbol_190[0] = message_189[1]  # type = ?
send_19 = send(text='s LowAlpha')
outlet_8 = outlet(comment='')
flonum_147 = flonum(format=6, parameter_enable=0)
flonum_147[0] = outlet_8[0]  # type = ?
bpatcher_2 = _subpatcher(embed=1, lockeddragscroll=0, subpatcher_id=6, viewvisibility=1)
bpatcher_2[0] = flonum_147[0]  # type = ?
bpatcher_2[1] = send_19[0]  # type = ?
gswitch_167 = gswitch(int=1, parameter_enable=0)
gswitch_167[0] = bpatcher_2[0]  # type = ?
number_169 = number(parameter_enable=0)
number_169[0] = gswitch_167[1]  # type = ?
message_171 = message()
message_171[0] = gswitch_167[2]  # type = ?
fromsymbol_172 = fromsymbol(text='fromsymbol')
fromsymbol_172[0] = number_169[0]  # type = ?
fromsymbol_172[0] = message_171[1]  # type = ?
p_oscclassf_157 = _subpatcher(subpatcher_id=0, text='p OscClassf')
p_oscclassf_157[0] = fromsymbol_85[0]  # type = ?
p_oscclassf_157[1] = fromsymbol_165[0]  # type = ?
p_oscclassf_157[2] = fromsymbol_172[0]  # type = ?
p_oscclassf_157[3] = fromsymbol_178[0]  # type = ?
p_oscclassf_157[4] = fromsymbol_182[0]  # type = ?
p_oscclassf_157[5] = fromsymbol_186[0]  # type = ?
p_oscclassf_157[6] = fromsymbol_190[0]  # type = ?
p_oscclassf_157[7] = fromsymbol_194[0]  # type = ?
metro_250_57 = newobj(text='metro 250')
metro_250_57[0] = message_118[0]  # type = bang
metro_250_57[0] = message_164[0]  # type = bang
metro_250_57[0] = message_171[0]  # type = bang
metro_250_57[0] = message_177[0]  # type = bang
metro_250_57[0] = message_181[0]  # type = bang
metro_250_57[0] = message_185[0]  # type = bang
metro_250_57[0] = message_189[0]  # type = bang
metro_250_57[0] = message_193[0]  # type = bang
toggle_53 = toggle(parameter_enable=0)
toggle_53[0] = gswitch_160[0]  # type = int
toggle_53[0] = gswitch_161[0]  # type = int
toggle_53[0] = gswitch_167[0]  # type = int
toggle_53[0] = gswitch_173[0]  # type = int
toggle_53[0] = gswitch_179[0]  # type = int
toggle_53[0] = gswitch_183[0]  # type = int
toggle_53[0] = gswitch_187[0]  # type = int
toggle_53[0] = gswitch_191[0]  # type = int
toggle_53[0] = metro_250_57[0]  # type = int
inlet_144 = inlet(comment='')
inlet_144[0] = p_oscclassf_157[0]  # type = ?
inlet_109 = inlet(comment='')
inlet_109[0] = toggle_53[0]  # type = int
inlet_112 = inlet(comment='')
inlet_112[0] = metro_250_57[1]  # type = int
inlet_137 = inlet(comment='')
inlet_137[0] = bpatcher_1[1]  # type = ?
inlet_137[0] = bpatcher_14[1]  # type = ?
inlet_137[0] = bpatcher_140[1]  # type = ?
inlet_137[0] = bpatcher_15[1]  # type = ?
inlet_137[0] = bpatcher_2[1]  # type = ?
inlet_137[0] = bpatcher_3[1]  # type = ?
inlet_137[0] = bpatcher_4[1]  # type = ?
inlet_137[0] = bpatcher_5[1]  # type = ?
# (comment) High Alpha
# (comment) Low Alpha
# (comment) Tetha
# (comment) Delta
# (comment) Gets Data by Osc and de-encapsulatei it
# (comment) Low Beta
# (comment) High Beta
# (comment) Low Gamma
# (comment) High Gamma
# (comment) Reboot
# -- subpatcher 0 --
sp0_outlet_150 = outlet(comment='')
sp0_outlet_152 = outlet(comment='')
sp0_outlet_153 = outlet(comment='')
sp0_outlet_149 = outlet(comment='')
sp0_outlet_151 = outlet(comment='')
sp0_outlet_156 = outlet(comment='')
sp0_outlet_155 = outlet(comment='')
sp0_outlet_148 = outlet(comment='')
sp0_unpack_s_s_s_s_s_s_s_s_82 = newobj(text='unpack s s s s s s s s')
sp0_unpack_s_s_s_s_s_s_s_s_82[0] = sp0_outlet_148[0]  # type = ?
sp0_unpack_s_s_s_s_s_s_s_s_82[1] = sp0_outlet_149[0]  # type = ?
sp0_unpack_s_s_s_s_s_s_s_s_82[2] = sp0_outlet_150[0]  # type = ?
sp0_unpack_s_s_s_s_s_s_s_s_82[3] = sp0_outlet_151[0]  # type = ?
sp0_unpack_s_s_s_s_s_s_s_s_82[4] = sp0_outlet_152[0]  # type = ?
sp0_unpack_s_s_s_s_s_s_s_s_82[5] = sp0_outlet_153[0]  # type = ?
sp0_unpack_s_s_s_s_s_s_s_s_82[6] = sp0_outlet_155[0]  # type = ?
sp0_unpack_s_s_s_s_s_s_s_s_82[7] = sp0_outlet_156[0]  # type = ?
sp0_route_brainrhythms_79 = newobj(text='route /BrainRhythms')
sp0_route_brainrhythms_79[0] = sp0_unpack_s_s_s_s_s_s_s_s_82[0]  # type = ?
sp0_udpreceive_2020_25 = newobj(text='udpreceive 2020')
sp0_udpreceive_2020_25[0] = sp0_route_brainrhythms_79[0]  # type = ?
sp0_message_41 = message(text='port 2020')
sp0_message_41[0] = sp0_udpreceive_2020_25[0]  # type = ?
sp0_button_76 = button()
sp0_button_76[0] = sp0_message_41[0]  # type = bang
sp0_message_39 = message(text='prepend port')
sp0_message_39[0] = sp0_message_41[0]  # type = ?
sp0_message_39[0] = sp0_button_76[0]  # type = ?
sp0_t_b_l_52 = newobj(text='t b l')
sp0_t_b_l_52[0] = sp0_message_39[0]  # type = bang
sp0_t_b_l_52[1] = sp0_message_41[1]  # type = ?
sp0_message_50 = message(text='2020')
sp0_message_50[0] = sp0_t_b_l_52[0]  # type = ?
sp0_t_b_l_145 = newobj(text='t b l')
sp0_t_b_l_145[0] = sp0_message_50[0]  # type = bang
sp0_t_b_l_145[1] = sp0_message_50[1]  # type = ?
sp0_inlet_146 = inlet(comment='')
sp0_inlet_146[0] = sp0_t_b_l_145[0]  # type = ?
# -- subpatcher 1 --
sp1_button_107 = button()
sp1_button_107[0] = sp1_message_108[0]  # type = bang
sp1_button_107[0] = sp1_counter_0_2000_109[0]  # type = bang
# (comment) El primer dato cuando el contador es "0", es el valor inicial
# (comment) Valor inicial
# (comment) Valor Min
# (comment) Valor Max
# (comment) Normalización de la senal para subpatch Arduino
# (comment) Valor minimo de la serie obtenido a través
de un comparador if <
# (comment) Valor maximo de la serie obtenido a través
de un comparador if >
# (comment) Datos Raw High Gamma
# (comment) El valor es retroalimentaco como nuevo valor inicial para la comparación del Max y Min
# (comment) El valor es escalado a flotante entre 0. y 1.
# (comment) Valor Raw High Gamma 
# (comment) Init Time, restarts counter
# (comment) Bang inicia el contador
sp1_counter_0_2000_109 = newobj(text='counter 0 2000')
sp1_counter_0_2000_109[0] = sp1_number_106[0]  # type = int
sp1_expr___f1__f2___f3___100_1 = newobj(text='expr ( ( $f1 - $f2 ) / $f3 ) * 100')
sp1_expr___f1__f2___f3___100_1[0] = sp1_flonum_8[0]  # type = ?
sp1_f_102 = f(text='f')
sp1_f_102[0] = sp1_flonum_117[0]  # type = float
sp1_f_102[0] = sp1_flonum_129[0]  # type = float
sp1_flonum_112 = flonum(format=6, parameter_enable=0)
sp1_flonum_112[0] = sp1_expr___f1__f2___f3___100_1[0]  # type = ?
sp1_flonum_112[0] = sp1_f_102[1]  # type = ?
sp1_flonum_112[0] = sp1_scale_0_100000_0_1_111[0]  # type = ?
sp1_flonum_112[0] = sp1_if_f1__f2_then_f1_121[0]  # type = ?
sp1_flonum_112[0] = sp1_if_f1__f2_then_f1_131[0]  # type = ?
sp1_flonum_117 = flonum(format=6, parameter_enable=0)
sp1_flonum_117[0] = sp1_if_f1__f2_then_f1_121[1]  # type = ?
sp1_flonum_125 = flonum(format=6, parameter_enable=0)
sp1_flonum_125[0] = sp1_expr___f1__f2___f3___100_1[1]  # type = ?
sp1_flonum_125[0] = sp1_scale_0_100000_0_1_111[1]  # type = ?
sp1_flonum_125[0] = sp1_flonum_117[0]  # type = ?
sp1_flonum_129 = flonum(format=6, parameter_enable=0)
sp1_flonum_129[0] = sp1_if_f1__f2_then_f1_131[1]  # type = ?
sp1_flonum_130 = flonum(format=6, parameter_enable=0)
sp1_flonum_130[0] = sp1_expr___f1__f2___f3___100_1[2]  # type = ?
sp1_flonum_130[0] = sp1_scale_0_100000_0_1_111[2]  # type = ?
sp1_flonum_130[0] = sp1_flonum_129[0]  # type = ?
sp1_flonum_8 = flonum(format=6, parameter_enable=0)
sp1_flonum_8[0] = sp1_outlet_10[0]  # type = ?
sp1_fromsymbol_113 = fromsymbol(text='fromsymbol')
sp1_fromsymbol_113[0] = sp1_flonum_112[0]  # type = ?
sp1_if_f1__f2_then_f1_121 = newobj(text='if $f1 < $f2 then $f1')
sp1_if_f1__f2_then_f1_121[0] = sp1_flonum_125[0]  # type = ?
sp1_if_f1__f2_then_f1_131 = newobj(text='if $f1 > $f2 then $f1')
sp1_if_f1__f2_then_f1_131[0] = sp1_flonum_130[0]  # type = ?
sp1_if_i1__0_then_bang_103 = newobj(text='if $i1 == 0 then bang')
sp1_if_i1__0_then_bang_103[0] = sp1_f_102[0]  # type = ?
sp1_inlet_137 = inlet(comment='', varname='u330003269')
sp1_inlet_137[0] = sp1_button_107[0]  # type = ?
sp1_inlet_137[0] = sp1_message_108[1]  # type = ?
sp1_inlet_138 = inlet(comment='', varname='u608003270')
sp1_inlet_138[0] = sp1_counter_0_2000_109[2]  # type = ?
sp1_message_108 = message(text='"2051984.97049"')
sp1_message_108[0] = sp1_fromsymbol_113[0]  # type = ?
sp1_number_106 = number(parameter_enable=0)
sp1_number_106[0] = sp1_if_i1__0_then_bang_103[0]  # type = ?
sp1_outlet_10 = outlet(comment='', varname='u375003271[1]')
sp1_outlet_139 = outlet(comment='', varname='u375003271')
sp1_scale_0_100000_0_1_111 = newobj(text='scale 0. 100000. 0. 1.')
sp1_scale_0_100000_0_1_111[0] = sp1_outlet_139[0]  # type = ?
# -- subpatcher 2 --
sp2_button_107 = button()
sp2_button_107[0] = sp2_message_108[0]  # type = bang
sp2_button_107[0] = sp2_counter_0_2000_109[0]  # type = bang
# (comment) El primer dato cuando el contador es "0", es el valor inicial
# (comment) Valor inicial
# (comment) Valor Min
# (comment) Valor Max
# (comment) Normalización de la senal para subpatch Arduino
# (comment) Valor minimo de la serie obtenido a través
de un comparador if <
# (comment) Valor maximo de la serie obtenido a través
de un comparador if >
# (comment) Datos Raw Low Gamma
# (comment) El valor es retroalimentaco como nuevo valor inicial para la comparación del Max y Min
# (comment) El valor es escalado a flotante entre 0. y 1.
# (comment) Valor Raw Low Gamma 
# (comment) Init Time, restarts counter
# (comment) Bang inicia el contador
sp2_counter_0_2000_109 = newobj(text='counter 0 2000')
sp2_counter_0_2000_109[0] = sp2_number_106[0]  # type = int
sp2_expr___f1__f2___f3___100_1 = newobj(text='expr ( ( $f1 - $f2 ) / $f3 ) * 100')
sp2_expr___f1__f2___f3___100_1[0] = sp2_flonum_8[0]  # type = ?
sp2_f_102 = f(text='f')
sp2_f_102[0] = sp2_flonum_117[0]  # type = float
sp2_f_102[0] = sp2_flonum_129[0]  # type = float
sp2_flonum_112 = flonum(format=6, parameter_enable=0)
sp2_flonum_112[0] = sp2_expr___f1__f2___f3___100_1[0]  # type = ?
sp2_flonum_112[0] = sp2_f_102[1]  # type = ?
sp2_flonum_112[0] = sp2_scale_0_100000_0_1_111[0]  # type = ?
sp2_flonum_112[0] = sp2_if_f1__f2_then_f1_121[0]  # type = ?
sp2_flonum_112[0] = sp2_if_f1__f2_then_f1_131[0]  # type = ?
sp2_flonum_117 = flonum(format=6, parameter_enable=0)
sp2_flonum_117[0] = sp2_if_f1__f2_then_f1_121[1]  # type = ?
sp2_flonum_125 = flonum(format=6, parameter_enable=0)
sp2_flonum_125[0] = sp2_expr___f1__f2___f3___100_1[1]  # type = ?
sp2_flonum_125[0] = sp2_scale_0_100000_0_1_111[1]  # type = ?
sp2_flonum_125[0] = sp2_flonum_117[0]  # type = ?
sp2_flonum_129 = flonum(format=6, parameter_enable=0)
sp2_flonum_129[0] = sp2_if_f1__f2_then_f1_131[1]  # type = ?
sp2_flonum_130 = flonum(format=6, parameter_enable=0)
sp2_flonum_130[0] = sp2_expr___f1__f2___f3___100_1[2]  # type = ?
sp2_flonum_130[0] = sp2_scale_0_100000_0_1_111[2]  # type = ?
sp2_flonum_130[0] = sp2_flonum_129[0]  # type = ?
sp2_flonum_8 = flonum(format=6, parameter_enable=0)
sp2_flonum_8[0] = sp2_outlet_10[0]  # type = ?
sp2_fromsymbol_113 = fromsymbol(text='fromsymbol')
sp2_fromsymbol_113[0] = sp2_flonum_112[0]  # type = ?
sp2_if_f1__f2_then_f1_121 = newobj(text='if $f1 < $f2 then $f1')
sp2_if_f1__f2_then_f1_121[0] = sp2_flonum_125[0]  # type = ?
sp2_if_f1__f2_then_f1_131 = newobj(text='if $f1 > $f2 then $f1')
sp2_if_f1__f2_then_f1_131[0] = sp2_flonum_130[0]  # type = ?
sp2_if_i1__0_then_bang_103 = newobj(text='if $i1 == 0 then bang')
sp2_if_i1__0_then_bang_103[0] = sp2_f_102[0]  # type = ?
sp2_inlet_137 = inlet(comment='', varname='u330003269')
sp2_inlet_137[0] = sp2_button_107[0]  # type = ?
sp2_inlet_137[0] = sp2_message_108[1]  # type = ?
sp2_inlet_138 = inlet(comment='', varname='u608003270')
sp2_inlet_138[0] = sp2_counter_0_2000_109[2]  # type = ?
sp2_message_108 = message(text='"1578576.60936"')
sp2_message_108[0] = sp2_fromsymbol_113[0]  # type = ?
sp2_number_106 = number(parameter_enable=0)
sp2_number_106[0] = sp2_if_i1__0_then_bang_103[0]  # type = ?
sp2_outlet_10 = outlet(comment='', varname='u375003271[1]')
sp2_outlet_139 = outlet(comment='', varname='u375003271')
sp2_scale_0_100000_0_1_111 = newobj(text='scale 0. 100000. 0. 1.')
sp2_scale_0_100000_0_1_111[0] = sp2_outlet_139[0]  # type = ?
# -- subpatcher 3 --
sp3_button_107 = button()
sp3_button_107[0] = sp3_message_108[0]  # type = bang
sp3_button_107[0] = sp3_counter_0_2000_109[0]  # type = bang
# (comment) El primer dato cuando el contador es "0", es el valor inicial
# (comment) Valor inicial
# (comment) Valor Min
# (comment) Valor Max
# (comment) Normalización de la senal para subpatch Arduino
# (comment) Valor minimo de la serie obtenido a través
de un comparador if <
# (comment) Valor maximo de la serie obtenido a través
de un comparador if >
# (comment) Datos Raw High Beta
# (comment) El valor es retroalimentaco como nuevo valor inicial para la comparación del Max y Min
# (comment) El valor es escalado a flotante entre 0. y 1.
# (comment) Valor Raw High Beta 
# (comment) Init Time, restarts counter
# (comment) Bang inicia el contador
sp3_counter_0_2000_109 = newobj(text='counter 0 2000')
sp3_counter_0_2000_109[0] = sp3_number_106[0]  # type = int
sp3_expr___f1__f2___f3___100_1 = newobj(text='expr ( ( $f1 - $f2 ) / $f3 ) * 100')
sp3_expr___f1__f2___f3___100_1[0] = sp3_flonum_8[0]  # type = ?
sp3_f_102 = f(text='f')
sp3_f_102[0] = sp3_flonum_117[0]  # type = float
sp3_f_102[0] = sp3_flonum_129[0]  # type = float
sp3_flonum_112 = flonum(format=6, parameter_enable=0)
sp3_flonum_112[0] = sp3_expr___f1__f2___f3___100_1[0]  # type = ?
sp3_flonum_112[0] = sp3_f_102[1]  # type = ?
sp3_flonum_112[0] = sp3_scale_0_100000_0_1_111[0]  # type = ?
sp3_flonum_112[0] = sp3_if_f1__f2_then_f1_121[0]  # type = ?
sp3_flonum_112[0] = sp3_if_f1__f2_then_f1_131[0]  # type = ?
sp3_flonum_117 = flonum(format=6, parameter_enable=0)
sp3_flonum_117[0] = sp3_if_f1__f2_then_f1_121[1]  # type = ?
sp3_flonum_125 = flonum(format=6, parameter_enable=0)
sp3_flonum_125[0] = sp3_expr___f1__f2___f3___100_1[1]  # type = ?
sp3_flonum_125[0] = sp3_scale_0_100000_0_1_111[1]  # type = ?
sp3_flonum_125[0] = sp3_flonum_117[0]  # type = ?
sp3_flonum_129 = flonum(format=6, parameter_enable=0)
sp3_flonum_129[0] = sp3_if_f1__f2_then_f1_131[1]  # type = ?
sp3_flonum_130 = flonum(format=6, parameter_enable=0)
sp3_flonum_130[0] = sp3_expr___f1__f2___f3___100_1[2]  # type = ?
sp3_flonum_130[0] = sp3_scale_0_100000_0_1_111[2]  # type = ?
sp3_flonum_130[0] = sp3_flonum_129[0]  # type = ?
sp3_flonum_8 = flonum(format=6, parameter_enable=0)
sp3_flonum_8[0] = sp3_outlet_10[0]  # type = ?
sp3_fromsymbol_113 = fromsymbol(text='fromsymbol')
sp3_fromsymbol_113[0] = sp3_flonum_112[0]  # type = ?
sp3_if_f1__f2_then_f1_121 = newobj(text='if $f1 < $f2 then $f1')
sp3_if_f1__f2_then_f1_121[0] = sp3_flonum_125[0]  # type = ?
sp3_if_f1__f2_then_f1_131 = newobj(text='if $f1 > $f2 then $f1')
sp3_if_f1__f2_then_f1_131[0] = sp3_flonum_130[0]  # type = ?
sp3_if_i1__0_then_bang_103 = newobj(text='if $i1 == 0 then bang')
sp3_if_i1__0_then_bang_103[0] = sp3_f_102[0]  # type = ?
sp3_inlet_137 = inlet(comment='', varname='u330003269')
sp3_inlet_137[0] = sp3_button_107[0]  # type = ?
sp3_inlet_137[0] = sp3_message_108[1]  # type = ?
sp3_inlet_138 = inlet(comment='', varname='u608003270')
sp3_inlet_138[0] = sp3_counter_0_2000_109[2]  # type = ?
sp3_message_108 = message(text='"3660106.95131"')
sp3_message_108[0] = sp3_fromsymbol_113[0]  # type = ?
sp3_number_106 = number(parameter_enable=0)
sp3_number_106[0] = sp3_if_i1__0_then_bang_103[0]  # type = ?
sp3_outlet_10 = outlet(comment='', varname='u375003271[1]')
sp3_outlet_139 = outlet(comment='', varname='u375003271')
sp3_scale_0_100000_0_1_111 = newobj(text='scale 0. 100000. 0. 1.')
sp3_scale_0_100000_0_1_111[0] = sp3_outlet_139[0]  # type = ?
# -- subpatcher 4 --
sp4_button_107 = button()
sp4_button_107[0] = sp4_message_108[0]  # type = bang
sp4_button_107[0] = sp4_counter_0_2000_109[0]  # type = bang
# (comment) El primer dato cuando el contador es "0", es el valor inicial
# (comment) Valor inicial
# (comment) Valor Min
# (comment) Valor Max
# (comment) Normalización de la senal para subpatch Arduino
# (comment) Valor minimo de la serie obtenido a través
de un comparador if <
# (comment) Valor maximo de la serie obtenido a través
de un comparador if >
# (comment) Datos Raw Low Beta
# (comment) El valor es retroalimentaco como nuevo valor inicial para la comparación del Max y Min
# (comment) El valor es escalado a flotante entre 0. y 1.
# (comment) Valor Raw Low Beta 
# (comment) Init Time, restarts counter
# (comment) Bang inicia el contador
sp4_counter_0_2000_109 = newobj(text='counter 0 2000')
sp4_counter_0_2000_109[0] = sp4_number_106[0]  # type = int
sp4_expr___f1__f2___f3___100_1 = newobj(text='expr ( ( $f1 - $f2 ) / $f3 ) * 100')
sp4_expr___f1__f2___f3___100_1[0] = sp4_flonum_8[0]  # type = ?
sp4_f_102 = f(text='f')
sp4_f_102[0] = sp4_flonum_117[0]  # type = float
sp4_f_102[0] = sp4_flonum_129[0]  # type = float
sp4_flonum_112 = flonum(format=6, parameter_enable=0)
sp4_flonum_112[0] = sp4_expr___f1__f2___f3___100_1[0]  # type = ?
sp4_flonum_112[0] = sp4_f_102[1]  # type = ?
sp4_flonum_112[0] = sp4_scale_0_100000_0_1_111[0]  # type = ?
sp4_flonum_112[0] = sp4_if_f1__f2_then_f1_121[0]  # type = ?
sp4_flonum_112[0] = sp4_if_f1__f2_then_f1_131[0]  # type = ?
sp4_flonum_117 = flonum(format=6, parameter_enable=0)
sp4_flonum_117[0] = sp4_if_f1__f2_then_f1_121[1]  # type = ?
sp4_flonum_125 = flonum(format=6, parameter_enable=0)
sp4_flonum_125[0] = sp4_expr___f1__f2___f3___100_1[1]  # type = ?
sp4_flonum_125[0] = sp4_scale_0_100000_0_1_111[1]  # type = ?
sp4_flonum_125[0] = sp4_flonum_117[0]  # type = ?
sp4_flonum_129 = flonum(format=6, parameter_enable=0)
sp4_flonum_129[0] = sp4_if_f1__f2_then_f1_131[1]  # type = ?
sp4_flonum_130 = flonum(format=6, parameter_enable=0)
sp4_flonum_130[0] = sp4_expr___f1__f2___f3___100_1[2]  # type = ?
sp4_flonum_130[0] = sp4_scale_0_100000_0_1_111[2]  # type = ?
sp4_flonum_130[0] = sp4_flonum_129[0]  # type = ?
sp4_flonum_8 = flonum(format=6, parameter_enable=0)
sp4_flonum_8[0] = sp4_outlet_10[0]  # type = ?
sp4_fromsymbol_113 = fromsymbol(text='fromsymbol')
sp4_fromsymbol_113[0] = sp4_flonum_112[0]  # type = ?
sp4_if_f1__f2_then_f1_121 = newobj(text='if $f1 < $f2 then $f1')
sp4_if_f1__f2_then_f1_121[0] = sp4_flonum_125[0]  # type = ?
sp4_if_f1__f2_then_f1_131 = newobj(text='if $f1 > $f2 then $f1')
sp4_if_f1__f2_then_f1_131[0] = sp4_flonum_130[0]  # type = ?
sp4_if_i1__0_then_bang_103 = newobj(text='if $i1 == 0 then bang')
sp4_if_i1__0_then_bang_103[0] = sp4_f_102[0]  # type = ?
sp4_inlet_137 = inlet(comment='', varname='u330003269')
sp4_inlet_137[0] = sp4_button_107[0]  # type = ?
sp4_inlet_137[0] = sp4_message_108[1]  # type = ?
sp4_inlet_138 = inlet(comment='', varname='u608003270')
sp4_inlet_138[0] = sp4_counter_0_2000_109[2]  # type = ?
sp4_message_108 = message(text='"9489321.2337"')
sp4_message_108[0] = sp4_fromsymbol_113[0]  # type = ?
sp4_number_106 = number(parameter_enable=0)
sp4_number_106[0] = sp4_if_i1__0_then_bang_103[0]  # type = ?
sp4_outlet_10 = outlet(comment='', varname='u375003271[1]')
sp4_outlet_139 = outlet(comment='', varname='u375003271')
sp4_scale_0_100000_0_1_111 = newobj(text='scale 0. 100000. 0. 1.')
sp4_scale_0_100000_0_1_111[0] = sp4_outlet_139[0]  # type = ?
# -- subpatcher 5 --
sp5_button_107 = button()
sp5_button_107[0] = sp5_message_108[0]  # type = bang
sp5_button_107[0] = sp5_counter_0_2000_109[0]  # type = bang
# (comment) El primer dato cuando el contador es "0", es el valor inicial
# (comment) Valor inicial
# (comment) Valor Min
# (comment) Valor Max
# (comment) Normalización de la senal para subpatch Arduino
# (comment) Valor minimo de la serie obtenido a través
de un comparador if <
# (comment) Valor maximo de la serie obtenido a través
de un comparador if >
# (comment) Datos Raw High Alpha
# (comment) El valor es retroalimentaco como nuevo valor inicial para la comparación del Max y Min
# (comment) El valor es escalado a flotante entre 0. y 1.
# (comment) Valor Raw high Alpha 
# (comment) Init Time, restarts counter
# (comment) Bang inicia el contador
sp5_counter_0_2000_109 = newobj(text='counter 0 2000')
sp5_counter_0_2000_109[0] = sp5_number_106[0]  # type = int
sp5_expr___f1__f2___f3___100_1 = newobj(text='expr ( ( $f1 - $f2 ) / $f3 ) * 100')
sp5_expr___f1__f2___f3___100_1[0] = sp5_flonum_8[0]  # type = ?
sp5_f_102 = f(text='f')
sp5_f_102[0] = sp5_flonum_117[0]  # type = float
sp5_f_102[0] = sp5_flonum_129[0]  # type = float
sp5_flonum_112 = flonum(format=6, parameter_enable=0)
sp5_flonum_112[0] = sp5_expr___f1__f2___f3___100_1[0]  # type = ?
sp5_flonum_112[0] = sp5_f_102[1]  # type = ?
sp5_flonum_112[0] = sp5_scale_0_100000_0_1_111[0]  # type = ?
sp5_flonum_112[0] = sp5_if_f1__f2_then_f1_121[0]  # type = ?
sp5_flonum_112[0] = sp5_if_f1__f2_then_f1_131[0]  # type = ?
sp5_flonum_117 = flonum(format=6, parameter_enable=0)
sp5_flonum_117[0] = sp5_if_f1__f2_then_f1_121[1]  # type = ?
sp5_flonum_125 = flonum(format=6, parameter_enable=0)
sp5_flonum_125[0] = sp5_expr___f1__f2___f3___100_1[1]  # type = ?
sp5_flonum_125[0] = sp5_scale_0_100000_0_1_111[1]  # type = ?
sp5_flonum_125[0] = sp5_flonum_117[0]  # type = ?
sp5_flonum_129 = flonum(format=6, parameter_enable=0)
sp5_flonum_129[0] = sp5_if_f1__f2_then_f1_131[1]  # type = ?
sp5_flonum_130 = flonum(format=6, parameter_enable=0)
sp5_flonum_130[0] = sp5_expr___f1__f2___f3___100_1[2]  # type = ?
sp5_flonum_130[0] = sp5_scale_0_100000_0_1_111[2]  # type = ?
sp5_flonum_130[0] = sp5_flonum_129[0]  # type = ?
sp5_flonum_8 = flonum(format=6, parameter_enable=0)
sp5_flonum_8[0] = sp5_outlet_10[0]  # type = ?
sp5_fromsymbol_113 = fromsymbol(text='fromsymbol')
sp5_fromsymbol_113[0] = sp5_flonum_112[0]  # type = ?
sp5_if_f1__f2_then_f1_121 = newobj(text='if $f1 < $f2 then $f1')
sp5_if_f1__f2_then_f1_121[0] = sp5_flonum_125[0]  # type = ?
sp5_if_f1__f2_then_f1_131 = newobj(text='if $f1 > $f2 then $f1')
sp5_if_f1__f2_then_f1_131[0] = sp5_flonum_130[0]  # type = ?
sp5_if_i1__0_then_bang_103 = newobj(text='if $i1 == 0 then bang')
sp5_if_i1__0_then_bang_103[0] = sp5_f_102[0]  # type = ?
sp5_inlet_137 = inlet(comment='', varname='u330003269')
sp5_inlet_137[0] = sp5_button_107[0]  # type = ?
sp5_inlet_137[0] = sp5_message_108[1]  # type = ?
sp5_inlet_138 = inlet(comment='', varname='u608003270')
sp5_inlet_138[0] = sp5_counter_0_2000_109[2]  # type = ?
sp5_message_108 = message(text='"16815923.6687"')
sp5_message_108[0] = sp5_fromsymbol_113[0]  # type = ?
sp5_number_106 = number(parameter_enable=0)
sp5_number_106[0] = sp5_if_i1__0_then_bang_103[0]  # type = ?
sp5_outlet_10 = outlet(comment='', varname='u375003271[1]')
sp5_outlet_139 = outlet(comment='', varname='u375003271')
sp5_scale_0_100000_0_1_111 = newobj(text='scale 0. 100000. 0. 1.')
sp5_scale_0_100000_0_1_111[0] = sp5_outlet_139[0]  # type = ?
# -- subpatcher 6 --
sp6_button_107 = button()
sp6_button_107[0] = sp6_message_108[0]  # type = bang
sp6_button_107[0] = sp6_counter_0_2000_109[0]  # type = bang
# (comment) El primer dato cuando el contador es "0", es el valor inicial
# (comment) Valor inicial
# (comment) Valor Min
# (comment) Valor Max
# (comment) Normalización de la senal para subpatch Arduino
# (comment) Valor minimo de la serie obtenido a través
de un comparador if <
# (comment) Valor maximo de la serie obtenido a través
de un comparador if >
# (comment) Datos Raw Low Alpha
# (comment) El valor es retroalimentaco como nuevo valor inicial para la comparación del Max y Min
# (comment) El valor es escalado a flotante entre 0. y 1.
# (comment) Valor Raw Low Alpha 
# (comment) Init Time, restarts counter
# (comment) Bang inicia el contador
sp6_counter_0_2000_109 = newobj(text='counter 0 2000')
sp6_counter_0_2000_109[0] = sp6_number_106[0]  # type = int
sp6_expr___f1__f2___f3___100_1 = newobj(text='expr ( ( $f1 - $f2 ) / $f3 ) * 100')
sp6_expr___f1__f2___f3___100_1[0] = sp6_flonum_8[0]  # type = ?
sp6_f_102 = f(text='f')
sp6_f_102[0] = sp6_flonum_117[0]  # type = float
sp6_f_102[0] = sp6_flonum_129[0]  # type = float
sp6_flonum_112 = flonum(format=6, parameter_enable=0)
sp6_flonum_112[0] = sp6_expr___f1__f2___f3___100_1[0]  # type = ?
sp6_flonum_112[0] = sp6_f_102[1]  # type = ?
sp6_flonum_112[0] = sp6_scale_0_100000_0_1_111[0]  # type = ?
sp6_flonum_112[0] = sp6_if_f1__f2_then_f1_121[0]  # type = ?
sp6_flonum_112[0] = sp6_if_f1__f2_then_f1_131[0]  # type = ?
sp6_flonum_117 = flonum(format=6, parameter_enable=0)
sp6_flonum_117[0] = sp6_if_f1__f2_then_f1_121[1]  # type = ?
sp6_flonum_125 = flonum(format=6, parameter_enable=0)
sp6_flonum_125[0] = sp6_expr___f1__f2___f3___100_1[1]  # type = ?
sp6_flonum_125[0] = sp6_scale_0_100000_0_1_111[1]  # type = ?
sp6_flonum_125[0] = sp6_flonum_117[0]  # type = ?
sp6_flonum_129 = flonum(format=6, parameter_enable=0)
sp6_flonum_129[0] = sp6_if_f1__f2_then_f1_131[1]  # type = ?
sp6_flonum_130 = flonum(format=6, parameter_enable=0)
sp6_flonum_130[0] = sp6_expr___f1__f2___f3___100_1[2]  # type = ?
sp6_flonum_130[0] = sp6_scale_0_100000_0_1_111[2]  # type = ?
sp6_flonum_130[0] = sp6_flonum_129[0]  # type = ?
sp6_flonum_8 = flonum(format=6, parameter_enable=0)
sp6_flonum_8[0] = sp6_outlet_10[0]  # type = ?
sp6_fromsymbol_113 = fromsymbol(text='fromsymbol')
sp6_fromsymbol_113[0] = sp6_flonum_112[0]  # type = ?
sp6_if_f1__f2_then_f1_121 = newobj(text='if $f1 < $f2 then $f1')
sp6_if_f1__f2_then_f1_121[0] = sp6_flonum_125[0]  # type = ?
sp6_if_f1__f2_then_f1_131 = newobj(text='if $f1 > $f2 then $f1')
sp6_if_f1__f2_then_f1_131[0] = sp6_flonum_130[0]  # type = ?
sp6_if_i1__0_then_bang_103 = newobj(text='if $i1 == 0 then bang')
sp6_if_i1__0_then_bang_103[0] = sp6_f_102[0]  # type = ?
sp6_inlet_137 = inlet(comment='', varname='u330003269')
sp6_inlet_137[0] = sp6_button_107[0]  # type = ?
sp6_inlet_137[0] = sp6_message_108[1]  # type = ?
sp6_inlet_138 = inlet(comment='', varname='u608003270')
sp6_inlet_138[0] = sp6_counter_0_2000_109[2]  # type = ?
sp6_message_108 = message(text='"35549470.9662"')
sp6_message_108[0] = sp6_fromsymbol_113[0]  # type = ?
sp6_number_106 = number(parameter_enable=0)
sp6_number_106[0] = sp6_if_i1__0_then_bang_103[0]  # type = ?
sp6_outlet_10 = outlet(comment='', varname='u375003271[1]')
sp6_outlet_139 = outlet(comment='', varname='u375003271')
sp6_scale_0_100000_0_1_111 = newobj(text='scale 0. 100000. 0. 1.')
sp6_scale_0_100000_0_1_111[0] = sp6_outlet_139[0]  # type = ?
# -- subpatcher 7 --
sp7_button_107 = button()
sp7_button_107[0] = sp7_message_108[0]  # type = bang
sp7_button_107[0] = sp7_counter_0_2000_109[0]  # type = bang
# (comment) El primer dato cuando el contador es "0", es el valor inicial
# (comment) Valor inicial
# (comment) Valor Min
# (comment) Valor Max
# (comment) Normalización de la senal para subpatch Arduino
# (comment) Valor minimo de la serie obtenido a través
de un comparador if <
# (comment) Valor maximo de la serie obtenido a través
de un comparador if >
# (comment) Datos Raw Tetha
# (comment) El valor es retroalimentaco como nuevo valor inicial para la comparación del Max y Min
# (comment) El valor es escalado a flotante entre 0. y 1.
# (comment) Valor Raw Tetha 
# (comment) Init Time, restarts counter
# (comment) Bang inicia el contador
sp7_counter_0_2000_109 = newobj(text='counter 0 2000')
sp7_counter_0_2000_109[0] = sp7_number_106[0]  # type = int
sp7_expr___f1__f2___f3___100_1 = newobj(text='expr ( ( $f1 - $f2 ) / $f3 ) * 100')
sp7_expr___f1__f2___f3___100_1[0] = sp7_flonum_8[0]  # type = ?
sp7_f_102 = f(text='f')
sp7_f_102[0] = sp7_flonum_117[0]  # type = float
sp7_f_102[0] = sp7_flonum_129[0]  # type = float
sp7_flonum_112 = flonum(format=6, parameter_enable=0)
sp7_flonum_112[0] = sp7_expr___f1__f2___f3___100_1[0]  # type = ?
sp7_flonum_112[0] = sp7_f_102[1]  # type = ?
sp7_flonum_112[0] = sp7_scale_0_100000_0_1_111[0]  # type = ?
sp7_flonum_112[0] = sp7_if_f1__f2_then_f1_121[0]  # type = ?
sp7_flonum_112[0] = sp7_if_f1__f2_then_f1_131[0]  # type = ?
sp7_flonum_117 = flonum(format=6, parameter_enable=0)
sp7_flonum_117[0] = sp7_if_f1__f2_then_f1_121[1]  # type = ?
sp7_flonum_125 = flonum(format=6, parameter_enable=0)
sp7_flonum_125[0] = sp7_expr___f1__f2___f3___100_1[1]  # type = ?
sp7_flonum_125[0] = sp7_scale_0_100000_0_1_111[1]  # type = ?
sp7_flonum_125[0] = sp7_flonum_117[0]  # type = ?
sp7_flonum_129 = flonum(format=6, parameter_enable=0)
sp7_flonum_129[0] = sp7_if_f1__f2_then_f1_131[1]  # type = ?
sp7_flonum_130 = flonum(format=6, parameter_enable=0)
sp7_flonum_130[0] = sp7_expr___f1__f2___f3___100_1[2]  # type = ?
sp7_flonum_130[0] = sp7_scale_0_100000_0_1_111[2]  # type = ?
sp7_flonum_130[0] = sp7_flonum_129[0]  # type = ?
sp7_flonum_8 = flonum(format=6, parameter_enable=0)
sp7_flonum_8[0] = sp7_outlet_10[0]  # type = ?
sp7_fromsymbol_113 = fromsymbol(text='fromsymbol')
sp7_fromsymbol_113[0] = sp7_flonum_112[0]  # type = ?
sp7_if_f1__f2_then_f1_121 = newobj(text='if $f1 < $f2 then $f1')
sp7_if_f1__f2_then_f1_121[0] = sp7_flonum_125[0]  # type = ?
sp7_if_f1__f2_then_f1_131 = newobj(text='if $f1 > $f2 then $f1')
sp7_if_f1__f2_then_f1_131[0] = sp7_flonum_130[0]  # type = ?
sp7_if_i1__0_then_bang_103 = newobj(text='if $i1 == 0 then bang')
sp7_if_i1__0_then_bang_103[0] = sp7_f_102[0]  # type = ?
sp7_inlet_137 = inlet(comment='', varname='u330003269')
sp7_inlet_137[0] = sp7_button_107[0]  # type = ?
sp7_inlet_137[0] = sp7_message_108[1]  # type = ?
sp7_inlet_138 = inlet(comment='', varname='u608003270')
sp7_inlet_138[0] = sp7_counter_0_2000_109[2]  # type = ?
sp7_message_108 = message(text='"39337368.8005"')
sp7_message_108[0] = sp7_fromsymbol_113[0]  # type = ?
sp7_number_106 = number(parameter_enable=0)
sp7_number_106[0] = sp7_if_i1__0_then_bang_103[0]  # type = ?
sp7_outlet_10 = outlet(comment='', varname='u375003271[1]')
sp7_outlet_139 = outlet(comment='', varname='u375003271')
sp7_scale_0_100000_0_1_111 = newobj(text='scale 0. 100000. 0. 1.')
sp7_scale_0_100000_0_1_111[0] = sp7_outlet_139[0]  # type = ?
# -- subpatcher 8 --
sp8_button_107 = button()
sp8_button_107[0] = sp8_message_108[0]  # type = bang
sp8_button_107[0] = sp8_counter_0_2000_109[0]  # type = bang
# (comment) El primer dato cuando el contador es "0", es el valor inicial
# (comment) Valor inicial
# (comment) Valor Min
# (comment) Valor Max
# (comment) Normalización de la senal para subpatch Arduino
# (comment) Valor minimo de la serie obtenido a través
de un comparador if <
# (comment) Valor maximo de la serie obtenido a través
de un comparador if >
# (comment) Datos Raw Delta
# (comment) El valor es retroalimentaco como nuevo valor inicial para la comparación del Max y Min
# (comment) El valor es escalado a flotante entre 0. y 1.
# (comment) Valor Raw Delta 
# (comment) Init Time, restarts counter
# (comment) Bang inicia el contador
sp8_counter_0_2000_109 = newobj(text='counter 0 2000')
sp8_counter_0_2000_109[0] = sp8_number_106[0]  # type = int
sp8_expr___f1__f2___f3___100_1 = newobj(text='expr ( ( $f1 - $f2 ) / $f3 ) * 100')
sp8_expr___f1__f2___f3___100_1[0] = sp8_flonum_8[0]  # type = ?
sp8_f_102 = f(text='f')
sp8_f_102[0] = sp8_flonum_117[0]  # type = float
sp8_f_102[0] = sp8_flonum_129[0]  # type = float
sp8_flonum_112 = flonum(format=6, parameter_enable=0)
sp8_flonum_112[0] = sp8_expr___f1__f2___f3___100_1[0]  # type = ?
sp8_flonum_112[0] = sp8_f_102[1]  # type = ?
sp8_flonum_112[0] = sp8_scale_0_100000_0_1_111[0]  # type = ?
sp8_flonum_112[0] = sp8_if_f1__f2_then_f1_121[0]  # type = ?
sp8_flonum_112[0] = sp8_if_f1__f2_then_f1_131[0]  # type = ?
sp8_flonum_117 = flonum(format=6, parameter_enable=0)
sp8_flonum_117[0] = sp8_if_f1__f2_then_f1_121[1]  # type = ?
sp8_flonum_125 = flonum(format=6, parameter_enable=0)
sp8_flonum_125[0] = sp8_expr___f1__f2___f3___100_1[1]  # type = ?
sp8_flonum_125[0] = sp8_scale_0_100000_0_1_111[1]  # type = ?
sp8_flonum_125[0] = sp8_flonum_117[0]  # type = ?
sp8_flonum_129 = flonum(format=6, parameter_enable=0)
sp8_flonum_129[0] = sp8_if_f1__f2_then_f1_131[1]  # type = ?
sp8_flonum_130 = flonum(format=6, parameter_enable=0)
sp8_flonum_130[0] = sp8_expr___f1__f2___f3___100_1[2]  # type = ?
sp8_flonum_130[0] = sp8_scale_0_100000_0_1_111[2]  # type = ?
sp8_flonum_130[0] = sp8_flonum_129[0]  # type = ?
sp8_flonum_8 = flonum(format=6, parameter_enable=0)
sp8_flonum_8[0] = sp8_outlet_10[0]  # type = ?
sp8_fromsymbol_113 = fromsymbol(text='fromsymbol')
sp8_fromsymbol_113[0] = sp8_flonum_112[0]  # type = ?
sp8_if_f1__f2_then_f1_121 = newobj(text='if $f1 < $f2 then $f1')
sp8_if_f1__f2_then_f1_121[0] = sp8_flonum_125[0]  # type = ?
sp8_if_f1__f2_then_f1_131 = newobj(text='if $f1 > $f2 then $f1')
sp8_if_f1__f2_then_f1_131[0] = sp8_flonum_130[0]  # type = ?
sp8_if_i1__0_then_bang_103 = newobj(text='if $i1 == 0 then bang')
sp8_if_i1__0_then_bang_103[0] = sp8_f_102[0]  # type = ?
sp8_inlet_137 = inlet(comment='', varname='u330003269')
sp8_inlet_137[0] = sp8_button_107[0]  # type = ?
sp8_inlet_137[0] = sp8_message_108[1]  # type = ?
sp8_inlet_138 = inlet(comment='', varname='u608003270')
sp8_inlet_138[0] = sp8_counter_0_2000_109[2]  # type = ?
sp8_message_108 = message(text='"3032808731.5"')
sp8_message_108[0] = sp8_fromsymbol_113[0]  # type = ?
sp8_number_106 = number(parameter_enable=0)
sp8_number_106[0] = sp8_if_i1__0_then_bang_103[0]  # type = ?
sp8_outlet_10 = outlet(comment='', varname='u375003271[1]')
sp8_outlet_139 = outlet(comment='', varname='u375003271')
sp8_scale_0_100000_0_1_111 = newobj(text='scale 0. 100000. 0. 1.')
sp8_scale_0_100000_0_1_111[0] = sp8_outlet_139[0]  # type = ?
